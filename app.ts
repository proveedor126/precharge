import culqiFunction from './src/presentation/function/validate-charge.function';

import * as dotenv from 'dotenv';

dotenv.config();

export const lambdaHandler = culqiFunction;
