import { autoInjectable, inject } from 'tsyringe';
import UseCase from './use-case';
import ValidateChargeRequestDto from '../dto/validate-charge-request.dto';
import ValidateChargeResponseDto from '../dto/validate-charge-response.dto';
import ValidationCulqiError from '../../error/validation.culqi-error';
import { Response } from '../../constants/constants';
import ChargeValidatorFactory from './factory/charge-validator.factory';
import { SourceValidator, sources } from './charge-validator/charge-validator';

const testTokens: {
  [key: string]: { cardNumber: string; month: number; year: number; cvv: string };
} = {
  tkn_test_visa: {
    cardNumber: '4111111111111111',
    month: 9,
    year: 2025,
    cvv: '123',
  },
  tkn_test_mastercard: {
    cardNumber: '5111111111111118',
    month: 6,
    year: 2025,
    cvv: '039',
  },
  tkn_test_amex: {
    cardNumber: '371111111111114',
    month: 11,
    year: 2025,
    cvv: '2841',
  },
  tkn_test_diners: {
    cardNumber: '36111111111111',
    month: 4,
    year: 2025,
    cvv: '964',
  },
};

export const getSource = (isTestToken: boolean, sourceId: string): SourceValidator => {
  if (sourceId.length !== 25 && !isTestToken) {
    throw new ValidationCulqiError(Response.INVALID_SOURCE_ID_INEXISTENT_VALUE);
  }

  const source = sources[sourceId.substring(0, 3)];

  if (!source) throw new ValidationCulqiError(Response.INVALID_SOURCE_ID_INEXISTENT_VALUE);

  return source;
};

export const getIsTestToken = (sourceId: string): boolean => {
  return !!testTokens[sourceId];
};

@autoInjectable()
export default class ValidateChargeUseCase
  implements UseCase<ValidateChargeRequestDto, ValidateChargeResponseDto>
{
  constructor(@inject('ChargeValidatorFactory') private _factory: ChargeValidatorFactory) {}

  private getSource(isTestToken: boolean, sourceId: string): number {
    return getSource(isTestToken, sourceId);
  }

  private getIsTestToken(sourceId: string): boolean {
    return getIsTestToken(sourceId);
  }

  async execute(input: ValidateChargeRequestDto): Promise<ValidateChargeResponseDto> {
    const isTestToken = this.getIsTestToken(input.source_id);
    const source = this.getSource(isTestToken, input.source_id);

    await this._factory.get(source).validate(input);

    const result = new ValidateChargeResponseDto();
    result.success = true;

    return result;
  }
}
