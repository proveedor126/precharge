import LightTokenRepository from '../../../domain/repository/light-token.repository';
import LightToken from '../../../domain/entity/light-token.entity';
import TokenRepository from '../../../domain/repository/token.repository';
import Token from '../../../domain/entity/token.entity';
import { Response, products } from '../../../constants/constants';
import ValidationCulqiError from '../../../error/validation.culqi-error';
import ChargeValidator, { SourceValidator } from './charge-validator';
import UnexpectedError from '../../../error/unexpected-error';

export const getLightToken = async (
  refId: string,
  merchantId: number | undefined,
  lightTokenRepository: LightTokenRepository,
): Promise<LightToken | undefined> => {
  let lightToken: LightToken | undefined = undefined;

  if (merchantId === undefined) {
    throw new UnexpectedError();
  }

  lightToken = await lightTokenRepository.getByRefIdAndMerchantId(refId, merchantId);

  if (!lightToken) {
    throw new ValidationCulqiError(Response.TOKEN_EXPIRED_RF);
  }

  lightToken && lightTokenRepository.delete(lightToken.id);

  return lightToken;
};

export const getToken = async (
  refId: string,
  merchantId: number | undefined,
  tokenRepository: TokenRepository,
): Promise<Token> => {
  let token: Token | undefined = undefined;

  if (merchantId === undefined) {
    throw new UnexpectedError();
  }

  token = await tokenRepository.getByRefIdAndMerchantId(refId, merchantId);

  if (!token) {
    throw new ValidationCulqiError(Response.TOKEN_ID_NOT_FOUND);
  }

  if (!token.active) {
    throw new ValidationCulqiError(Response.TOKEN_USED_RF);
  }

  token.active = false;
  tokenRepository.update(token);

  const now = new Date();

  if (token?.expirationDate && token.expirationDate < now) {
    throw new ValidationCulqiError(Response.TOKEN_EXPIRED_RF);
  }

  return token;
};

export const getProductId = () => {
  return products.TOKEN;
};

export default abstract class BaseTokenChargeValidator extends ChargeValidator {
  constructor(
    _lightTokenRepository: LightTokenRepository,
    _tokenRepository: TokenRepository,
  ) {
    super(_lightTokenRepository, _tokenRepository);
  }

  protected getProductId(_?: number | undefined): number {
    return getProductId();
  }

  protected async getLightToken(
    refId: string,
    merchantId?: number,
  ): Promise<LightToken | undefined> {
    return await getLightToken(refId, merchantId, this._lightTokenRepository);
  }

  async getToken(refId: string, merchantId?: number): Promise<Token | undefined> {
    return await getToken(refId, merchantId, this._tokenRepository);
  }
}
