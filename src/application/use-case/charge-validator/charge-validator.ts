import { Response } from '../../../constants/constants';
import LightToken from '../../../domain/entity/light-token.entity';
import Token from '../../../domain/entity/token.entity';
import LightTokenRepository from '../../../domain/repository/light-token.repository';
import TokenRepository from '../../../domain/repository/token.repository';
import UnexpectedError from '../../../error/unexpected-error';
import ValidationCulqiError from '../../../error/validation.culqi-error';
import MerchantDataDto from '../../dto/merchant-data.dto';
import ValidateChargeRequestDto from '../../dto/validate-charge-request.dto';

export const operations: { [key: string]: number } = {
  CARD_VALIDATE_CHARGE: 69,
  CHARGE_CREATION: 40,
};

export enum SourceValidator {
  Token = 1,
  Card = 2,
  Yape = 3,
}

export const sources: { [key: string]: SourceValidator } = {
  tkn: SourceValidator.Token,
  crd: SourceValidator.Card,
  ype: SourceValidator.Yape,
};

export const products: { [key: string]: number } = {
  TOKEN: 4,
};

export const environments: { [key: string]: string } = {
  TEST: 'test',
  LIVE: 'live',
};

export const validateSourceEnvironment = (sourceId: string, productId: number): void => {
  const currentEnvironment = process.env.CULQI_ENVIRONMENT;

  if (currentEnvironment === environments.TEST && sourceId.includes(environments.LIVE)) {
    if (productId === products.TOKEN)
      throw new ValidationCulqiError(Response.TOKEN_NOT_IN_INTEGRATION);

    throw new ValidationCulqiError(Response.CARD_NOT_EXISTS_IN_INTEGRATION_RF);
  }

  if (currentEnvironment === environments.LIVE && sourceId.includes(environments.TEST)) {
    if (productId === products.TOKEN)
      throw new ValidationCulqiError(Response.TOKEN_NOT_IN_PRODUCTION);

    throw new ValidationCulqiError(Response.CARD_NOT_EXISTS_IN_PRODUCTION_RF);
  }
};

export const validateAmount = (
  operation: number,
  amount: string,
  currency: string,
  lightToken: LightToken | undefined,
  token: Token | undefined,
  merchantData: MerchantDataDto | undefined,
) => {
  const numberAmount = getAmount(amount);

  const brandId = lightToken ? lightToken?.brandId : token?.brandId;

  if (!brandId) throw new UnexpectedError();

  const terminal = merchantData?.merchantProcessorTerminal?.find(
    (terminal) => terminal.brandId === brandId && terminal.currencyIsoCode === currency,
  );

  if (!terminal)
    throw new ValidationCulqiError(Response.INVALID_CURRENCY_CODE_NOT_AVAILABLE_FOR_MERCHANT_RF);

  if (operation !== operations.CARD_VALIDATE_CHARGE) {
    const convertedAmount = numberAmount / Math.pow(10, terminal.currencyExponent);
    if (convertedAmount < 1 || convertedAmount > 99999)
      throw new ValidationCulqiError(Response.INVALID_AMOUNT);
  }
};

export const getOperation = (paymentType?: number): number => {
  return paymentType === 1 ? operations.CARD_VALIDATE_CHARGE : operations.CHARGE_CREATION;
};

export const getAmount = (amount: string): number => {
  return Number.parseInt(amount);
};

export default abstract class ChargeValidator {
  constructor(
    protected _lightTokenRepository: LightTokenRepository,
    protected _tokenRepository: TokenRepository,
  ) {}

  protected validateAmount(
    operation: number,
    amount: string,
    currecy: string,
    lightToken: LightToken | undefined,
    token: Token | undefined,
    merchantData: MerchantDataDto | undefined,
  ): void {
    validateAmount(operation, amount, currecy, lightToken, token, merchantData);
  }

  private validateSourceEnvironment(sourceId: string, productId: number): void {
    validateSourceEnvironment(sourceId, productId);
  }

  protected async validateCharge(
    lightToken: LightToken | undefined,
    token: Token | undefined,
    input: ValidateChargeRequestDto,
  ): Promise<void> {
    const operation = this.getOperation(input.payment_type);
    const productId = this.getProductId(input.product_id);

    this.validateSourceEnvironment(input.source_id, productId);
    this.validateAmount(
      operation,
      input.amount,
      input.currency_code,
      lightToken,
      token,
      input.merchantData,
    );
  }

  private getOperation(paymentType?: number): number {
    return getOperation(paymentType);
  }

  protected abstract getProductId(productId?: number): number;

  protected abstract getLightToken(
    refId: string,
    merchantId: number | undefined,
  ): Promise<LightToken | undefined>;

  protected abstract getToken(
    refId: string,
    merchanId: number | undefined,
  ): Promise<Token | undefined>;

  public abstract getSource(): SourceValidator;

  public async validate(input: ValidateChargeRequestDto): Promise<void> {
    const lightToken = await this.getLightToken(
      input.source_id,
      input.merchantData?.merchant?.id,
    );
    const token = await this.getToken(input.source_id, input.merchantData?.merchant?.id);

    this.validateCharge(lightToken, token, input);
  }
}
