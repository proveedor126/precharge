import { inject, injectable } from 'tsyringe';
import ValidationCulqiError from '../../../error/validation.culqi-error';
import { Response, yapeParameters } from '../../../constants/constants';
import LightToken from '../../../domain/entity/light-token.entity';
import Token from '../../../domain/entity/token.entity';
import ValidateChargeRequestDto from '../../dto/validate-charge-request.dto';
import { SourceValidator, getAmount } from './charge-validator';
import UnexpectedError from '../../../error/unexpected-error';
import MerchantDataDto from '../../dto/merchant-data.dto';
import LightTokenRepository from '../../../domain/repository/light-token.repository';
import TokenRepository from '../../../domain/repository/token.repository';
import BaseTokenChargeValidator from './base-token.charge-validator';

export const validateYapeCharge = (amount: number, currency: string): void => {
  if (amount > yapeParameters.maxAmount) {
    throw new ValidationCulqiError(Response.INVALID_AMOUNT);
  }

  if (currency?.toUpperCase() !== yapeParameters.currency.toUpperCase()) {
    throw new ValidationCulqiError(Response.INVALID_CURRENCY_CODE_YAPE);
  }
};

export const validateYapeAmount = (
  amount: number,
  lightToken: LightToken | undefined,
  token: Token | undefined,
) => {
  const yapeAmount = lightToken ? lightToken?.yapeAmount : token?.yapeAmount;

  if (yapeAmount === undefined) {
    throw new UnexpectedError();
  }

  if (amount !== yapeAmount) {
    throw new ValidationCulqiError(Response.INVALID_AMOUNT);
  }
};

@injectable()
export default class YapeChargeValidator extends BaseTokenChargeValidator {
  constructor(
    @inject('MockedLightTokenRepository') _lightTokenRepository: LightTokenRepository,
    @inject('PostgressTokenRepository') _tokenRepository: TokenRepository,
  ) {
    super(_lightTokenRepository, _tokenRepository);
  }

  validateAmount(
    operation: number,
    amount: string,
    currecy: string,
    lightToken: LightToken | undefined,
    token: Token | undefined,
    merchantData: MerchantDataDto | undefined,
  ): void {
    super.validateAmount(operation, amount, currecy, lightToken, token, merchantData);

    validateYapeCharge(getAmount(amount), currecy);
  }

  protected async validateCharge(
    lightToken: LightToken | undefined,
    token: Token | undefined,
    input: ValidateChargeRequestDto,
  ): Promise<void> {
    super.validateCharge(lightToken, token, input);
    validateYapeAmount(getAmount(input.amount), lightToken, token);
  }

  getSource(): SourceValidator {
    return SourceValidator.Yape;
  }
}
