import { inject, injectable } from 'tsyringe';
import LightTokenRepository from '../../../domain/repository/light-token.repository';
import LightToken from '../../../domain/entity/light-token.entity';
import TokenRepository from '../../../domain/repository/token.repository';
import Token from '../../../domain/entity/token.entity';
import { Response, products } from '../../../constants/constants';
import ValidationCulqiError from '../../../error/validation.culqi-error';
import ChargeValidator, { SourceValidator } from './charge-validator';

export const getLightToken = async (
  refId: string,
  lightTokenRepository: LightTokenRepository,
): Promise<LightToken | undefined> => {
  let lightToken: LightToken | undefined = undefined;
  lightToken = await lightTokenRepository.getByRefId(refId);
  lightToken && lightTokenRepository.delete(lightToken.id);
  return lightToken;
};

export const getToken = async (refId: string, tokenRepository: TokenRepository): Promise<Token> => {
  let token: Token | undefined = undefined;
  token = await tokenRepository.getByRefId(refId);

  if (!token) {
    throw new ValidationCulqiError(Response.TOKEN_ID_NOT_FOUND);
  }

  return token;
};

export const getProductId = (productId: number | undefined): number => {
  return productId !== undefined ? productId : products.TOKEN;
}

@injectable()
export default class CardChargeValidator extends ChargeValidator {
  constructor(
    @inject('MockedLightTokenRepository') _lightTokenRepository: LightTokenRepository,
    @inject('PostgressTokenRepository') _tokenRepository: TokenRepository,
  ) {
    super(_lightTokenRepository, _tokenRepository);
  }

  getSource(): SourceValidator {
    return SourceValidator.Card;
  }

  protected getProductId(productId?: number | undefined): number {
    return getProductId(productId);
  }

  protected async getLightToken(refId: string): Promise<LightToken | undefined> {
    return await getLightToken(refId, this._lightTokenRepository);
  }

  protected async getToken(refId: string): Promise<Token | undefined> {
    return await getToken(refId, this._tokenRepository);
  }
}
