import { inject, injectable } from 'tsyringe';
import LightTokenRepository from '../../../domain/repository/light-token.repository';
import TokenRepository from '../../../domain/repository/token.repository';
import BaseTokenChargeValidator from './base-token.charge-validator';
import { SourceValidator } from './charge-validator';

@injectable()
export default class TokenChargeValidator extends BaseTokenChargeValidator {
  public getSource(): SourceValidator {
    return SourceValidator.Token;
  }
  
  constructor(
    @inject('MockedLightTokenRepository') _lightTokenRepository: LightTokenRepository,
    @inject('PostgressTokenRepository') _tokenRepository: TokenRepository,
  ) {
    super(_lightTokenRepository, _tokenRepository);
  }
}
