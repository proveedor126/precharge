import { injectAll, injectable } from 'tsyringe';
import UnexpectedError from '../../../error/unexpected-error';
import ChargeValidator, { SourceValidator } from '../charge-validator/charge-validator';

@injectable()
export default class ChargeValidatorFactory {
  constructor(@injectAll('chargeValidators') private _chargeValidators: ChargeValidator[]) {}

  get(source: SourceValidator): ChargeValidator {
    const validator = this._chargeValidators.find(chargeValidator => chargeValidator.getSource() === source);

    if(!validator) {
        throw new UnexpectedError();
    }

    return validator;
  }
}
