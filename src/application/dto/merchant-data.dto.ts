import { z } from 'zod';

export const merchantDataSchema = z.object({
  merchant: z.object({
    id: z.number(),
  }),
  merchantProcessorTerminal: z.array(
    z.object({
      currencyIsoCode: z.string(),
      currencyExponent: z.number(),
      brandId: z.number(),
    }),
  ),
});

type MerchantDataDto = z.infer<typeof merchantDataSchema>;

export default MerchantDataDto;
