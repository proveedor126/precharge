import { z } from 'zod';
import { Response, countries } from '../../constants/constants';
import MerchantdataDto from './merchant-data.dto';
export const schema = 1;

const validCurrencies = ['PEN', 'USD'];
const booleanStrings = ['true', 'false'];

export const validateChargeRequestSchema = z.object({
  amount: z.coerce
    .number({
      required_error: `${Response.INVALID_AMOUNT}`,
      invalid_type_error: `${Response.INVALID_AMOUNT_LESS_THAN_9999900}`,
    },)
    .min(100, `${Response.INVALID_AMOUNT_GREATHER_THAN_100}`)
    .max(9999900, `${Response.INVALID_AMOUNT_LESS_THAN_9999900}`)
    .refine(
      (number: number) => Number.isInteger(number),
      `${Response.INVALID_AMOUNT_LESS_THAN_9999900}`,
    )
    .transform((x: number) => x.toString()),
  application_fee: z.coerce
    .number({
      invalid_type_error: `${Response.INVALID_APPLICATION_FEE}`,
    })
    .min(1, `${Response.INVALID_APPLICATION_FEE}`)
    .max(5000000, `${Response.INVALID_APPLICATION_FEE}`)
    .refine((number: number) => Number.isInteger(number), `${Response.INVALID_APPLICATION_FEE}`)
    .optional()
    .transform((x) => x?.toString()),
  currency_code: z
    .string({
      required_error: `${Response.INVALID_CURRENCY_CODE}`,
    })
    .min(1, `${Response.INVALID_CURRENCY_CODE}`)
    .refine(
      (currency: string) => validCurrencies.some((validCurrency) => validCurrency === currency),
      `${Response.INVALID_CURRENCY_CODE}`,
    ),
  source_id: z
    .string({ required_error: `${Response.INVALID_SOURCE_ID}` })
    .trim()
    .min(1, `${Response.INVALID_SOURCE_ID}`),
  description: z
    .string()
    .min(5, `${Response.DESCRIPTION_LENGTH_INVALID}`)
    .max(80, `${Response.DESCRIPTION_LENGTH_INVALID}`)
    .optional(),
  installments: z.coerce
    .number({
      invalid_type_error: `${Response.INVALID_INSTALLMENTS}`,
    })
    .min(0, `${Response.INVALID_INSTALLMENTS}`)
    .refine((number: number) => Number.isInteger(number), `${Response.INVALID_INSTALLMENTS}`)
    .transform((x: number) => x.toString())
    .optional(),
  email: z
    .string({
      required_error: `${Response.INVALID_CLIENT_EMAIL}`,
    })
    .email(`${Response.INVALID_CLIENT_EMAIL}`)
    .min(5, `${Response.INVALID_CLIENT_EMAIL}`)
    .max(50, `${Response.INVALID_CLIENT_EMAIL}`),
  capture: z
    .string()
    .refine(
      (capture) => booleanStrings.some((booleanString) => booleanString === capture?.toLowerCase()),
      `${Response.EMPTY_CAPTURE}`,
    )
    .optional(),
  metadata: z
    .record(
      z.string().min(0, `${Response.METADATA_INVALID}`).max(30, `${Response.METADATA_LIMIT_20}`),
      z.unknown(),
      {
        invalid_type_error: `${Response.METADATA_INVALID}`,
      },
    )
    .refine(
      (metadata: Record<string, unknown>) => Object.keys(metadata).length <= 20,
      `${Response.METADATA_LIMIT_20}`,
    )
    .refine((metadata: Record<string, unknown>) => {
      for (let key in metadata) {
        if (`${metadata[key]}`.length > 200) {
          return false;
        }
      }

      return true;
    }, `${Response.METADATA_LIMIT_KEY_30_CHARACTERS_RF}`)
    .optional(),
  antifraud_details: z
    .object({
      first_name: z
        .string()
        .min(2)
        .max(50)
        .regex(/^[^0-9±!@£$%^&*_+§¡€#¢§¶•ªº«\\<>-?:;|=.,]{1,50}$/)
        .optional(),
      last_name: z
        .string()
        .min(2)
        .max(50)
        .regex(/^[^0-9±!@£$%^&*_+§¡€#¢§¶•ªº«\\<>-?:;|=.,]{1,50}$/)
        .optional(),
      address: z.string().min(5).max(100).optional(),
      address_city: z.string().min(2).max(30).optional(),
      country_code: z
        .string()
        .min(2, `${Response.INVALID_COUNTRY_CODE}`)
        .max(2, `${Response.INVALID_COUNTRY_CODE}`)
        .regex(/[A-Z]{2}/, `${Response.INVALID_COUNTRY_CODE}`)
        .refine((country: string) => {
          if (!country) return true;

          if (countries[country]) return true;
        }, `${Response.INVALID_FILTER_CLIENT_COUNTRY_CODE}`)
        .optional(),
      phone_number: z.coerce
        .number({
          required_error: `${Response.INVALID_PHONE_NUMBER}`,
          invalid_type_error: `${Response.INVALID_PHONE_NUMBER}`,
        })
        .refine((phone: number) => {
          return phone && phone >= 10000 && phone <= 999999999999999;
        }, `${Response.INVALID_PHONE_NUMBER}`)
        .transform((phone: number) => phone.toString()),
    })
    .optional(),
  fraud_validation: z
    .string()
    .trim()
    .min(1, `${Response.INVALID_FRAUD_VALIDATE}`)
    .refine(
      (capture) => booleanStrings.some((booleanString) => booleanString === capture?.toLowerCase()),
      `${Response.INVALID_FRAUD_VALIDATE}`,
    )
    .optional(),
});

type ValidateChargeRequestDtoInterface = z.infer<typeof validateChargeRequestSchema>;

interface Authentication3DS {
  cavv?: string;
  directoryServerTransactionId?: string;
  eci?: string;
  protocolVersion?: string;
  xid?: string;
}

interface Antifraud {
  first_name?: string;
  last_name?: string;
  address?: string;
  address_city?: string;
  country_code?: string;
  phone_number: string;
}

export default class ValidateChargeRequestDto implements ValidateChargeRequestDtoInterface {
  amount: string;
  application_fee?: string;
  currency_code: string;
  source_id: string;
  description?: string;
  installments?: string;
  email: string;
  capture?: string;
  metadata?: Record<string, unknown>;
  authentication_3DS?: Authentication3DS;
  payment_type?: number;
  product_id?: number;
  channel_id?: number;
  transaction_id?: bigint;
  antifraud_details?: Antifraud;
  fraud_validation?: string;
  charge_suscription?: boolean;
  merchant?: string;
  reference_code?: string;
  processing_channel?: string;
  merchantData?: MerchantdataDto;
}
