import { SecretsManager } from '@aws-sdk/client-secrets-manager';
import { rcFile } from 'rc-config-loader';
import { z } from 'zod';

export const configSchema = z.object({
  secrets: z.record(
    z.object({
      namespaced: z.boolean(),
    }),
  ),
});

type KeyValue = {
  [key: string]: unknown;
};

export default class SettingsProvider {
  private _configFileName = 'culqifunction';
  private _obj: KeyValue = {};

  get<T>(key: string): T | undefined {
    return this._obj[key] as T;
  }

  async initialize(): Promise<void> {
    try {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const rawConfigContent = rcFile<any>(this._configFileName);
      const configContent = configSchema.parse(rawConfigContent);

      const keys = Object.keys(configContent.secrets);

      const secretManager = new SecretsManager({
        region: 'us-east-1',
      });

      await Promise.allSettled(
        keys.map(async (key) => {
          const value = await secretManager.getSecretValue({ SecretId: key });
          const data = JSON.parse(value.SecretString || '{}') as KeyValue;

          if (configContent.secrets[key].namespaced) {
            this._obj[key] = data;
          } else {
            Object.entries(data).forEach(([key, value]) => (this._obj[key] = value));
          }
        }),
      );
    } catch {
      throw new Error('Configuration from secrets could not be loaded');
    }
  }
}
