import { injectable } from "tsyringe";
import { unzip } from "zlib";

@injectable()
export default class TextDecompressor {
    async decompress(text: string): Promise<string> {
        return await new Promise((resolve, reject) => {
            const textBuffer = Buffer.from(text, 'hex');
            unzip(textBuffer, (err, buffer) => {
                if(err) {
                    reject(new Error(`Error at unzipping content from Merchant Header: ${err?.message}`));
                } else {
                    resolve(buffer.toString());
                }
            });
        });
    }
}