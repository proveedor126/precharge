import LightToken from '../../domain/entity/light-token.entity';
import LightTokenRepository from '../../domain/repository/light-token.repository';

const memoryTokens: {
  [key: string]: {
    id: string;
    refId: string;
    yapeAmount: number | undefined;
    brandId: number;
  };
} = {
  'tkn_001111111111111111111:45299': {
    id: 'tkn_001111111111111111111:45299',
    refId: 'tkn_0011111111111111111111',
    yapeAmount: undefined,
    brandId: 1,
  },
  'ype_0011111111111111111111:1': {
    id: 'ype_0011111111111111111111:1',
    refId: 'ype_0011111111111111111111',
    yapeAmount: 20000,
    brandId: 1,
  },
  'crd_001111111111111111111': {
    id: 'crd_001111111111111111111',
    refId: 'crd_001111111111111111111',
    yapeAmount: undefined,
    brandId: 1,
  },
};

export default class MockedLightTokenRepository implements LightTokenRepository {
  async getByRefIdAndMerchantId(
    refId: string,
    merchantId: number,
  ): Promise<LightToken | undefined> {
    const memoryLightToken = memoryTokens[`${refId}:${merchantId}`];

    if (!memoryLightToken) return undefined;

    const copiedToken = { ...memoryLightToken };

    return new LightToken(
      copiedToken.id,
      copiedToken.refId,
      copiedToken.yapeAmount,
      copiedToken.brandId,
    );
  }

  async getByRefId(refId: string): Promise<LightToken | undefined> {
    const memoryLightToken = memoryTokens[refId];

    if (!memoryLightToken) return undefined;

    const copiedToken = { ...memoryLightToken };

    return new LightToken(
      copiedToken.id,
      copiedToken.refId,
      copiedToken.yapeAmount,
      copiedToken.brandId,
    );
  }

  async delete(id: string): Promise<void> {
    const memoryLightToken = memoryTokens[id];

    if (!memoryLightToken) return;

    delete memoryTokens[id];
  }
}
