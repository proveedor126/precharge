import { injectable } from 'tsyringe';
import Token from '../../domain/entity/token.entity';
import TokenRepository from '../../domain/repository/token.repository';
import { Pool } from 'pg';

interface TokenData {
  id: number;
  ref_id: string;
  active: boolean;
  expirationdate: Date;
  merchantid: number;
  yapeamount?: number;
  brandid: number;
}

@injectable()
export default class PostgressTokenRepository implements TokenRepository {
  private _pool: Pool;

  private getPool() {
    return this._pool
      ? this._pool
      : new Pool({
          user: 'myusername',
          password: 'mypassword',
          database: 'dbcoretrx',
          host: 'docker.for.mac.localhost',
          port: 5432,
        });
  }

  async getByRefIdAndMerchantId(refId: string, merchantId: number): Promise<Token | undefined> {
    const client = await this.getPool().connect();

    try {
      const result = await client.query<TokenData>(
        'SELECT id, ref_id, active, merchantid, expirationdate, yapeamount, brandid FROM public.tokens WHERE ref_id=$1 AND merchantid=$2',
        [refId, merchantId],
      );

      if (result?.rows && result.rows.length === 0) {
        return undefined;
      }

      const data = result.rows[0];

      return new Token(
        data.id,
        data.ref_id,
        data.active,
        data.merchantid,
        data.expirationdate,
        data.yapeamount,
        data.brandid,
      );
    } catch (err) {
      throw err;
    } finally {
      client.release(true);
    }
  }
  async update(token: Token): Promise<void> {
    const client = await this.getPool().connect();

    try {
      await client.query('UPDATE public.tokens SET active=$1 WHERE id=$2', [
        token.active,
        token.id,
      ]);
    } catch (err) {
      throw err;
    } finally {
      client.release(true);
    }
  }

  async getByRefId(refId: string): Promise<Token | undefined> {
    const client = await this.getPool().connect();

    try {
      const result = await client.query<TokenData>(
        'SELECT id, ref_id, active, merchantid, expirationdate, yapeamount, brandid FROM public.tokens WHERE ref_id=$1',
        [refId],
      );

      if (result?.rows && result.rows.length === 0) {
        return undefined;
      }

      const data = result.rows[0];

      return new Token(
        data.id,
        data.ref_id,
        data.active,
        data.merchantid,
        data.expirationdate,
        data.yapeamount,
        data.brandid,
      );
    } catch (err) {
      throw err;
    } finally {
      client.release(true);
    }
  }
}
