import 'reflect-metadata';
import { container } from 'tsyringe';
import registerApplication from './application.di-container';
import registerInfrastructure from './infrastructure.di-container';

registerApplication(container);
registerInfrastructure(container);

export default container;
