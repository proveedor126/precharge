import MockedLightTokenRepository from '../repository/mocked-light-token.repository';
import PostgressTokenRepository from '../repository/postgress-token.repository';
import { DependencyContainer } from 'tsyringe';
import TextDecompressor from '../adapter/text-decompressor.adapter';

const registerInfrastructure = (container: DependencyContainer) => {
  container.register<TextDecompressor>('TextDecompressor', { useClass: TextDecompressor });

  container.register<MockedLightTokenRepository>('MockedLightTokenRepository', {
    useClass: MockedLightTokenRepository,
  });
  container.register<PostgressTokenRepository>('PostgressTokenRepository', {
    useClass: PostgressTokenRepository,
  });
};

export default registerInfrastructure;
