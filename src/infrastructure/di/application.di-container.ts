import { DependencyContainer } from 'tsyringe';
import ValidateChargeUseCase from '../../application/use-case/validate-charge.use-case';
import ConsoleLogger from '../logger/console.logger';
import { ValidateChargeController } from '../../presentation/controller/validate-charge.controller';
import CardChargeValidator from '../../application/use-case/charge-validator/card.charge-validator';
import TokenChargeValidator from '../../application/use-case/charge-validator/token.charge-validator';
import YapeChargeValidator from '../../application/use-case/charge-validator/yape.charge-validator';
import ChargeValidatorFactory from '../../application/use-case/factory/charge-validator.factory';

const registerApplication = (container: DependencyContainer) => {
  container.register<ValidateChargeUseCase>('ValidateChargeUseCase', {
    useClass: ValidateChargeUseCase,
  });
  container.register<ConsoleLogger>('ConsoleLogger', { useClass: ConsoleLogger });

  container.register<ValidateChargeController>('ValidateChargeController', {
    useClass: ValidateChargeController,
  });

  container.register<CardChargeValidator>('chargeValidators', {
    useClass: CardChargeValidator,
  });

  container.register<TokenChargeValidator>('chargeValidators', {
    useClass: TokenChargeValidator,
  });

  container.register<YapeChargeValidator>('chargeValidators', {
    useClass: YapeChargeValidator,
  });

  container.register<ChargeValidatorFactory>('ChargeValidatorFactory', {
    useClass: ChargeValidatorFactory,
  });
};

export default registerApplication;
