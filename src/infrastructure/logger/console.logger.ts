import { singleton } from 'tsyringe';

@singleton()
export default class ConsoleLogger {
  info(message: string, ...args: unknown[]) {
    const logDate = new Date();
    logDate.getUTCDate();

    console.log(
      JSON.stringify({
        _logLevel: 'INFO',
        logDate,
        msg: message,
        ...args,
      }),
    );
  }

  debug(message: string, ...args: unknown[]) {
    const logDate = new Date();
    logDate.getUTCDate();

    console.debug(
      JSON.stringify({
        _logLevel: 'DEBUG',
        logDate,
        msg: message,
        ...args,
      }),
    );
  }

  warning(message: string, ...args: unknown[]) {
    const logDate = new Date();
    logDate.getUTCDate();

    console.warn(
      JSON.stringify({
        _logLevel: 'INFO',
        msg: message,
        ...args,
      }),
    );
  }

  error(message: string, ...args: unknown[]) {
    const logDate = new Date();
    logDate.getUTCDate();

    console.error(
      JSON.stringify({
        _logLevel: 'ERROR',
        msg: message,
        ...args,
      }),
    );
  }
}
