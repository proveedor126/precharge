import Token from "../entity/token.entity";

export default interface TokenRepository {
    getByRefId(refId: string): Promise<Token | undefined>;
    getByRefIdAndMerchantId(refId: string, merchantId: number): Promise<Token | undefined>;
    update(token: Token): Promise<void>;
}