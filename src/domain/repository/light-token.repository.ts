import LightToken from '../entity/light-token.entity';

export default interface LightTokenRepository {
  getByRefId(refId: string): Promise<LightToken | undefined>;
  getByRefIdAndMerchantId(refId: string, merchant: number): Promise<LightToken | undefined>;
  delete(id: string): Promise<void>;
}
