export default class LightToken {
  constructor(
    public id: string,
    public refId: string,
    public yapeAmount: number | undefined,
    public brandId: number,
  ) {}
}
