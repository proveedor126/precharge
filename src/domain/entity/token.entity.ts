export default class Token {
  constructor(
    public id: number,
    public refId: string,
    public active: boolean,
    public merchantId: number,
    public expirationDate: Date |undefined,
    public yapeAmount: number | undefined,
    public brandId: number,
  ) {}
}
