export type ErrorData = {
  type?: string;
  code?: string;
  declineCode?: string;
  merchantMessage?: string;
  usermessage?: string;
  param?: string;
};

const errors: Record<number, any> = {
  1: {
    type: 'operacion_denegada',
    code: 'INV0001',
    merchantMessage: 'El código de comercio es inválido.',
  },
  2: {
    type: 'comercio_invalido',
    code: 'INV0002',
    merchantMessage: 'El comercio no fue encontrado.',
  },
  3: {
    type: 'comercio_invalido',
    code: 'INV0003',
    merchantMessage: 'El comercio no tiene una llave activa.',
  },
  4: { type: 'comercio_invalido', code: 'INV0004', merchantMessage: 'El comercio no está activo.' },
  5: {
    type: 'operacion_denegada',
    code: 'INV0005',
    merchantMessage: 'El número de pedido es inválido.',
  },
  6: {
    type: 'parametro_invalido',
    code: 'INV0006',
    merchantMessage: 'La información del comercio no coincide con la enviada. ',
  },
  7: {
    type: 'operacion_denegada',
    code: 'INV0007',
    merchantMessage: 'El código de moneda enviado es inválido.',
  },
  8: { type: 'parametro_invalido', code: 'INV0008', merchantMessage: 'La moneda no existe.' },
  9: {
    type: 'parametro_invalido',
    code: 'INV0009',
    merchantMessage: 'El comercio no puede aceptar la moneda enviada',
  },
  10: {
    type: 'operacion_denegada',
    code: 'INV0010',
    merchantMessage: 'El código de país es inválido.',
  },
  11: { type: 'operacion_denegada', code: 'INV0011', merchantMessage: 'La dirección es inválida.' },
  12: {
    type: 'operacion_denegada',
    code: 'INV0012',
    merchantMessage: 'El número de teléfono es inválido.',
  },
  13: {
    type: 'operacion_denegada',
    code: 'INV0013',
    merchantMessage: 'El correo electrónico es inválido. ',
  },
  14: {
    type: 'operacion_denegada',
    code: 'INV0014',
    merchantMessage: 'Los apellidos son inválidos.',
  },
  15: {
    type: 'operacion_denegada',
    code: 'INV0015',
    merchantMessage: 'Los nombres son inválidos.',
  },
  16: {
    type: 'parametro_invalido',
    code: 'INV0016',
    merchantMessage: 'La transacción ya existe. ',
  },
  17: { type: 'operacion_denegada', code: 'INV0017', merchantMessage: 'El monto es inválido.' },
  18: {
    type: 'operacion_denegada',
    code: 'REC0021',
    merchantMessage: 'La transaccion no existe.',
    userMessage: 'La compra no ha podido ser procesada. Contáctese con el comercio.',
  },
  19: {
    type: 'operacion_denegada',
    code: 'REC0023',
    merchantMessage: 'La transacción ha expirado.',
    userMessage: 'La compra no ha podido ser procesada. Contáctese con el comercio.',
  },
  20: {
    type: 'operacion_denegada',
    code: 'INV0020',
    merchantMessage: 'La transacción ha expirado.',
    userMessage: 'La compra no ha podido ser procesada. Contáctese con el comercio.',
  },
  21: {
    type: 'parametro_invalido',
    code: 'INV0021',
    merchantMessage: 'El producto enviado no existe.',
  },
  22: {
    type: 'operacion_denegada',
    code: 'INV0022',
    merchantMessage: 'La transacción no ha sido autorizada.',
  },
  23: {
    type: 'parametro_invalido',
    code: 'INV0023',
    merchantMessage: 'La información enviada no pudo ser descifrada.',
  },
  24: {
    type: 'parametro_invalido',
    code: 'INV0024',
    merchantMessage: 'No se pudo leer la información de la venta.',
  },
  25: {
    type: 'operacion_denegada',
    code: 'INV0025',
    merchantMessage: 'Parámetros recibidos inválidos',
  },
  26: { type: 'venta_registrada', code: 'REG0000', merchantMessage: 'Venta creada exitosamente.' },
  27: {
    type: 'operacion_denegada',
    code: 'INV9999',
    merchantMessage: 'Ha sucedido alguna excepción inesperada',
  },
  28: {
    type: 'operacion_denegada',
    code: 'DNGI0001',
    merchantMessage: 'Operacion denegada.',
    userMessage: 'La compra no ha podido ser procesada. Intente nuevamente ó utilice otra tarjeta.',
  },
  29: {
    type: 'operacion_denegada',
    code: 'INC0001',
    merchantMessage: 'Error inesperado, contactar a CULQI.',
    userMessage:
      'La compra no ha podido ser procesada. Intente más tarde o contáctese con el comercio.',
  },
  30: {
    type: 'operacion_denegada',
    code: 'REC0001',
    merchantMessage: 'Revisar configuración del comercio, contactar a CULQI.',
    userMessage:
      'La compra no ha podido ser procesada. Intente más tarde o contáctese con el comercio.',
  },
  31: {
    type: 'operacion_denegada',
    code: 'INC0002',
    merchantMessage: 'Error de comunicación, contactar a CULQI.',
    userMessage:
      'La compra no ha podido ser procesada. Intente más tarde o contáctese con el comercio.',
  },
  32: {
    type: 'operacion_denegada',
    code: 'INC0003',
    merchantMessage: 'Error de comunicación, contactar a CULQI.',
    userMessage:
      'La compra no ha podido ser procesada. Intente más tarde o contáctese con el comercio.',
  },
  33: {
    type: 'operacion_denegada',
    code: 'INC0004',
    merchantMessage: 'Codigo nuevo interno, contactar a CULQI.',
    userMessage: 'La compra no ha podido ser procesada. Intente nuevamente ó utilice otra tarjeta.',
  },
  34: {
    type: 'operacion_denegada',
    code: 'INC0005',
    merchantMessage: 'Error de comunicación, contactar a CULQI.',
    userMessage:
      'La compra no ha podido ser procesada. Intente más tarde o contáctese con el comercio.',
  },
  35: {
    type: 'operacion_denegada',
    code: 'INC0006',
    merchantMessage: 'Error de comunicación, contactar a CULQI.',
    userMessage:
      'La compra no ha podido ser procesada. Intente más tarde o contáctese con el comercio.',
  },
  36: {
    type: 'operacion_denegada',
    code: 'INC0007',
    merchantMessage: 'Error inesperado, contactar a CULQI.',
    userMessage:
      'La compra no ha podido ser procesada. Intente más tarde o contáctese con el comercio.',
  },
  37: {
    type: 'operacion_denegada',
    code: 'ANU0015',
    merchantMessage: 'Error de comunicación, contactar a CULQI.',
  },
  38: {
    type: 'operacion_denegada',
    code: 'ANU0016',
    merchantMessage: 'Error de comunicación, contactar a CULQI.',
  },
  39: {
    type: 'venta_exitosa',
    code: 'DEP0012',
    merchantMessage: 'La operación de venta ha sido autorizada exitosamente',
  },
  40: {
    type: 'venta_exitosa',
    code: 'DEP0013',
    merchantMessage: 'La operación de venta ha sido autorizada exitosamente',
  },
  41: {
    type: 'operacion_denegada',
    code: 'DNGA9999',
    merchantMessage:
      'Operación denegada. El cliente debe intentar nuevamente ó utilice otra tarjeta.',
    userMessage: 'Operación denegada. Intente nuevamente ó utilice otra tarjeta.',
  },
  42: {
    type: 'operacion_denegada',
    code: 'ANU9999',
    merchantMessage: 'No se realizó la operación, contactar a CULQI.',
  },
  43: {
    type: 'venta_exitosa',
    code: 'DEP9999',
    merchantMessage: 'La operación de venta ha sido autorizada exitosamente',
  },
  44: {
    type: 'operacion_denegada',
    code: 'ANU0017',
    merchantMessage: 'Error inesperado, contactar a CULQI.',
  },
  45: {
    type: 'venta_exitosa',
    code: 'DEP0014',
    merchantMessage: 'La operación de venta ha sido autorizada exitosamente',
  },
  46: {
    type: 'parametro_invalido',
    code: 'INV0026',
    merchantMessage: "El parámetro 'moneda' no puede ser nulo o vacio.",
    userMessage: "El parámetro 'moneda' no puede ser nulo o vacio.",
  },
  47: {
    type: 'parametro_invalido',
    code: 'INV0027',
    merchantMessage:
      "El parámetro 'moneda' debe de tener solo 3 caracteres alfabéticos en mayúsculas.",
    userMessage: "El parámetro 'moneda' debe de tener solo 3 caracteres alfabéticos en mayúsculas.",
  },
  48: {
    type: 'parametro_invalido',
    code: 'INV0028',
    merchantMessage: "El parámetro 'monto' no puede ser nulo o vacio.",
    userMessage: "El parámetro 'monto' no puede ser nulo o vacio.",
  },
  49: {
    type: 'parametro_invalido',
    code: 'INV0029',
    merchantMessage: "El parámetro 'monto' debe de tener solo entre 3 y 15 caracteres numéricos",
    userMessage: "El parámetro 'monto' debe de tener solo entre 3 y 15 caracteres numéricos",
  },
  50: {
    type: 'parametro_invalido',
    code: 'INV0030',
    merchantMessage: "El parámetro 'codigo_comercio' no puede ser nulo o vacio.",
    userMessage: "El parámetro 'codigo_comercio' no puede ser nulo o vacio.",
  },
  51: {
    type: 'parametro_invalido',
    code: 'INV0031',
    merchantMessage: "El parámetro 'codigo_comercio' debe de tener máximo 12 caracteres",
    userMessage: "El parámetro 'codigo_comercio' debe de tener máximo 12 caracteres",
  },
  52: {
    type: 'parametro_invalido',
    code: 'INV0032',
    merchantMessage: "El parámetro 'descripcion' no puede ser nulo o vacio.",
    userMessage: "El parámetro 'descripcion' no puede ser nulo o vacio.",
  },
  53: {
    type: 'parametro_invalido',
    code: 'INV0033',
    merchantMessage: "El parámetro 'descripcion' debe de tener entre 5 y 80 caracteres",
    userMessage: "El parámetro 'descripcion' debe de tener entre 5 y 80 caracteres",
  },
  54: {
    type: 'parametro_invalido',
    code: 'INV0034',
    merchantMessage: "El parámetro 'numero_pedido' no puede ser nulo o vacio.",
    userMessage: "El parámetro 'numero_pedido' no puede ser nulo o vacio.",
  },
  55: {
    type: 'parametro_invalido',
    code: 'INV0035',
    merchantMessage: "El parámetro 'numero_pedido' debe de tener entre 1 y 33 caracteres",
    userMessage: "El parámetro 'numero_pedido' debe de tener entre 1 y 33 caracteres",
  },
  56: {
    type: 'parametro_invalido',
    code: 'INV0036',
    merchantMessage: "El parámetro 'cod_pais' no puede ser nulo o vacio.",
    userMessage: "El parámetro 'cod_pais' no puede ser nulo o vacio.",
  },
  57: {
    type: 'parametro_invalido',
    code: 'INV0037',
    merchantMessage: "El parámetro 'cod_pais' debe de tener solo 2 caracteres alfabéticos",
    userMessage: "El parámetro 'cod_pais' debe de tener solo 2 caracteres alfabéticos",
  },
  58: {
    type: 'parametro_invalido',
    code: 'INV0038',
    merchantMessage: "El parámetro 'direccion' no puede ser nulo o vacio.",
    userMessage: "El parámetro 'direccion' no puede ser nulo o vacio.",
  },
  59: {
    type: 'parametro_invalido',
    code: 'INV0039',
    merchantMessage: "El parámetro 'direccion' debe de tener entre 5 y 100 caracteres",
    userMessage: "El parámetro 'direccion' debe de tener entre 5 y 100 caracteres",
  },
  60: {
    type: 'parametro_invalido',
    code: 'INV0040',
    merchantMessage: "El parámetro 'ciudad' no puede ser nulo o vacio.",
    userMessage: "El parámetro 'ciudad' no puede ser nulo o vacio.",
  },
  61: {
    type: 'parametro_invalido',
    code: 'INV0041',
    merchantMessage: "El parámetro 'ciudad' debe de tener entre 2 y 30 caracteres",
    userMessage: "El parámetro 'ciudad' debe de tener entre 2 y 30 caracteres",
  },
  62: {
    type: 'parametro_invalido',
    code: 'INV0042',
    merchantMessage: "El parámetro 'num_tel' no puede ser nulo o vacio.",
    userMessage: "El parámetro 'num_tel' no puede ser nulo o vacio.",
  },
  63: {
    type: 'parametro_invalido',
    code: 'INV0043',
    merchantMessage: "El parámetro 'num_tel' debe de tener entre 5 y 15 caracteres",
    userMessage: "El parámetro 'num_tel' debe de tener entre 5 y 15 caracteres",
  },
  64: {
    type: 'parametro_invalido',
    code: 'INV0044',
    merchantMessage: "El parámetro 'nombres' no puede ser nulo o vacio.",
    userMessage: "El parámetro 'nombres' no puede ser nulo o vacio.",
  },
  65: {
    type: 'parametro_invalido',
    code: 'INV0045',
    merchantMessage: "El parámetro 'nombres' debe de tener entre 2 y 15 caracteres",
    userMessage: "El parámetro 'nombres' debe de tener entre 2 y 15 caracteres",
  },
  66: {
    type: 'parametro_invalido',
    code: 'INV0046',
    merchantMessage: "El parámetro 'nombres' debe de tener un nombre válido.",
    userMessage: "El parámetro 'nombres' debe de tener un nombre válido.",
  },
  67: {
    type: 'parametro_invalido',
    code: 'INV0047',
    merchantMessage: "El parámetro 'apellidos' no puede ser nulo o vacio.",
    userMessage: "El parámetro 'apellidos' no puede ser nulo o vacio.",
  },
  68: {
    type: 'parametro_invalido',
    code: 'INV0048',
    merchantMessage: "El parámetro 'apellidos' debe de tener entre 2 y 15 caracteres",
    userMessage: "El parámetro 'apellidos' debe de tener entre 2 y 15 caracteres",
  },
  69: {
    type: 'parametro_invalido',
    code: 'INV0049',
    merchantMessage: "El parámetro 'apellidos' debe de tener un apellido válido",
    userMessage: "El parámetro 'apellidos' debe de tener un apellido válido",
  },
  70: {
    type: 'parametro_invalido',
    code: 'INV0050',
    merchantMessage: "El parámetro 'correo_electronico' no puede ser nulo o vacio.",
    userMessage: "El parámetro 'correo_electronico' no puede ser nulo o vacio.",
  },
  71: {
    type: 'parametro_invalido',
    code: 'INV0051',
    merchantMessage: "El parámetro 'correo_electronico' tiene que ser un email válido",
    userMessage: "El parámetro 'correo_electronico' tiene que ser un email válido",
  },
  72: {
    type: 'parametro_invalido',
    code: 'INV0052',
    merchantMessage: "El parámetro 'correo_electronico' debe de tener entre 5 y 50 caracteres",
    userMessage: "El parámetro 'correo_electronico' debe de tener entre 5 y 50 caracteres",
  },
  73: {
    type: 'parametro_invalido',
    code: 'INV0053',
    merchantMessage: "El parámetro 'id_usuario_comercio' no puede ser nulo o vacio.",
    userMessage: "El parámetro 'id_usuario_comercio' no puede ser nulo o vacio.",
  },
  74: {
    type: 'parametro_invalido',
    code: 'INV0054',
    merchantMessage: "El parámetro 'id_usuario_comercio' debe de tener entre 1 y 50 caracteres",
    userMessage: "El parámetro 'id_usuario_comercio' debe de tener entre 2 y 20 caracteres",
  },
  75: {
    type: 'operacion_denegada',
    code: 'REC0002',
    merchantMessage: 'Marca Inva?lida.',
    userMessage: 'La marca de su tarjeta es inválida. Intente nuevamente ó utilice otra tarjeta.',
  },
  76: {
    type: 'operacion_denegada',
    code: 'REC0020',
    merchantMessage: 'Número de tarjeta inválido.',
    userMessage: 'Su número de tarjeta es inválida. Intente nuevamente ó utilice otra tarjeta.',
  },
  77: {
    type: 'operacion_denegada',
    code: 'REC0019',
    merchantMessage: 'Falta configurar al comercio, contactar a CULQI.',
    userMessage:
      'La compra no ha podido ser procesada. Intente más tarde o contáctese con el comercio.',
  },
  78: {
    type: 'parametro_invalido',
    code: 'REC0003',
    merchantMessage: 'El Email ingresado por el comprador, es inválido.',
    userMessage: 'El correo electrónico que ingresó, es inválido. Inténtelo nuevamente.',
  },
  79: {
    type: 'parametro_invalido',
    code: 'REC0004',
    merchantMessage: 'El Email ingresado por el comprador, es inválido.',
    userMessage: 'El correo electrónico que ingresó, es inválido. Inténtelo nuevamente.',
  },
  80: {
    type: 'parametro_invalido',
    code: 'REC0005',
    merchantMessage: 'El Nombre ingresado por el comprador, es inválido.',
    userMessage: 'El nombre que ingresó, es inválido. Inténtelo nuevamente.',
  },
  81: {
    type: 'parametro_invalido',
    code: 'REC0006',
    merchantMessage: 'El Nombre ingresado por el comprador, es inválido.',
    userMessage: 'El nombre que ingresó, es inválido. Inténtelo nuevamente.',
  },
  82: {
    type: 'parametro_invalido',
    code: 'REC0007',
    merchantMessage: 'El Apellido ingresado por el comprador, es inválido.',
    userMessage: 'El apellido que ingresó, es inválido. Inténtelo nuevamente.',
  },
  83: {
    type: 'parametro_invalido',
    code: 'REC0008',
    merchantMessage: 'El Apellido ingresado por el comprador, es inválido.',
    userMessage: 'El apellido que ingresó es inválido. Inténtelo nuevamente.',
  },
  84: {
    type: 'operacion_denegada',
    code: 'REC0009',
    merchantMessage: 'El Número de tarjeta ingresado por el comprador, es inválido.',
    userMessage: 'El número de tarjeta, que ingresó, es inválido. Inténtelo nuevamente.',
  },
  89: {
    type: 'expiracion_invalida',
    code: 'REC0010',
    merchantMessage: 'El año de expiración de la tarjeta ingresada por el comprador, es inválido.',
    userMessage:
      'El año de expiración de su tarjeta, que ingresó, es inválido. Inténtelo nuevamente.',
  },
  90: {
    type: 'expiracion_invalida',
    code: 'REC0011',
    merchantMessage: 'El año de expiración de la tarjeta ingresada por el comprador, es inválido.',
    userMessage:
      'El año de expiración de su tarjeta, que ingresó, es inválido. Inténtelo nuevamente.',
  },
  91: {
    type: 'expiracion_invalida',
    code: 'REC0012',
    merchantMessage: 'El año de expiración de la tarjeta ingresada por el comprador, es inválido.',
    userMessage:
      'El año de expiración de su tarjeta, que ingresó, es inválido. Inténtelo nuevamente.',
  },
  92: {
    type: 'expiracion_invalida',
    code: 'REC0013',
    merchantMessage: 'El mes de expiración de la tarjeta ingresada por el comprador, es inválido.',
    userMessage:
      'El mes de expiración de su tarjeta, que ingresó, es inválido. Inténtelo nuevamente.',
  },
  93: {
    type: 'expiracion_invalida',
    code: 'REC0014',
    merchantMessage: 'El mes de expiración de la tarjeta ingresada por el comprador, es inválido.',
    userMessage:
      'El mes de expiración de su tarjeta, que ingresó, es inválido. Inténtelo nuevamente.',
  },
  94: {
    type: 'expiracion_invalida',
    code: 'REC0015',
    merchantMessage: 'El mes de expiración de la tarjeta ingresada por el comprador, es inválido.',
    userMessage:
      'El mes de expiración de su tarjeta, que ingresó, es inválido. Inténtelo nuevamente.',
  },
  95: {
    type: 'cvv_invalido',
    code: 'REC0016',
    merchantMessage:
      'El codigo de seguridad (CVV) de la tarjeta ingresada por el comprador, es inválido.',
    userMessage:
      'El código de seguridad (CVV) de su tarjeta, que ingresó, es inválido. Inténtelo nuevamente.',
  },
  96: {
    type: 'cvv_invalido',
    code: 'REC0017',
    merchantMessage:
      'El codigo de seguridad (CVV) de la tarjeta ingresada por el comprador, es inválido.',
    userMessage:
      'El código de seguridad (CVV) de su tarjeta, que ingresó, es inválido. Inténtelo nuevamente.',
  },
  97: {
    type: 'parametro_invalido',
    code: 'REC0018',
    merchantMessage: 'Ausencia de campo de verificación.',
    userMessage: 'No se pudo iniciar el proceso de compra con su tarjeta. Inténtelo nuevamente.',
  },
  98: { type: 'comercio_invalido', code: 'ANU0018', merchantMessage: 'El comercio no existe.' },
  99: {
    type: 'operacion_denegada',
    code: 'ANU0019',
    merchantMessage: 'Error en la petición, contactar a CULQI.',
  },
  100: {
    type: 'operacion_denegada',
    code: 'ANU0020',
    merchantMessage: 'La transacción no existe.',
  },
  101: {
    type: 'operacion_denegada',
    code: 'ANU0021',
    merchantMessage: 'Error de comunicación, contactar a CULQI.',
  },
  102: {
    type: 'operacion_denegada',
    code: 'ANU0022',
    merchantMessage: 'Error de comunicación, contactar a CULQI.',
  },
  103: {
    type: 'operacion_denegada',
    code: 'ANU0023',
    merchantMessage: 'La transacción no se encuentra autorizada, por ende, no se puede anular.',
  },
  104: {
    type: 'contactar_emisor',
    code: 'DNGE0001',
    merchantMessage: 'Contactar con entidad emisora de su tarjeta. ',
    userMessage: 'Contáctese con la entidad emisora de su tarjeta.',
  },
  105: {
    type: 'contactar_emisor',
    code: 'DNGE0002',
    merchantMessage: 'Exceso de actividad. ',
    userMessage: 'El emisor tiene exceso de procesamiento, intente nuevamete en algunos minutos.',
  },
  106: {
    type: 'contactar_emisor',
    code: 'DNGE0003',
    merchantMessage: 'Contactar con entidad emisora de su tarjeta. ',
    userMessage: 'Contáctese con la entidad emisora de su tarjeta.',
  },
  107: {
    type: 'contactar_emisor',
    code: 'DNGE0004',
    merchantMessage: 'Contactar con entidad emisora de su tarjeta. ',
    userMessage: 'Contáctese con la entidad emisora de su tarjeta.',
  },
  108: {
    type: 'contactar_emisor',
    code: 'DNGE0005',
    merchantMessage: 'Contactar con entidad emisora de su tarjeta. ',
    userMessage: 'Contáctese con la entidad emisora de su tarjeta.',
  },
  109: {
    type: 'contactar_emisor',
    code: 'DNGE0006',
    merchantMessage: 'Contactar con entidad emisora de su tarjeta. ',
    userMessage: 'Contáctese con la entidad emisora de su tarjeta.',
  },
  110: {
    type: 'contactar_emisor',
    code: 'DNGE0007',
    merchantMessage: 'Contactar con entidad emisora de su tarjeta. ',
    userMessage: 'Contáctese con la entidad emisora de su tarjeta.',
  },
  111: {
    type: 'contactar_emisor',
    code: 'DNGE0008',
    merchantMessage: 'Contactar con entidad emisora de su tarjeta. ',
    userMessage: 'Contáctese con la entidad emisora de su tarjeta.',
  },
  112: {
    type: 'contactar_emisor',
    code: 'DNGE0009',
    merchantMessage: 'Contactar con entidad emisora de su tarjeta. ',
    userMessage: 'Contáctese con la entidad emisora de su tarjeta.',
  },
  113: {
    type: 'contactar_emisor',
    code: 'DNGE0010',
    merchantMessage: 'Contactar con entidad emisora de su tarjeta. ',
    userMessage: 'Contáctese con la entidad emisora de su tarjeta.',
  },
  114: {
    type: 'contactar_emisor',
    code: 'DNGE0011',
    merchantMessage: 'Contactar con entidad emisora de su tarjeta. ',
    userMessage: 'Contáctese con la entidad emisora de su tarjeta.',
  },
  115: {
    type: 'contactar_emisor',
    code: 'DNGE0012',
    merchantMessage: 'Contactar con entidad emisora de su tarjeta. ',
    userMessage: 'Contáctese con la entidad emisora de su tarjeta.',
  },
  116: {
    type: 'contactar_emisor',
    code: 'DNGE0013',
    merchantMessage: 'Contactar con entidad emisora de su tarjeta. ',
    userMessage: 'Contáctese con la entidad emisora de su tarjeta.',
  },
  117: {
    type: 'cvv_invalido',
    code: 'DNGE0014',
    merchantMessage: 'Código de seguridad inválido.',
    userMessage: 'Su tarjeta es inválida. Contáctese con la entidad emisora de su tarjeta.',
  },
  118: {
    type: 'fondos_insuficientes',
    code: 'DNGE0015',
    merchantMessage: 'Fondos Insuficientes.',
    userMessage: 'Su tarjeta no tiene fondos suficientes. Recárgela o intente con otra tarjeta.',
  },
  119: {
    type: 'operacion_denegada',
    code: 'DNGE0016',
    merchantMessage: 'La tarjeta es cuotas.',
    userMessage:
      'Su tarjeta solo soporta pago en cuotas. Seleccione una cuota ó intente con otra tarjeta.',
  },
  120: {
    type: 'operacion_denegada',
    code: 'DNGE0017',
    merchantMessage: 'La tarjeta ha superado la cantidad máxima de transacciones en el día. ',
    userMessage:
      'Su tarjeta superó el número de compras permitidas. Intente nuevamente con otra tarjeta.',
  },
  121: {
    type: 'contactar_emisor',
    code: 'DNGE0018',
    merchantMessage: 'Operación denegada.',
    userMessage: 'Operación denegada. Intente nuevamente ó utilice otra tarjeta.',
  },
  122: {
    type: 'contactar_emisor',
    code: 'DNGE0019',
    merchantMessage: 'Operación denegada por emisor',
    userMessage:
      'Operación denegada por la entidad emisora de su tarjeta. Intente nuevamente ó utilice otra tarjeta.',
  },
  123: {
    type: 'contactar_emisor',
    code: 'DNGE0020',
    merchantMessage: 'Operación denegada por emisor',
    userMessage:
      'Operación denegada por la entidad emisora de su tarjeta. Intente nuevamente ó utilice otra tarjeta.',
  },
  124: {
    type: 'contactar_emisor',
    code: 'DNGE0021',
    merchantMessage: 'Operación denegada por emisor',
    userMessage:
      'Operación denegada por la entidad emisora de su tarjeta. Intente nuevamente ó utilice otra tarjeta.',
  },
  125: {
    type: 'contactar_emisor',
    code: 'DNGE0022',
    merchantMessage: 'Operación denegada por emisor',
    userMessage:
      'Operación denegada por la entidad emisora de su tarjeta. Intente nuevamente ó utilice otra tarjeta.',
  },
  126: {
    type: 'contactar_emisor',
    code: 'DNGE0023',
    merchantMessage: 'Operación denegada por emisor',
    userMessage:
      'Operación denegada por la entidad emisora de su tarjeta. Intente nuevamente ó utilice otra tarjeta.',
  },
  127: {
    type: 'operacion_denegada',
    code: 'DNGE0024',
    merchantMessage: 'Retener tarjeta.',
    userMessage: 'Contáctese con la entidad emisora de su tarjeta.',
  },
  128: {
    type: 'contactar_emisor',
    code: 'DNGE0025',
    merchantMessage: 'Tarjeta con restricciones de crédito.',
    userMessage:
      'Su tarjeta tiene restricciones. Contáctese con la entidad emisora de su tarjeta. ',
  },
  129: {
    type: 'contactar_emisor',
    code: 'DNGE0026',
    merchantMessage: 'Tarjeta con restricciones de débito.',
    userMessage:
      'Su tarjeta tiene restricciones. Contáctese con la entidad emisora de su tarjeta. ',
  },
  130: {
    type: 'operacion_denegada',
    code: 'DNGE0027',
    merchantMessage: 'Tarjeta no autenticada.',
    userMessage:
      'Su tarjeta no ha podido ser autenticada. Intente nuevamente ó utilice otra tarjeta.',
  },
  131: {
    type: 'operacion_denegada',
    code: 'DNGE0028',
    merchantMessage: 'Tarjeta restringida.',
    userMessage:
      'Su tarjeta tiene restricciones. Contáctese con la entidad emisora de su tarjeta. ',
  },
  132: {
    type: 'operacion_denegada',
    code: 'DNGE0029',
    merchantMessage: 'Operación denegada por emisor',
    userMessage:
      'Operación denegada por la entidad emisora de su tarjeta. Intente nuevamente ó utilice otra tarjeta.',
  },
  133: {
    type: 'tarjeta_perdida',
    code: 'DNGE0030',
    merchantMessage: 'Tarjeta reportada como perdida.',
    userMessage: 'Contáctese con la entidad emisora de su tarjeta.',
  },
  134: {
    type: 'tarjeta_robada',
    code: 'DNGE0031',
    merchantMessage: 'Tarjeta reportada como robada.',
    userMessage: 'Contáctese con la entidad emisora de su tarjeta.',
  },
  135: {
    type: 'contactar_emisor',
    code: 'DNGE0032',
    merchantMessage: 'Operación no permitida para esta tarjeta.',
    userMessage:
      'Operación no permitida para esta tarjeta. Contáctese con la entidad emisora de su tarjeta. ',
  },
  136: {
    type: 'contactar_emisor',
    code: 'DNGE0033',
    merchantMessage: 'Operación no permitida para esta tarjeta.',
    userMessage:
      'Operación no permitida para esta tarjeta. Contáctese con la entidad emisora de su tarjeta. ',
  },
  137: {
    type: 'contactar_emisor',
    code: 'DNGE0034',
    merchantMessage: 'Operación no permitida para esta tarjeta.',
    userMessage:
      'Operación no permitida para esta tarjeta. Contáctese con la entidad emisora de su tarjeta. ',
  },
  138: {
    type: 'contactar_emisor',
    code: 'DNGE0035',
    merchantMessage: 'Tarjeta Inválida.',
    userMessage: 'Su tarjeta es inválida. Contáctese con la entidad emisora de su tarjeta. ',
  },
  139: {
    type: 'contactar_emisor',
    code: 'DNGE0036',
    merchantMessage: 'Tarjeta Inválida.',
    userMessage: 'Su tarjeta es inválida. Contáctese con la entidad emisora de su tarjeta. ',
  },
  140: {
    type: 'operacion_denegada',
    code: 'DNGE0037',
    merchantMessage: 'Operación denegada por emisor',
    userMessage:
      'Operación denegada por la entidad emisora de su tarjeta. Intente nuevamente ó utilice otra tarjeta.',
  },
  141: {
    type: 'operacion_denegada',
    code: 'DNGE0038',
    merchantMessage: 'Operación denegada por emisor',
    userMessage:
      'Operación denegada por la entidad emisora de su tarjeta. Intente nuevamente ó utilice otra tarjeta.',
  },
  142: {
    type: 'operacion_denegada',
    code: 'DNGE0039',
    merchantMessage: 'Operación denegada por emisor',
    userMessage:
      'Operación denegada por la entidad emisora de su tarjeta. Intente nuevamente ó utilice otra tarjeta.',
  },
  143: {
    type: 'operacion_denegada',
    code: 'DNGE0040',
    merchantMessage: 'Operación denegada por emisor',
    userMessage:
      'Operación denegada por la entidad emisora de su tarjeta. Intente nuevamente ó utilice otra tarjeta.',
  },
  144: {
    type: 'operacion_denegada',
    code: 'DNGE0041',
    merchantMessage: 'Operación denegada por emisor',
    userMessage:
      'Operación denegada por la entidad emisora de su tarjeta. Intente nuevamente ó utilice otra tarjeta.',
  },
  145: {
    type: 'operacion_denegada',
    code: 'DNGE0042',
    merchantMessage: 'Operación denegada por emisor',
    userMessage:
      'Operación denegada por la entidad emisora de su tarjeta. Intente nuevamente ó utilice otra tarjeta.',
  },
  146: {
    type: 'operacion_denegada',
    code: 'DNGE0043',
    merchantMessage: 'Operación denegada por emisor',
    userMessage:
      'Operación denegada por la entidad emisora de su tarjeta. Intente nuevamente ó utilice otra tarjeta.',
  },
  147: {
    type: 'operacion_denegada',
    code: 'DNGE0044',
    merchantMessage: 'Operación denegada por emisor',
    userMessage:
      'Operación denegada por la entidad emisora de su tarjeta. Intente nuevamente ó utilice otra tarjeta.',
  },
  148: {
    type: 'operacion_denegada',
    code: 'DNGE0045',
    merchantMessage: 'Operación denegada por emisor',
    userMessage:
      'Operación denegada por la entidad emisora de su tarjeta. Intente nuevamente ó utilice otra tarjeta.',
  },
  149: {
    type: 'operacion_denegada',
    code: 'DNGE0046',
    merchantMessage: 'Operación denegada por emisor',
    userMessage:
      'Operación denegada por la entidad emisora de su tarjeta. Intente nuevamente ó utilice otra tarjeta.',
  },
  150: {
    type: 'cvv_invalido',
    code: 'DNGE0047',
    merchantMessage: 'Código de seguridad (CVV) no coincide',
    userMessage:
      'El código de seguridad (CVV) de su tarjeta es inválido. Intente nuevamente ó utilice otra tarjeta.',
  },
  151: {
    type: 'cvv_invalido',
    code: 'DNGE0048',
    merchantMessage: 'Código de seguridad (CVV) no procesado por la entidad emisora de la tarjeta ',
    userMessage:
      'El código de seguridad (CVV) no ha podido ser procesado. Intente nuevamente ó utilice otra tarjeta.',
  },
  152: {
    type: 'cvv_invalido',
    code: 'DNGE0049',
    merchantMessage:
      'Código de seguridad (CVV) no procesado por la entidad emisora de la tarjeta  ',
    userMessage:
      'El código de seguridad (CVV) no ha podido ser procesado. Intente nuevamente ó utilice otra tarjeta.',
  },
  153: {
    type: 'cvv_invalido',
    code: 'DNGE0050',
    merchantMessage: 'Código de seguridad (CVV) no ingresado',
    userMessage:
      'El código de seguridad (CVV) no ha sido ingresado. Intente nuevamente ó utilice otra tarjeta.',
  },
  154: {
    type: 'cvv_invalido',
    code: 'DNGE0051',
    merchantMessage:
      'Código de seguridad (CVV) no reconocido por la entidad emisora de la tarjeta ',
    userMessage:
      'El código de seguridad (CVV) de su tarjeta no es reconocido. Intente nuevamente ó utilice otra tarjeta.',
  },
  155: {
    type: 'tarjeta_vencida',
    code: 'DNGE0052',
    merchantMessage: 'Tarjeta vencida.',
    userMessage:
      'Su tarjeta esta vencida. Contáctese con su emisor de tarjeta ó intente nuevamente con otra tarjeta.',
  },
  156: {
    type: 'tarjeta_vencida',
    code: 'DNGE0053',
    merchantMessage: 'Tarjeta vencida.',
    userMessage:
      'Su tarjeta esta vencida. Contáctese con su emisor de tarjeta ó intente nuevamente con otra tarjeta.',
  },
  157: {
    type: 'operacion_denegada',
    code: 'DNGA0001',
    merchantMessage: 'La tarjeta no es de la marca VISA',
    userMessage: 'Operación denegada. Intente nuevamente ó utilice otra tarjeta.',
  },
  158: {
    type: 'operacion_denegada',
    code: 'DNGA0002',
    merchantMessage: 'La tarjeta es inválida',
    userMessage: 'Su tarjeta es inválida. Intente nuevamente ó utilice otra tarjeta.',
  },
  159: {
    type: 'operacion_denegada',
    code: 'DNGA0003',
    merchantMessage: 'Error en formato interno',
    userMessage: 'Por favor, intente nuevamente ó utilice otra tarjeta. ',
  },
  160: {
    type: 'operacion_denegada',
    code: 'DNGA0004',
    merchantMessage: 'Transaccion inválida',
    userMessage: 'Por favor, intente nuevamente ó utilice otra tarjeta. ',
  },
  161: {
    type: 'tarjeta_vencida',
    code: 'DNGA0005',
    merchantMessage: 'Tarjeta vencida.',
    userMessage:
      'Su tarjeta esta vencida. Contáctese con su emisor de tarjeta ó intente nuevamente con otra tarjeta.',
  },
  162: {
    type: 'operacion_denegada',
    code: 'DNGA0006',
    merchantMessage: 'Tarjeta excede monto máximo de compra',
    userMessage: 'Su tarjeta excedió el monto por compra. Intente nuevamente con otra tarjeta.',
  },
  163: {
    type: 'operacion_denegada',
    code: 'DNGA0007',
    merchantMessage: 'El Emisor no responde, no procesa.',
    userMessage: 'Por favor, intente nuevamente ó utilice otra tarjeta. ',
  },
  164: {
    type: 'operacion_denegada',
    code: 'DNGA0008',
    merchantMessage: 'El Adquirente no responde, no procesa.',
    userMessage: 'Por favor, intente nuevamente ó utilice otra tarjeta. ',
  },
  165: {
    type: 'operacion_denegada',
    code: 'DNGA0009',
    merchantMessage: 'Contactar con el comercio. ',
    userMessage:
      'Operación denegada. Intente nuevamente ó utilice otra tarjeta. Si el problema persiste, contáctese con el Comercio.',
  },
  166: {
    type: 'operacion_denegada',
    code: 'DNGA0010',
    merchantMessage:
      'El monto de la transacción supera el valor máximo permitido para operaciones virtuales.',
    userMessage:
      'El monto de la compra supera el valor máximo permitido. Contáctese con el Comercio.',
  },
  167: {
    type: 'operacion_denegada',
    code: 'DNGA0011',
    merchantMessage: 'El monto de la transacción supera el valor máximo permitido.',
    userMessage:
      'El monto de la compra supera el valor máximo permitido. Contáctese con el Comercio.',
  },
  168: {
    type: 'operacion_denegada',
    code: 'DNGA0012',
    merchantMessage: 'El monto de la transacción no llega al mínimo permitido.',
    userMessage: 'El monto de la compra no llega al mínimo permitido. Contáctese con el Comercio.',
  },
  169: {
    type: 'operacion_denegada',
    code: 'DNGA0013',
    merchantMessage: 'El comercio ha superado la cantidad máxima de transacciones en el día.',
    userMessage: 'Operación denegada. Contáctese con el Comercio.',
  },
  170: {
    type: 'operacion_denegada',
    code: 'DNGA0014',
    merchantMessage: 'Módulo antifraude',
    userMessage: 'Operación denegada. Contáctese con el Comercio.',
  },
  171: {
    type: 'operacion_denegada',
    code: 'DNGA0015',
    merchantMessage: 'Módulo antifraude',
    userMessage: 'Operación denegada. Contáctese con el Comercio.',
  },
  172: {
    type: 'operacion_denegada',
    code: 'DNGA0016',
    merchantMessage: 'Respuesta del módulo antifraude inválida.',
    userMessage:
      'Operación denegada. Intente nuevamente ó utilice otra tarjeta. Si el problema persiste, contáctese con el Comercio.',
  },
  173: {
    type: 'operacion_denegada',
    code: 'DNGA0017',
    merchantMessage: 'No hay respuesta del módulo antifraude.',
    userMessage:
      'Operación denegada. Intente nuevamente ó utilice otra tarjeta. Si el problema persiste, contáctese con el Comercio.',
  },
  174: {
    type: 'operacion_denegada',
    code: 'DNGA0018',
    merchantMessage: 'Violación de seguridad.',
    userMessage:
      'Operación denegada. Intente nuevamente ó utilice otra tarjeta. Si el problema persiste, contáctese con el Comercio.',
  },
  175: {
    type: 'operacion_denegada',
    code: 'DNGA0019',
    merchantMessage: 'Comercio mal configurado, contactar a CULQI.',
    userMessage:
      'La compra no ha podido ser procesada. Intente más tarde o contáctese con el comercio.',
  },
  176: {
    type: 'operacion_denegada',
    code: 'DNGA0020',
    merchantMessage: 'Error interno en el adquirente.',
    userMessage:
      'La compra no ha podido ser procesada. Intente más tarde o contáctese con el comercio.',
  },
  177: {
    type: 'operacion_denegada',
    code: 'DNGA0021',
    merchantMessage: 'Error interno en el adquirente.',
    userMessage:
      'La compra no ha podido ser procesada. Intente más tarde o contáctese con el comercio.',
  },
  178: {
    type: 'operacion_denegada',
    code: 'DNGA0022',
    merchantMessage: 'Error interno en el adquirente.',
    userMessage:
      'La compra no ha podido ser procesada. Intente más tarde o contáctese con el comercio.',
  },
  179: {
    type: 'operacion_denegada',
    code: 'DNGA0023',
    merchantMessage: 'Error interno en el adquirente.',
    userMessage: 'Operación denegada. Intente nuevamente.',
  },
  180: {
    type: 'operacion_denegada',
    code: 'DNGA0024',
    merchantMessage: 'Entidad emisora de la tarjeta no disponible para la autorización.',
    userMessage:
      'Operación denegada. Su entidad emisora no esta disponible. Intente nuevamente ó utilice otra tarjeta.',
  },
  181: {
    type: 'operacion_denegada',
    code: 'DNGA0025',
    merchantMessage: 'Entidad emisora de la tarjeta no disponible para la autenticación.',
    userMessage:
      'Operación denegada. Su entidad emisora no esta disponible. Intente nuevamente ó utilice otra tarjeta.',
  },
  182: {
    type: 'operacion_denegada',
    code: 'DNGA0026',
    merchantMessage: 'Transacción duplicada.',
    userMessage: 'Operación denegada. Intente nuevamente ó utilice otra tarjeta.',
  },
  183: {
    type: 'operacion_denegada',
    code: 'DNGA0027',
    merchantMessage: 'Comunicación duplicada.',
    userMessage: 'Operación denegada. Intente nuevamente ó utilice otra tarjeta.',
  },
  184: {
    type: 'operacion_denegada',
    code: 'DNGA0028',
    merchantMessage: 'Formato de mensaje erroneo.',
    userMessage: 'Operación denegada. Intente nuevamente ó utilice otra tarjeta.',
  },
  185: {
    type: 'operacion_denegada',
    code: 'DNGA0029',
    merchantMessage: 'Error en el envío de parámetros, contactar a CULQI.',
    userMessage:
      'La compra no ha podido ser procesada. Intente más tarde o contáctese con el comercio.',
  },
  186: {
    type: 'operacion_denegada',
    code: 'DNGA0030',
    merchantMessage: 'Número de pedido del comercio duplicado, contactar a CULQI.',
    userMessage:
      'La compra no ha podido ser procesada. Intente más tarde o contáctese con el comercio.',
  },
  187: {
    type: 'operacion_denegada',
    code: 'DNGA0031',
    merchantMessage: 'Comercio inhabilitada, contactar a CULQI.',
    userMessage:
      'La compra no ha podido ser procesada. Intente más tarde o contáctese con el comercio.',
  },
  188: {
    type: 'operacion_denegada',
    code: 'DNGA0032',
    merchantMessage: 'El comercio no está configurado, contactar a CULQI. ',
    userMessage:
      'La compra no ha podido ser procesada. Intente más tarde o contáctese con el comercio.',
  },
  189: {
    type: 'operacion_denegada',
    code: 'DNGA0033',
    merchantMessage: 'Se canceló/interrumpio el proceso de pago. ',
    userMessage: 'La compra no ha podido ser procesada. Intente nuevamente.',
  },
  193: {
    type: 'operacion_denegada',
    code: 'DNGA0034',
    merchantMessage: 'Sin respuesta del autorizador, contactar a CULQI.',
    userMessage:
      'La compra no ha podido ser procesada. Intente nuevamente ó utilice otra tarjeta. Si el problema persiste, contáctese con el Comercio.',
  },
  194: {
    type: 'operacion_denegada',
    code: 'DNGA0035',
    merchantMessage: 'eTicket inválido.',
    userMessage:
      'La compra no ha podido ser procesada. Intente nuevamente ó utilice otra tarjeta. Si el problema persiste, contáctese con el Comercio.',
  },
  195: {
    type: 'operacion_denegada',
    code: 'DNGA0036',
    merchantMessage: 'Valor ECI inválido.',
    userMessage:
      'La compra no ha podido ser procesada. Intente nuevamente ó utilice otra tarjeta. Si el problema persiste, contáctese con el Comercio.',
  },
  196: {
    type: 'operacion_denegada',
    code: 'DNGA0037',
    merchantMessage: 'No activó la opción enviar al autorizador.',
    userMessage:
      'La compra no ha podido ser procesada. Intente más tarde o contáctese con el comercio.',
  },
  197: {
    type: 'operacion_denegada',
    code: 'DNGA0038',
    merchantMessage: 'Intento de pago fuera del tiempo permitido.',
    userMessage: 'Operación denegada. Intente nuevamente ó utilice otra tarjeta.',
  },
  198: {
    type: 'operacion_denegada',
    code: 'DNGA0039',
    merchantMessage: 'Datos originales distintos.',
    userMessage: 'Operación denegada. Intente nuevamente ó utilice otra tarjeta.',
  },
  199: {
    type: 'operacion_denegada',
    code: 'DNGA0040',
    merchantMessage: 'Código de referencia repetido.',
    userMessage: 'Operación denegada. Intente nuevamente ó utilice otra tarjeta.',
  },
  200: {
    type: 'operacion_denegada',
    code: 'DNGA0041',
    merchantMessage: 'Transaccción inválida.',
    userMessage: 'Por favor, intente nuevamente ó utilice otra tarjeta. ',
  },
  201: {
    type: 'operacion_denegada',
    code: 'DNGA0042',
    merchantMessage: 'Emisor inválido.',
    userMessage:
      'Operación denegada. Su entidad emisora no esta disponible. Intente nuevamente ó utilice otra tarjeta.',
  },
  202: {
    type: 'operacion_denegada',
    code: 'DNGA0043',
    merchantMessage: 'Transaccion no permitida.',
    userMessage: 'Operación denegada. Intente nuevamente ó utilice otra tarjeta.',
  },
  203: {
    type: 'operacion_denegada',
    code: 'DNGA0044',
    merchantMessage: "Cuenta del comercio 'from', invalida o no existe.",
    userMessage:
      'La compra no ha podido ser procesada. Intente más tarde o contáctese con el comercio.',
  },
  204: {
    type: 'operacion_denegada',
    code: 'DNGA0045',
    merchantMessage: 'No se puede enrutar la transaccion.',
    userMessage:
      'La compra no ha podido ser procesada. Intente nuevamente ó utilice otra tarjeta. Si el problema persiste, contáctese con el Comercio.',
  },
  205: {
    type: 'operacion_denegada',
    code: 'DNGA0046',
    merchantMessage: 'El ciclo de vida de la autorizacion, es invalida.',
    userMessage:
      'La compra no ha podido ser procesada. Intente nuevamente ó utilice otra tarjeta. Si el problema persiste, contáctese con el Comercio.',
  },
  206: {
    type: 'operacion_denegada',
    code: 'DNGA0047',
    merchantMessage: 'Operación denegada por emisor.',
    userMessage: 'Operación no permitida. Contáctese con la entidad emisora de su tarjeta.',
  },
  207: {
    type: 'operacion_denegada',
    code: 'DNGA0048',
    merchantMessage: "Cuenta del comercio 'to', invalida o no existe.",
    userMessage:
      'La compra no ha podido ser procesada. Intente más tarde o contáctese con el comercio.',
  },
  208: {
    type: 'operacion_denegada',
    code: 'DNGA0049',
    merchantMessage: 'Monto Invalido.',
    userMessage:
      'El monto de su compra es inválido. Intente nuevamente ó utilice otra tarjeta. Si el problema persiste, contáctese con el Comercio.',
  },
  209: {
    type: 'operacion_denegada',
    code: 'DNGA0050',
    merchantMessage: 'Sistema No disponible, contactar a CULQI.',
    userMessage:
      'La compra no ha podido ser procesada. Intente nuevamente ó utilice otra tarjeta. Si el problema persiste, contáctese con el Comercio.',
  },
  210: {
    type: 'operacion_denegada',
    code: 'DNGA0051',
    merchantMessage: 'Permiso Denegado, contactar a CULQI.',
    userMessage:
      'La compra no ha podido ser procesada. Intente más tarde o contáctese con el comercio.',
  },
  211: {
    type: 'operacion_denegada',
    code: 'DNGA0052',
    merchantMessage: 'Llave Denegada, contactar a CULQI.',
    userMessage:
      'La compra no ha podido ser procesada. Intente más tarde o contáctese con el comercio.',
  },
  212: {
    type: 'operacion_denegada',
    code: 'DNGA0053',
    merchantMessage: 'Número de Referencia Invalido',
    userMessage: 'Operación denegada. Intente nuevamente ó utilice otra tarjeta.',
  },
  213: {
    type: 'expiracion_invalida',
    code: 'DNGA0054',
    merchantMessage: 'Fecha de Expiracion Invalida.',
    userMessage:
      'La fecha de expiración de su tarjeta es inválida. Intente nuevamente ó utilice otra tarjeta.',
  },
  214: {
    type: 'operacion_denegada',
    code: 'DNGA0055',
    merchantMessage: 'Moneda Invalida, contactar a CULQI.',
    userMessage:
      'La compra no ha podido ser procesada. Intente nuevamente. Si el problema persiste, contáctese con el Comercio.',
  },
  215: {
    type: 'operacion_denegada',
    code: 'DNGA0056',
    merchantMessage: 'Fecha Invalida, contactar a CULQI.',
    userMessage:
      'La compra no ha podido ser procesada. Intente nuevamente. Si el problema persiste, contáctese con el Comercio.',
  },
  216: {
    type: 'operacion_denegada',
    code: 'DNGA0057',
    merchantMessage: 'Hora Invalida, contactar a CULQI.',
    userMessage:
      'La compra no ha podido ser procesada. Intente nuevamente. Si el problema persiste, contáctese con el Comercio.',
  },
  217: {
    type: 'operacion_denegada',
    code: 'DNGA0058',
    merchantMessage: 'Diferido Invalido',
    userMessage: 'El diferido de su compra es inválido. Intente nuevamente ó utilice otra tarjeta.',
  },
  218: {
    type: 'operacion_denegada',
    code: 'DNGA0059',
    merchantMessage: 'Cuota Invalida',
    userMessage: 'La cuota de su compra es inválida. Intente nuevamente ó utilice otra tarjeta.',
  },
  219: {
    type: 'operacion_denegada',
    code: 'DNGA0060',
    merchantMessage: 'Monto Invalido',
    userMessage:
      'El monto de su compra es inválido. Intente nuevamente ó utilice otra tarjeta. Si el problema persiste, contáctese con el Comercio.',
  },
  220: {
    type: 'operacion_denegada',
    code: 'DNGA0061',
    merchantMessage: 'Tarjeta de Credito Invalida.',
    userMessage: 'Su tarjeta es inválida. Intente nuevamente ó utilice otra tarjeta.',
  },
  221: {
    type: 'operacion_denegada',
    code: 'DNGA0062',
    merchantMessage: 'Comercio Invalido, contactar a CULQI.',
    userMessage:
      'La compra no ha podido ser procesada. Intente más tarde o contáctese con el comercio.',
  },
  222: {
    type: 'operacion_denegada',
    code: 'DNGA0063',
    merchantMessage: 'Mensaje del protocolo de autenticación SecureCoce',
    userMessage: 'La compra no ha podido ser procesada. Intente nuevamente ó utilice otra tarjeta.',
  },
  223: {
    type: 'operacion_denegada',
    code: 'DNGA0064',
    merchantMessage: 'Error de cifrado A, contactar a CULQI.',
    userMessage:
      'La compra no ha podido ser procesada. Intente nuevamente. Si el problema persiste, contáctese con el Comercio.',
  },
  224: {
    type: 'operacion_denegada',
    code: 'DNGA0065',
    merchantMessage: 'Error de cifrado B, contactar a CULQI.',
    userMessage:
      'La compra no ha podido ser procesada. Intente nuevamente. Si el problema persiste, contáctese con el Comercio.',
  },
  225: {
    type: 'operacion_denegada',
    code: 'DNGA0066',
    merchantMessage: 'Marca Inva?lida',
    userMessage: 'La marca de su tarjeta es inválida. Intente nuevamente ó utilice otra tarjeta.',
  },
  226: {
    type: 'operacion_denegada',
    code: 'DNGA0067',
    merchantMessage: 'Proceso Inva?lido',
    userMessage: 'La compra no ha podido ser procesada. Intente nuevamente ó utilice otra tarjeta.',
  },
  227: {
    type: 'operacion_denegada',
    code: 'DNGA0068',
    merchantMessage: 'El Número de Referencia es inválido',
    userMessage: 'La compra no ha podido ser procesada. Intente nuevamente ó utilice otra tarjeta.',
  },
  228: {
    type: 'operacion_denegada',
    code: 'DNGA0069',
    merchantMessage: 'Número de Tarjeta vacío, contactar a CULQI.',
    userMessage:
      'La compra no ha podido ser procesada. Intente nuevamente. Si el problema persiste, contáctese con el Comercio.',
  },
  229: {
    type: 'operacion_denegada',
    code: 'DNGA0070',
    merchantMessage: 'Número de Referencia Duplicada',
    userMessage: 'Operación denegada. Intente nuevamente ó utilice otra tarjeta.',
  },
  230: {
    type: 'operacion_denegada',
    code: 'DNGA0071',
    merchantMessage: 'Error de Procesamiento, contactar a CULQI.',
    userMessage:
      'La compra no ha podido ser procesada. Intente nuevamente. Si el problema persiste, contáctese con el Comercio.',
  },
  231: {
    type: 'operacion_denegada',
    code: 'DNGA0072',
    merchantMessage: 'Error de Validación',
    userMessage: 'La compra no ha podido ser procesada. Intente nuevamente ó utilice otra tarjeta.',
  },
  232: {
    type: 'operacion_denegada',
    code: 'DNGA0073',
    merchantMessage: 'El tipo de proceso es inválido, contactar a CULQI.',
    userMessage:
      'La compra no ha podido ser procesada. Intente nuevamente. Si el problema persiste, contáctese con el Comercio.',
  },
  233: {
    type: 'operacion_denegada',
    code: 'DNGA0074',
    merchantMessage: 'El tipo de proceso es inválido, contactar a CULQI.',
    userMessage:
      'La compra no ha podido ser procesada. Intente nuevamente. Si el problema persiste, contáctese con el Comercio.',
  },
  234: {
    type: 'operacion_denegada',
    code: 'DNGA0075',
    merchantMessage: 'El co?digo cliente es inválido, contactar a CULQI.',
    userMessage:
      'La compra no ha podido ser procesada. Intente nuevamente. Si el problema persiste, contáctese con el Comercio.',
  },
  235: {
    type: 'operacion_denegada',
    code: 'DNGA0076',
    merchantMessage: 'El co?digo pai?s es inválido, contactar a CULQI.',
    userMessage:
      'La compra no ha podido ser procesada. Intente nuevamente. Si el problema persiste, contáctese con el Comercio.',
  },
  236: {
    type: 'operacion_denegada',
    code: 'DNGA0077',
    merchantMessage: 'Código de seguridad (CVV) es inválido.',
    userMessage:
      'El código de seguridad (CVV) de su tarjeta es inválido. Intente nuevamente ó utilice otra tarjeta.',
  },
  237: {
    type: 'operacion_denegada',
    code: 'DNGA0078',
    merchantMessage: 'Código de seguridad (CVV) es inválido.',
    userMessage:
      'El código de seguridad (CVV) de su tarjeta es inválido. Intente nuevamente ó utilice otra tarjeta.',
  },
  238: {
    type: 'operacion_denegada',
    code: 'DNGA0079',
    merchantMessage: 'Fecha de Expiracion Inválida.',
    userMessage:
      'La fecha de expiración de su tarjeta es inválida. Intente nuevamente ó utilice otra tarjeta.',
  },
  239: {
    type: 'operacion_denegada',
    code: 'DNGA0080',
    merchantMessage: 'Monto Inválido.',
    userMessage:
      'El monto de su compra es inválido. Intente nuevamente ó utilice otra tarjeta. Si el problema persiste, contáctese con el Comercio.',
  },
  240: {
    type: 'operacion_denegada',
    code: 'DNGA0081',
    merchantMessage: 'Monto Inválido.',
    userMessage:
      'El monto de su compra es inválido. Intente nuevamente ó utilice otra tarjeta. Si el problema persiste, contáctese con el Comercio.',
  },
  241: {
    type: 'operacion_denegada',
    code: 'DNGA0082',
    merchantMessage: 'Cuota Invalida.',
    userMessage: 'La cuota de su compra es inválida. Intente nuevamente ó utilice otra tarjeta.',
  },
  242: {
    type: 'operacion_denegada',
    code: 'DNGA0083',
    merchantMessage: 'Codigo de Comercio Facilitador ausente, contactar a CULQI.',
    userMessage:
      'La compra no ha podido ser procesada. Intente más tarde o contáctese con el comercio.',
  },
  243: {
    type: 'expiracion_invalida',
    code: 'DNGA0084',
    merchantMessage: 'Tarjeta vencida.',
    userMessage:
      'Su tarjeta esta vencida. Contáctese con su emisor de tarjeta ó intente nuevamente con otra tarjeta.',
  },
  244: {
    type: 'venta_exitosa',
    code: 'AUT0000',
    merchantMessage: 'La operación de venta ha sido autorizada exitosamente',
    userMessage: 'Su compra ha sido exitosa.',
  },
  245: {
    type: 'devolucion_exitosa',
    code: 'ANU0000',
    merchantMessage: 'La transacción ha sido anulada exitosamente con el emisor.',
  },
  246: {
    type: 'operacion_denegada',
    code: 'ANU0001',
    merchantMessage: 'La transacción no ha podido ser anulada.',
  },
  247: {
    type: 'operacion_denegada',
    code: 'ANU0002',
    merchantMessage: 'El código de autorización es inválido.',
  },
  248: {
    type: 'operacion_denegada',
    code: 'ANU0003',
    merchantMessage: 'El código de operación es inválido, contactar a CULQI.',
  },
  249: {
    type: 'operacion_denegada',
    code: 'ANU0004',
    merchantMessage: 'El mensaje procesado es inválido, contactar a CULQI.',
  },
  250: {
    type: 'operacion_denegada',
    code: 'ANU0005',
    merchantMessage: 'El mensaje procesado es inválido, contactar a CULQI.',
  },
  251: {
    type: 'operacion_denegada',
    code: 'ANU0006',
    merchantMessage: 'El mensaje procesado es inválido, contactar a CULQI.',
  },
  252: {
    type: 'operacion_denegada',
    code: 'ANU0007',
    merchantMessage: 'El mensaje procesado es inválido, contactar a CULQI.',
  },
  253: {
    type: 'operacion_denegada',
    code: 'ANU0008',
    merchantMessage: 'El mensaje procesado es inválido, contactar a CULQI.',
  },
  254: {
    type: 'operacion_denegada',
    code: 'ANU0009',
    merchantMessage: 'El mensaje procesado es inválido, contactar a CULQI.',
  },
  255: {
    type: 'operacion_denegada',
    code: 'ANU0010',
    merchantMessage: 'No se realizó la operación, contactar a CULQI.',
  },
  256: {
    type: 'operacion_denegada',
    code: 'ANU0011',
    merchantMessage: 'La transacción ya fue anulada con anterioridad.',
  },
  257: {
    type: 'operacion_denegada',
    code: 'ANU0012',
    merchantMessage: 'El mensaje enviado es inválido, contactar a CULQI.',
  },
  258: {
    type: 'operacion_denegada',
    code: 'ANU0013',
    merchantMessage: 'La transacción no existe.',
  },
  259: {
    type: 'operacion_denegada',
    code: 'ANU0014',
    merchantMessage: 'El mensaje enviado es inválido, contactar a CULQI.',
  },
  260: {
    type: 'venta_exitosa',
    code: 'DEP0000',
    merchantMessage: 'La operación de venta ha sido autorizada exitosamente',
  },
  261: {
    type: 'venta_exitosa',
    code: 'DEP0001',
    merchantMessage: 'La operación de venta ha sido autorizada exitosamente',
  },
  262: {
    type: 'venta_exitosa',
    code: 'DEP0002',
    merchantMessage: 'La operación de venta ha sido autorizada exitosamente',
  },
  263: {
    type: 'venta_exitosa',
    code: 'DEP0003',
    merchantMessage: 'La operación de venta ha sido autorizada exitosamente',
  },
  264: {
    type: 'venta_exitosa',
    code: 'DEP0004',
    merchantMessage: 'La operación de venta ha sido autorizada exitosamente',
  },
  265: {
    type: 'venta_exitosa',
    code: 'DEP0005',
    merchantMessage: 'La operación de venta ha sido autorizada exitosamente',
  },
  266: {
    type: 'venta_exitosa',
    code: 'DEP0006',
    merchantMessage: 'La operación de venta ha sido autorizada exitosamente',
  },
  267: {
    type: 'venta_exitosa',
    code: 'DEP0007',
    merchantMessage: 'La operación de venta ha sido autorizada exitosamente',
  },
  268: {
    type: 'venta_exitosa',
    code: 'DEP0008',
    merchantMessage: 'La operación de venta ha sido autorizada exitosamente',
  },
  269: {
    type: 'venta_exitosa',
    code: 'DEP0009',
    merchantMessage: 'La operación de venta ha sido autorizada exitosamente',
  },
  270: {
    type: 'venta_exitosa',
    code: 'DEP0010',
    merchantMessage: 'La operación de venta ha sido autorizada exitosamente',
  },
  271: {
    type: 'venta_exitosa',
    code: 'DEP0011',
    merchantMessage: 'La operación de venta ha sido autorizada exitosamente',
  },
  272: {
    code: 'INV0055',
    merchantMessage: "El parámetro 'num_tel' debe de contener sólo números.",
  },
  273: {
    type: 'denegada_fraude',
    code: 'FRA0001',
    merchantMessage:
      'Operación rechazada debido a que tiene una alta probabilidad de ser fraudulenta.',
    userMessage: 'Operación rechazada debido a un alto riesgo de fraude.',
  },
  274: {
    type: 'denegada_fraude',
    code: 'FRA0002',
    merchantMessage:
      'Operación rechazada debido a que el usuario ha realizado operaciones fraudulentas previamente.',
    userMessage: 'Su usuario no puede realizar compras.',
  },
  275: {
    type: 'denegada_fraude',
    code: 'FRA0003',
    merchantMessage:
      'Operación rechazada debido a que se han realizado operaciones fraudulentas con la misma tarjeta.',
    userMessage: 'Su tarjeta está registrada como no confiable.',
  },
  276: {
    type: 'denegada_fraude',
    code: 'FRA0004',
    merchantMessage:
      'Operación rechazada debido a que el cliente utiliza una tarjeta de alto riesgo de fraude o extranjera.',
    userMessage: 'Por favor, intente realizar la compra de nuevo con una tarjeta local.',
  },
  300: {
    type: 'parametro_invalido',
    code: 'INV0056',
    merchantMessage: 'Moneda invalida.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  301: {
    type: 'parametro_invalido',
    code: 'INV0057',
    merchantMessage:
      'Has ingresado un pedido inválido. Ya existe una transacción con el mismo código de pedido y debe ser único por cada intento de cargo.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  302: {
    type: 'parametro_invalido',
    code: 'INV0058',
    merchantMessage: 'Cargo exitoso.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  303: {
    type: 'parametro_invalido',
    code: 'INV0059',
    merchantMessage: 'Error Nucleo',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  304: {
    type: 'parametro_invalido',
    code: 'INV0060',
    merchantMessage: 'Moneda vacia.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  305: {
    type: 'parametro_invalido',
    code: 'INV0061',
    merchantMessage: 'Formato de moneda inválido. Debe cumplir el ISO 4217.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  306: {
    type: 'parametro_invalido',
    code: 'INV0062',
    merchantMessage: 'Monto vacío.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  307: {
    type: 'parametro_invalido',
    code: 'INV0063',
    merchantMessage:
      'Has ingresado un monto inválido. El número debe ser un entero sin punto decimal. p. ej: 199.00 sería 19900.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  308: {
    type: 'parametro_invalido',
    code: 'INV0064',
    merchantMessage: 'Descripción vacía.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  309: {
    type: 'parametro_invalido',
    code: 'INV0065',
    merchantMessage:
      'Has ingresado una descripción inválida. Usa menos de 80 caracteres y más de 5.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  310: {
    type: 'parametro_invalido',
    code: 'INV0066',
    merchantMessage: 'Número pedido vacío.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  311: {
    type: 'parametro_invalido',
    code: 'INV0067',
    merchantMessage: 'Número pedido invalido.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  312: {
    type: 'parametro_invalido',
    code: 'INV0068',
    merchantMessage: 'Codigo pais invalido.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  313: {
    type: 'parametro_invalido',
    code: 'INV0069',
    merchantMessage:
      'Has enviado un código de país inválido. Debes cumplir el ISO-3166-1. p. ej: PE (Perú).',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  314: {
    type: 'parametro_invalido',
    code: 'INV0070',
    merchantMessage: 'Direccción vacía.',
    userMessage: 'Has ingresado una dirección inválida.',
  },
  315: {
    type: 'parametro_invalido',
    code: 'INV0071',
    merchantMessage:
      'Has ingresado una dirección inválida. Usa menos de 100 caracteres y más de 5.',
    userMessage: 'Has ingresado una dirección inválida.',
  },
  316: {
    type: 'parametro_invalido',
    code: 'INV0072',
    merchantMessage: 'Ciudad Vacía.',
    userMessage: 'Ciudad vacía.',
  },
  317: {
    type: 'parametro_invalido',
    code: 'INV0073',
    merchantMessage:
      'Has ingresado una ciudad inválida. Usa menos de 30 caracteres y más de 2. p. ej: Lima, Arequipa, Santiago.',
    userMessage: 'Has ingresado una ciudad inválida.',
  },
  318: {
    type: 'parametro_invalido',
    code: 'INV0074',
    merchantMessage: 'Teléfono vacío.',
    userMessage: 'Teléfono vacío.',
  },
  319: {
    type: 'parametro_invalido',
    code: 'INV0075',
    merchantMessage: 'Has ingresado un nombre inválido.',
    userMessage: 'Has ingresado un nombre inválido.',
  },
  320: {
    type: 'parametro_invalido',
    code: 'INV0076',
    merchantMessage: 'Has ingresado un nombre inválido.',
    userMessage: 'Has ingresado un nombre inválido.',
  },
  321: {
    type: 'parametro_invalido',
    code: 'INV0077',
    merchantMessage: 'Has ingresado un apellido inválido.',
    userMessage: 'Has ingresado un apellido inválido.',
  },
  322: {
    type: 'parametro_invalido',
    code: 'INV0078',
    merchantMessage: 'Has ingresado un apellido inválido.',
    userMessage: 'Has ingresado un apellido inválido.',
  },
  323: {
    type: 'parametro_invalido',
    code: 'INV0079',
    merchantMessage: 'Has ingresado un apellido inválido.',
    userMessage: 'Has ingresado un apellido inválido.',
  },
  324: {
    type: 'parametro_invalido',
    code: 'INV0080',
    merchantMessage: 'Correo vaciío.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  325: {
    type: 'parametro_invalido',
    code: 'INV0081',
    merchantMessage: 'Correo invalido',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  326: {
    type: 'parametro_invalido',
    code: 'INV0082',
    merchantMessage: 'Correo longitud.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  327: {
    type: 'parametro_invalido',
    code: 'INV0083',
    merchantMessage: 'Usuario vacío.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  328: {
    type: 'parametro_invalido',
    code: 'INV0084',
    merchantMessage: 'usuario longitud.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  329: {
    type: 'parametro_invalido',
    code: 'INV0085',
    merchantMessage:
      'La marca de la tarjeta proporcionada no es soportada en tu cuenta por el momento. Si necesitas ',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  330: {
    type: 'parametro_invalido',
    code: 'INV0086',
    merchantMessage: 'Correo nulo',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  331: {
    type: 'parametro_invalido',
    code: 'INV0087',
    merchantMessage: 'Correo vacío',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  332: {
    type: 'parametro_invalido',
    code: 'INV0088',
    merchantMessage: 'Has ingresado un nombre inválido.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  333: {
    type: 'parametro_invalido',
    code: 'INV0089',
    merchantMessage: 'Has ingresado un nombre inválido.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  334: {
    type: 'parametro_invalido',
    code: 'INV0090',
    merchantMessage: 'Has ingresado un apellido inválido.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  335: {
    type: 'parametro_invalido',
    code: 'INV0091',
    merchantMessage: 'Has ingresado un apellido inválido.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  336: {
    type: 'parametro_invalido',
    code: 'INV0092',
    merchantMessage: 'El año de expiración de la tarjeta es inválido.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  337: {
    type: 'parametro_invalido',
    code: 'INV0093',
    merchantMessage: 'El año de expiración de la tarjeta es inválido.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  338: {
    type: 'parametro_invalido',
    code: 'INV0094',
    merchantMessage: 'El año de expiración de la tarjeta es inválido.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  339: {
    type: 'parametro_invalido',
    code: 'INV0095',
    merchantMessage: 'El año de expiración de la tarjeta es inválido.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  340: {
    type: 'parametro_invalido',
    code: 'INV0096',
    merchantMessage: 'El mes de expiración de la tarjeta es inválido.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  341: {
    type: 'parametro_invalido',
    code: 'INV0097',
    merchantMessage: 'El mes de expiración de la tarjeta es inválido.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  342: {
    type: 'parametro_invalido',
    code: 'INV0098',
    merchantMessage: 'El mes de expiración de la tarjeta es inválido.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  343: {
    type: 'parametro_invalido',
    code: 'INV0099',
    merchantMessage: 'No estás enviando el cvv.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  344: {
    type: 'parametro_invalido',
    code: 'INV0100',
    merchantMessage:
      'Has ingresado un número de tarjeta inválido. Debe contener solo números enteros.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  345: {
    type: 'parametro_invalido',
    code: 'INV0101',
    merchantMessage: 'tarjeta internacional',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  346: {
    type: 'parametro_invalido',
    code: 'INV0102',
    merchantMessage: 'Api Key invalido.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  347: {
    type: 'parametro_invalido',
    code: 'INV0103',
    merchantMessage: 'Merchant invalido.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  348: {
    type: 'parametro_invalido',
    code: 'INV0104',
    merchantMessage: 'Mercahnt inactivo.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  349: {
    type: 'parametro_invalido',
    code: 'INV0105',
    merchantMessage: 'Error al crear token.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  350: {
    type: 'parametro_invalido',
    code: 'INV0106',
    merchantMessage: 'Error de conexión',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  351: {
    type: 'parametro_invalido',
    code: 'INV0107',
    merchantMessage: 'Token no encontrado.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  352: {
    type: 'parametro_invalido',
    code: 'INV0108',
    merchantMessage: 'rechazada fraude.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  353: {
    type: 'parametro_invalido',
    code: 'INV0109',
    merchantMessage: 'No hay header autorización.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  354: {
    type: 'parametro_invalido',
    code: 'INV0110',
    merchantMessage: 'Autenticación inválida.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  355: {
    type: 'parametro_invalido',
    code: 'INV0111',
    merchantMessage: 'Merchant code inválido.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  356: {
    type: 'parametro_invalido',
    code: 'INV0112',
    merchantMessage: 'Llave de comercio inválida.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  357: {
    type: 'parametro_invalido',
    code: 'INV0113',
    merchantMessage: 'Cargo no encontrado.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  358: {
    type: 'parametro_invalido',
    code: 'INV0114',
    merchantMessage: 'Cargo no autorizado.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  359: {
    type: 'parametro_invalido',
    code: 'INV0115',
    merchantMessage: 'Plan existe.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  360: {
    type: 'parametro_invalido',
    code: 'INV0116',
    merchantMessage: 'Interval invalido.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  361: {
    type: 'parametro_invalido',
    code: 'INV0117',
    merchantMessage: 'Interval count invalido',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  362: {
    type: 'parametro_invalido',
    code: 'INV0118',
    merchantMessage: 'Interval count longitud.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  363: {
    type: 'parametro_invalido',
    code: 'INV0119',
    merchantMessage: 'Limit longitud.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  364: {
    type: 'parametro_invalido',
    code: 'INV0120',
    merchantMessage: 'FT interval count error.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  365: {
    type: 'parametro_invalido',
    code: 'INV0121',
    merchantMessage: 'FT Interval ',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  366: {
    type: 'parametro_invalido',
    code: 'INV0122',
    merchantMessage: 'Plan error.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  367: {
    type: 'parametro_invalido',
    code: 'INV0123',
    merchantMessage: 'El plan no existe',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  368: {
    type: 'parametro_invalido',
    code: 'INV0124',
    merchantMessage: 'El comercio no ha actualizado a la versión 2',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  369: {
    type: 'parametro_invalido',
    code: 'INV0125',
    merchantMessage: 'El JSON enviado en el Request body no cuenta con el formato esperado',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  370: {
    type: 'parametro_invalido',
    code: 'INV0126',
    merchantMessage: 'El método HTTP ingresado no está habilitado para este endpoint.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  371: {
    type: 'parametro_invalido',
    code: 'INV0127',
    merchantMessage: 'El Content-Type ingresado no está habilitado para este endpoint.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  372: {
    type: 'parametro_invalido',
    code: 'INV0128',
    merchantMessage: 'La autenticación no es válida para procesar su solicitud.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  373: {
    type: 'parametro_invalido',
    code: 'INV0129',
    merchantMessage: 'Se está procesando una transacción con la misma información (Duplicado).',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  374: {
    type: 'parametro_invalido',
    code: 'INV0130',
    merchantMessage: 'El iin solicitado no existe.',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  379: {
    type: 'parametro_invalido',
    code: 'INV0134',
    merchantMessage: 'Ya existe un usuario registrado con este correo electrónico',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  380: {
    type: 'parametro_invalido',
    code: 'INV0135',
    merchantMessage: 'No existe el usuario ingresado',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  381: {
    type: 'parametro_invalido',
    code: 'INV0136',
    merchantMessage: 'El parámetro source ingresado es incorrecto',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  382: {
    type: 'parametro_invalido',
    code: 'INV0137',
    merchantMessage: 'El card no ha sido encontrado',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  383: {
    type: 'parametro_invalido',
    code: 'INV0138',
    merchantMessage: 'El token es inválido o expiró',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  385: {
    type: 'parametro_invalido',
    code: 'INV0139',
    merchantMessage: 'La creación de token está desactivada de servidor a servidor',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  386: {
    type: 'parametro_invalido',
    code: 'INV0140',
    merchantMessage: 'El monto solicitado de devolución es inválido',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  387: {
    type: 'parametro_invalido',
    code: 'INV0141',
    merchantMessage: 'El usuario está en la lista negra',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  388: {
    type: 'parametro_invalido',
    code: 'INV0142',
    merchantMessage: 'La tarjeta está en la lista negra',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  389: {
    type: 'parametro_invalido',
    code: 'INV0143',
    merchantMessage: 'La tarjeta es internacional y el comercio no acepta compras internacionales',
    userMessage: 'Lo sentimos, ocurrió un problema en tu compra. Contáctanos para ayudarte.',
  },
  400: {
    type: 'invalid_request_error',
    merchantMessage: 'Estás enviando una petición con un cuerpo vacío.',
  },
  401: {
    type: 'invalid_request_error',
    merchantMessage:
      'Petición inválida, verifica que el Content-Type sea application/json (formato JSON) y el cuerpo un JSON válido.',
  },
  402: {
    type: 'resource_error',
    merchantMessage:
      'La URL de la petición no ha sido reconocida (%s: %s). Por favor, revisa la documentación en https://culqi.com/api para apuntar a la dirección correcta del recurso.',
  },
  403: {
    type: 'authentication_error',
    merchantMessage:
      'Código de comercio proporcionado inválido: %s. Revisa tus llaves correctas en tu panel Culqi (http://culqi.com).',
  },
  404: {
    type: 'authentication_error',
    merchantMessage:
      'Api Key (Llave API) proporcionada inválida: %s. Revisa tus llaves correctas en tu panel Culqi (http://culqi.com).',
  },
  405: {
    type: 'authentication_error',
    merchantMessage:
      'Tu comercio actualmente no puede realizar cargos reales en producción. Si eres el dueño del sitio, por favor activa tu comercio en https://integ-panel.culqi.com para poder quitar esta limitación.',
  },
  406: {
    type: 'authentication_error',
    merchantMessage:
      "Olvidaste indicar tu Código de Comercio en la autenticación, colocaste el header 'Authorization' pero sin el formato correcto con Bearer. Usando la autenticación Bearer, tu header de 'Authorization' sería algo así: 'Authorization: Bearer TU_CODIGO_COMERCIO'. Revisa la referencia del API para mayor detalle sobre la autenticación en https://culqi.com/api.",
  },
  407: {
    type: 'authentication_error',
    merchantMessage:
      "Olvidaste indicar tu API Key en la autenticación, colocaste el header 'Authorization' pero sin el formato correcto con Bearer. Usando la autenticación Bearer, tu header de 'Authorization' sería algo así: 'Authorization: Bearer TU_API_KEY'. Revisa la referencia del API para mayor detalle sobre la autenticación en https://culqi.com/api.",
  },
  408: {
    type: 'authentication_error',
    merchantMessage:
      'Tu código de comercio ha sido desactivado. Contáctate con Culqi para conocer el motivo',
  },
  409: {
    type: 'authentication_error',
    merchantMessage: 'El API Key ingresado se encuentra inactivo: %s',
  },
  410: {
    type: 'parameter_error',
    code: 'invalid_number',
    merchantMessage:
      'El número de la tarjeta no es un número de tarjeta de crédito o débito válido. Tiene que ser numérico de 13 a 16 digitos.',
    userMessage: 'El número de tarjeta de crédito o débito brindado no es válido.',
    param: 'card_number',
  },
  411: {
    type: 'card_error',
    merchantMessage:
      'No tienes activada la siguiente marca de tarjetas: %s. Contáctate con culqi.com/soporte  para solicitar la activación.',
    userMessage:
      'Lo sentimos, por el momento no aceptamos pagos con esta marca de tarjeta. Intente nuevamente con otra tarjeta.',
  },
  412: {
    type: 'card_error',
    merchantMessage:
      'Hemos detectado que estas intentando tokenizar una tarjeta real usando tu código de comercio en integración. Para encontrar tu llave de producción ingresa al Panel Culqi de Producción y obténla en la seccion Desarrollo -> API Keys. Realiza nuevamente tu petición con estas llaves. ',
    userMessage:
      ' ¡Uh! Tu tarjeta fue rechazada. Estás utilizando una tarjeta real para procesar pagos de prueba. ',
  },
  413: {
    type: 'card_error',
    merchantMessage:
      "Hemos detectado que estas queriendo tokenizar una tarjeta de prueba usando tu Código de Comercio en Producción. Para encontrar tu llave de integración ingresa al 'Panel Culqi' de Integración y obténla en la sección 'Desarrollo ->API Keys'. Eealiza nuevamente tu petición con estas llaves. ",
    userMessage:
      '¡Uh! Tu tarjeta fue rechazada. Estás utilizando una tarjeta de prueba para procesar pagos reales.',
  },
  414: {
    type: 'parameter_error',
    code: 'invalid_expiration_month',
    merchantMessage:
      'El mes de expiración de la tarjeta es inválido. Tiene que ser numérico del 1 al 12. ',
    userMessage:
      'El mes de expiración de tu tarjeta  es inválido. Verifica la información ingresada.',
    param: 'expiration_month',
  },
  415: {
    type: 'parameter_error',
    code: 'invalid_expiration_year',
    merchantMessage:
      'El año de expiración de la tarjeta es inválido. Tiene que ser numérico de 4 dígitos y no menor al año actual. ',
    userMessage:
      'El año de expiración de tu tarjeta es inválido. Verifica la información ingresada.',
    param: 'expiration_year',
  },
  416: {
    type: 'parameter_error',
    code: 'invalid_cvv',
    merchantMessage: 'El CVV es invalido. Tiene que ser numérico de 3 a 4 digitos. ',
    userMessage:
      'El código de seguridad (CVV) de tu tarjeta es inválido. Verifica la información ingresada.',
    param: 'cvv',
  },
  417: {
    type: 'parameter_error',
    code: 'invalid_email',
    merchantMessage: 'El email es invalido. Tiene que tener un formato válido y debe existir.',
    userMessage: 'El email es inválido. Verifica la información ingresada.',
    param: 'email',
  },
  418: {
    type: 'card_error',
    merchantMessage:
      'El comercio no acepta pagos con Tarjeta Internacional, por favor intenta con otra tarjeta.',
    userMessage:
      'Tu tarjeta no puede ser procesada por ser internacional,  Contáctanos para permitir tu compra.',
  },
  419: {
    type: 'parameter_error',
    merchantMessage:
      'Operación rechazada debido a que el usuario ha realizado operaciones fraudulentas previamente.',
    userMessage:
      'Lo sentimos, su usuario no puede realizar compras por el momento. Contáctanos para indicarte el motivo de rechazo.',
  },
  420: {
    type: 'card_error',
    merchantMessage:
      'Operación rechazada debido a que se han realizado operaciones fraudulentas con la misma tarjeta previamente.',
    userMessage:
      '       Lo sentimos, su tarjeta no puede realizar la transacción. Contáctanos para más detalle.',
  },
  421: {
    type: 'parameter_error',
    merchantMessage:
      'Operación rechazada debido a que tiene una alta probabilidad de ser fraudulenta.',
    userMessage: '  Lo sentimos, su operación fue rechazada. Contáctanos para más detalle.',
  },
  422: {
    type: 'card_error',
    merchantMessage:
      'No se encuentra el BIN de la tarjeta, para mayor información contactarse con Culqi a culqi.com/soporte .',
    userMessage:
      'Lo sentimos, tu tarjeta no puede ser procesada debido a que no es posible reconocerla como tarjeta de débito o crédito. Contáctanos para ayudarte.',
  },
  423: {
    type: 'parameter_error',
    merchantMessage: "No existe el token que intentas consultar: '%s'.",
  },
  424: {
    type: 'parameter_error',
    merchantMessage: "No existe el siguiente token: '%s' en el entorno de producción.",
  },
  425: {
    type: 'parameter_error',
    merchantMessage: "No existe el siguiente token: '%s' en el entorno de integración.",
  },
  426: {
    type: 'parameter_error',
    merchantMessage:
      "Colocaste un valor inválido para el filtro 'limit', debe ser entero y positivo.",
    param: 'limit',
  },
  427: {
    type: 'parameter_error',
    merchantMessage: "Colocaste un valor inválido para el filtro 'limit', debe ser menor de 100.",
    param: 'limit',
  },
  428: {
    type: 'parameter_error',
    merchantMessage: "El filtro 'before' tiene un token inexistente en la petición. ",
    param: 'before',
  },
  429: {
    type: 'parameter_error',
    merchantMessage: "El filtro 'after' tiene un token inexistente en la petición. ",
    param: 'after',
  },
  430: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'creation_date_from' tiene una fecha inválida, tiene que ser formato UNIX o no tiene que estar vacío.",
    param: 'date_from',
  },
  431: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'creation_date_to' tiene una fecha inválida, tiene que ser formato UNIX o no tiene que estar vacío.",
    param: 'date_to',
  },
  432: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'card_brand' tiene un valor inválido o está vacio. Estos son los únicos valores permitidos: 'visa', 'mastercard', 'american_express' o 'diners_club'.",
    param: 'card brand',
  },
  433: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'card_type'  tiene un valor inválido o está vacio. Estos son los únicos valores permitidos: 'credito', 'debito' o 'prepagada'.",
    param: 'card type',
  },
  434: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'device_type' tiene un valor inválido o está vacío. Estos son los únicos valores permitidos: 'desktop', 'mobile' o 'tablet'",
    param: 'device type',
  },
  435: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'iin' tiene un valor inválido o está vacío.  Un BIN es de 6 dígitos.",
    param: 'iin',
  },
  436: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'country_code' tiene un valor inválido o está vacío. Los valores pertimidos son los códigos ISO-3166-1 (Alfa 2) como : PE, US.",
    param: 'country code',
  },
  437: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'amount' es inválido o está vacío.  El valor debe ser entero y sin punto decimal. Ejemplo: 19900 (199.00 soles)",
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
    param: 'amount',
  },
  438: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'amount' es inválido. El valor debe ser mayor o igual que '100' (1 sol) .",
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
    param: 'amount',
  },
  439: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'amount' es inválido.  El valor de 'amount' debe ser de no más de 9999900 (S/ 99,999.00).",
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
    param: 'amount',
  },
  440: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'currency_code' es inválido o está vacío, el código de la moneda en tres letras (Formato ISO 4217). Culqi actualmente soporta las siguientes monedas: 'USD', PEN'.",
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
    param: 'currency_code',
  },
  441: {
    type: 'card_error',
    merchantMessage:
      'No tienes activada la siguiente moneda: %s. Contáctate con culqi.com/soporte  para solicitar la activación.',
    userMessage:
      '¡Lo sentimos! Tu operación no puede procesarse en la moneda elegida. Contáctanos para ayudarte',
    param: 'currency_code',
  },
  442: {
    type: 'parameter_error',
    merchantMessage: "El campo 'source_id' es inválido o está vacío.",
    param: 'source_id',
  },
  443: {
    type: 'parameter_error',
    merchantMessage: "El campo 'source_id' tiene un valor inexistente en Culqi.",
    param: 'source_id',
  },
  444: {
    type: 'parameter_error',
    merchantMessage:
      "No puedes utilizar un token que ya expiró: '%s'. Los tokens expiran tras 5 minutos de creados y sólo se pueden usar una vez.",
    userMessage:
      'La operación fue rechazada debido a que superó el tiempo de inactividad (más de 5 min).',
    param: 'source_id',
  },
  445: {
    type: 'parameter_error',
    merchantMessage:
      "No puedes utilizar un token más de una vez: '%s'. Además, recordar también que expiran tras 5 minutos  de ser creados.",
    userMessage:
      'La operación de compra ya fue efectuada, por favor verifica el estado actual de tu compra.',
    param: 'source_id',
  },
  446: {
    type: 'parameter_error',
    merchantMessage: "No existe el siguiente source_id: '%s' en el entorno de integración.",
    param: 'source_id',
  },
  447: {
    type: 'parameter_error',
    merchantMessage: "No existe el siguiente source_id: '%s' en el entorno de producción.",
    param: 'source_id',
  },
  448: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'capture' es inválido o está vacío. Los valores permitidos son de tipo booleano: 'true' o 'false'.",
    param: 'capture',
  },
  449: {
    type: 'parameter_error',
    merchantMessage: 'Enviaste más de 20 parámetros en el metadata. El limite es 20. ',
    param: 'metadata',
  },
  450: {
    type: 'parameter_error',
    merchantMessage:
      "En el parametro '%s' de la metadata, su valor excede el límite de caracteres . El limite de valor es de 200 caracteres.",
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
    param: 'metadata',
  },
  451: {
    type: 'parameter_error',
    merchantMessage:
      "El nombre del parametro '%s' de la metadata excede el límite de caracteres. El lÍmite para el nombre de parámetro es de 30 caracteres.",
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
    param: 'metadata',
  },
  452: {
    type: 'parameter_error',
    merchantMessage:
      "En el campo 'installments', el número de coutas que ingresaste no es soportado por la tarjeta. Revisar la referencia del API en https://www.culqi.com/api.",
    userMessage:
      'La operación fue rechazada debido a que no acepta la cantidad de coutas seleccionada. Por favor, vuelve a intentar la compra. ',
    param: 'installments',
  },
  453: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'installments' es inválido o está vacío, recordar que el valor debe ser entero positivo. Revisar la referencia del API en https://www.culqi.com/api.",
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
    param: 'installments',
  },
  454: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'first_name' es inválido o está vacío, debe ser de menos de 50 caracteres. Revisar la referencia del API en https://www.culqi.com/api.",
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
    param: 'first_name',
  },
  455: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'last_name' es inválido o está vacío, debe ser de menos de 50 caracteres. Revisar la referencia del API en https://www.culqi.com/api.",
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
    param: 'last_name',
  },
  456: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'email' es inválido o está vacío. El valor debe tener un formato válido de email y ser existente. Revisar la referencia del API en https://www.culqi.com/api.",
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
    param: 'email',
  },
  457: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'address' es inválido o está vacío. El valor debe ser de menos de 100 caracteres, y más de 5. Revisar la referencia del API en https://www.culqi.com/api.",
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
    param: 'address',
  },
  458: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'address_city' es inválido o está vacío. El valor debe ser de menos de 30 caracteres, y más de 2. Revisar la referencia del API en https://www.culqi.com/api.",
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
    param: 'address_city',
  },
  459: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'phone_number' es inválido o está vacío. El valor debe ser de menos de 15 caracteres, y más de 5. Revisar la referencia del API en https://www.culqi.com/api.",
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
    param: 'phone_number',
  },
  460: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'country_code' es inválido o está vacío. El valor debe tener el formato ISO-3166-1 (Alfa 2) de 2 caracteres. Revisar la referencia del API en https://www.culqi.com/api.",
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
    param: 'country_code',
  },
  461: {
    type: 'card_error',
    merchantMessage:
      'Operación rechazada debido a que tiene una alta probabilidad de ser fraudulenta.',
    userMessage: '  Lo sentimos, su operación fue rechazada. Contáctanos para más detalle.',
  },
  462: {
    type: 'parameter_error',
    merchantMessage: "No existe el cargo que intentas consultar: '%s' o tiene un formato inválido.",
  },
  463: {
    type: 'parameter_error',
    merchantMessage: "No existe el siguiente cargo: '%s' en el entorno de integración.",
  },
  464: {
    type: 'parameter_error',
    merchantMessage: "No existe el siguiente cargo: '%s' en el entorno de producción.",
  },
  465: {
    type: 'parameter_error',
    merchantMessage: "No existe el cargo que intentas capturar: '%s' o tiene un formato inválido.",
  },
  466: {
    type: 'parameter_error',
    merchantMessage: "No existe el siguiente cargo: '%s' en el entorno de integración.",
  },
  467: {
    type: 'parameter_error',
    merchantMessage: "No existe el siguiente cargo: '%s' en el entorno de producción.",
  },
  468: {
    type: 'parameter_error',
    code: 'CAP002',
    merchantMessage: "El cargo '%s' ya ha sido capturado.",
  },
  469: { type: 'parameter_error', merchantMessage: "El cargo '%s' ya ha sido devuelto." },
  470: {
    type: 'parameter_error',
    merchantMessage: "El cargo '%s' ha sido rechazado, no puede ser devuelto.",
  },
  471: {
    type: 'parameter_error',
    merchantMessage:
      "No existe el cargo que intentas actualizar: '%s' o tiene un formato inválido.",
  },
  472: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'amount' tiene un valor inválido o está vacio. Los valores válidos no deben contener comas decimales, es decir, si queremos filtrar por la cantidad de 25 soles el 'amount' sería 2500.",
    param: 'amount',
  },
  473: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'amount' tiene un valor inválido. El valor de 'amount' debe ser mayor o igual que 100 (1 sol, 1 dólar).",
    param: 'amount',
  },
  474: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'amount' tiene un valor inválido. El valor de 'amount' debe ser de no más de S/ 99,999.00',",
    param: 'amount',
  },
  475: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'installments' tiene un valor inválido o está vacio. El valor de las coutas son enteros positivos. Consultar el detalle de la referencia de API en https://culqi.com/api",
    param: 'installments',
  },
  476: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'currency_code' tiene un valor inválido o está vacío, se debe colocar código de la moneda en tres letras (Formato ISO 4217). Culqi actualmente soporta las siguientes monedas: USD, PEN.",
    param: 'currency_code',
  },
  477: {
    type: 'parameter_error',
    merchantMessage:
      "'Invalid type, code, decline_code: us. Culqi currently supports these type, code, decline_code: card_error, etc",
    param: 'currency_code',
  },
  478: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'fraud_score' tiene un valor inválido o está vacío. El valor debe ser entero mayor a 0 y menor o igual que 100.'.",
    param: 'score',
  },
  479: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'first_name' tiene un valor inválido o está vacío. El valor de 'first_name' no puede contener números y debe ser de menos de 50 caracteres.",
    param: 'first_name',
  },
  480: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'last_name' tiene un valor inválido o está vacío. El valor de 'last_name' no puede contener números y debe ser de menos de 50 caracteres.",
    param: 'last_name',
  },
  481: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'email' tiene un valor inválido o está vacío. El valor debe tener un formato válido de email y ser existente.",
    param: 'email',
  },
  482: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'address' tiene un valor inválido o está vacío. El valor debe tener menos de 100 caracteres, y mas de 5.",
    param: 'address',
  },
  483: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'address_city' tiene un valor inválido o está vacío. El valor debe tener menos de 30 caracteres, y mas de 2. Ejem: 'lima'.",
    param: 'address_city',
  },
  484: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'phone_number' tiene un valor inválido o está vacío. El valor debe tener menos de 15 caracteres, y mas de 5. ",
    param: 'phone_number',
  },
  485: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'country_code' tiene un valor inválido o está vacío.  Los valores pertimidos son los códigos ISO-3166-1 (Alfa 2) como : PE, US.",
    param: 'country_code',
  },
  486: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'dispute' tiene un valor inválido o está vacío. El valor debe ser un booleano (true o false).",
    param: 'dispute',
  },
  487: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'captured' tiene un valor inválido o está vacío. El valor debe ser un booleano (true o false).",
    param: 'captured',
  },
  488: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'duplicated' tiene un valor inválido o está vacío. El valor debe ser un booleano (true o false).",
    param: 'duplicated',
  },
  489: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'paid' tiene un valor inválido o está vacío. El valor debe ser un booleano (true o false).",
    param: 'paid',
  },
  490: {
    type: 'parameter_error',
    merchantMessage: "El filtro 'customer_id' tiene un valor inválido o está vacío.",
    param: 'customer_id',
  },
  491: {
    type: 'parameter_error',
    merchantMessage: 'El customer_id solicitado no existe.',
    param: 'customer_id',
  },
  492: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'reference' tiene un valor inválido o está vacío. El valor debe tener un rango de 5 a 15 caracteres",
    param: 'reference',
  },
  493: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'creation_date_from' tiene un valor inválido o está vacío. El valor debe estar en formato formato UNIX Time Stamp. Ejem: 1486150271.",
    param: 'date_from',
  },
  494: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'creation_date_to' tiene un valor inválido o está vacío. El valor debe estar en formato formato UNIX Time Stamp. Ejem: 1486150271.",
    param: 'date_to',
  },
  495: {
    type: 'parameter_error',
    merchantMessage: "El filtro 'fee' tiene un valor inválido o está vacío.",
    param: 'fee',
  },
  496: {
    type: 'parameter_error',
    merchantMessage: "El filtro 'before' tiene un cargo inválido o está vacío.",
    param: 'before',
  },
  497: {
    type: 'parameter_error',
    merchantMessage: "El filtro 'after' tiene un cargo inválido o está vacío.",
    param: 'after',
  },
  498: {
    type: 'parameter_error',
    merchantMessage:
      "Colocaste un valor inválido para el filtro 'limit', debe ser entero y positivo.",
    param: 'limit',
  },
  499: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'card_brand' tiene un valor inválido o está vacio. Estos son los únicos valores permitidos: 'visa', 'mastercard', 'american_express' o 'diners_club'.",
    param: 'card brand',
  },
  500: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'card_type'  tiene un valor inválido o está vacio. Estos son los únicos valores permitidos: 'credito', 'debito' o prepagada.",
    param: 'card type',
  },
  501: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'device_type' tiene un valor inválido o está vacío. Estos son los únicos valores permitidos: 'desktop', 'mobile', 'tablet' o es vacío.",
    param: 'device type',
  },
  502: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'iin' tiene un valor inválido o está vacío.  Un BIN es de 6 dígitos.",
    param: 'iin',
  },
  503: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'country_code' tiene un valor inválido o está vacío. Los valores pertimidos son los códigos ISO-3166-1 (Alfa 2) como : PE, US.",
    param: 'country code',
  },
  504: {
    type: 'parameter_error',
    merchantMessage: "No existe la transferencia que intentas consultar: '%s'.",
  },
  505: {
    type: 'parameter_error',
    merchantMessage: "No existe la siguiente transferencia: '%s' en el entorno de producción.",
  },
  506: {
    type: 'parameter_error',
    merchantMessage: "No existe la siguiente transferencia: '%s' en el entorno de integración.",
  },
  507: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'currency_code' tiene un valor inválido o está vacío, se debe colocar código de la moneda en tres letras (Formato ISO 4217). Culqi actualmente soporta las siguientes monedas: USD, PEN.",
    param: 'currency_code',
  },
  508: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'creation_date' tiene un valor inválido o está vacío. El valor debe estar en formato formato UNIX Time Stamp. Ejem: 1486150271.",
    param: 'creation_date',
  },
  509: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'creation_date_from' tiene un valor inválido o está vacío. El valor debe estar en formato formato UNIX Time Stamp. Ejem: 1486150271.",
    param: 'creation_date_from',
  },
  510: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'creation_date_to' tiene un valor inválido o está vacío. El valor debe estar en formato formato UNIX Time Stamp. Ejem: 1486150271.",
    param: 'creation_date_to',
  },
  511: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'available_date' tiene un valor inválido o está vacío. El valor debe estar en formato formato UNIX Time Stamp. Ejem: 1486150271.",
    param: 'available_date',
  },
  512: {
    type: 'parameter_error',
    merchantMessage:
      "El filta 'available_date_from' tiene un valor inválido o está vacío. El valor debe estar en formato formato UNIX Time Stamp. Ejem: 1486150271.",
    param: 'available_date_from',
  },
  513: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'available_date_to' tiene un valor inválido o está vacío. El valor debe estar en formato formato UNIX Time Stamp. Ejem: 1486150271.",
    param: 'available_date_to',
  },
  514: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'deposit_amount' tiene un valor inválido. El valor de 'deposit_amount' debe ser mayor o igual que 100 (1 sol, 1 dólar).",
    param: 'deposit_amount',
  },
  515: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'min_transfer_amount' tiene un valor inválido. El valor de 'depositminamount' debe ser mayor o igual que 100 (1 sol, 1 dólar).",
    param: 'min_transfer_amount',
  },
  516: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'max_transfer_amount' tiene un valor inválido. El valor de 'max_transfer_amount' debe ser mayor o igual que 100 (1 sol, 1 dólar).",
    param: 'max_transfer_amount',
  },
  517: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'state' tiene un valor inválido o está vacio. Estos son los únicos valores permitidos: 'pagado' o 'pendiente'.",
    param: 'state',
  },
  518: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'type' tiene un valor inválido o está vacío. Estos son los únicos valores permitidos:  'ventanilla', 'interbancario', o 'entre_cuentas'.",
    param: 'type',
  },
  519: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'first_name' es inválido o está vacío, debe ser de menos de 50 caracteres. Revisar la referencia del API en https://www.culqi.com/api.",
    param: 'first_name',
  },
  520: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'last_name' es inválido o está vacío, debe ser de menos de 50 caracteres. Revisar la referencia del API en https://www.culqi.com/api.",
    param: 'last_name',
  },
  521: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'email' es inválido o está vacío. El valor debe tener un formato válido de email y ser existente. Revisar la referencia del API en https://www.culqi.com/api.",
    param: 'email',
  },
  522: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'address' es inválido o está vacío. El valor debe ser de menos de 100 caracteres, y más de 5. Revisar la referencia del API en https://www.culqi.com/api.",
    param: 'address',
  },
  523: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'address_city' es inválido o está vacío. El valor debe ser de menos de 30 caracteres, y más de 2. Revisar la referencia del API en https://www.culqi.com/api.",
    param: 'address_city',
  },
  524: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'phone_number' es inválido o está vacío. El valor debe ser de menos de 15 caracteres, y más de 5. Revisar la referencia del API en https://www.culqi.com/api.",
    param: 'phone_number',
  },
  525: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'country_code' es inválido o está vacío. El valor debe tener el formato ISO-3166-1 (Alfa 2) de 2 caracteres. Revisar la referencia del API en https://www.culqi.com/api.",
    param: 'country_code',
  },
  526: {
    type: 'parameter_error',
    merchantMessage: 'Un cliente esta registrado actualmente con este email.',
    param: 'email',
  },
  527: {
    type: 'parameter_error',
    merchantMessage:
      "No existe el cliente que intentas consultar: '%s' o tiene un formato inválido.",
  },
  528: {
    type: 'parameter_error',
    merchantMessage: "No existe el siguiente cliente: '%s' en el entorno de integración.",
  },
  529: {
    type: 'parameter_error',
    merchantMessage: "No existe el siguiente cliente: '%s' en el entorno de producción.",
  },
  530: {
    type: 'parameter_error',
    merchantMessage:
      "No existe el cliente que intentas eliminar: '%s' o tiene un formato inválido.",
  },
  531: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'first_name' es inválido o está vacío, debe ser de menos de 50 caracteres. Revisar la referencia del API en https://www.culqi.com/api.",
    param: 'first_name',
  },
  532: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'last_name' es inválido o está vacío, debe ser de menos de 50 caracteres. Revisar la referencia del API en https://www.culqi.com/api.",
    param: 'last_name',
  },
  533: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'address' es inválido o está vacío. El valor debe ser de menos de 100 caracteres, y más de 5. Revisar la referencia del API en https://www.culqi.com/api.",
    param: 'address',
  },
  534: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'address_city' es inválido o está vacío. El valor debe ser de menos de 30 caracteres, y más de 2. Revisar la referencia del API en https://www.culqi.com/api.",
    param: 'address_city',
  },
  535: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'phone_number' es inválido o está vacío. El valor debe ser de menos de 15 caracteres, y más de 5. Revisar la referencia del API en https://www.culqi.com/api.",
    param: 'phone_number',
  },
  536: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'country_code' es inválido o está vacío. El valor debe tener el formato ISO-3166-1 (Alfa 2) de 2 caracteres. Revisar la referencia del API en https://www.culqi.com/api.",
    param: 'country_code',
  },
  537: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'first_name' tiene un valor inválido o está vacío. El valor de 'first_name' no puede contener números y debe ser de menos de 50 caracteres.",
    param: 'first_name',
  },
  538: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'last_name' tiene un valor inválido o está vacío. El valor de 'last_name' no puede contener números y debe ser de menos de 50 caracteres.",
    param: 'last_name',
  },
  539: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'email' tiene un valor inválido o está vacío. El valor debe tener un formato válido de email y ser existente.",
    param: 'email',
  },
  540: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'address' tiene un valor inválido o está vacío. El valor debe tener menos de 100 caracteres, y mas de 5.",
    param: 'address',
  },
  541: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'address_city' tiene un valor inválido o está vacío. El valor debe tener menos de 30 caracteres, y mas de 2. Ejem: 'lima'.",
    param: 'address_city',
  },
  542: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'phone_number' tiene un valor inválido o está vacío. El valor debe tener menos de 15 caracteres, y mas de 5. ",
    param: 'phone_number',
  },
  543: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'country_code' tiene un valor inválido o está vacío.  Los valores pertimidos son los códigos ISO-3166-1 (Alfa 2) como : PE, US.",
    param: 'country_code',
  },
  544: {
    type: 'parameter_error',
    merchantMessage: "El campo 'token_id' es inválido o está vacío.",
    param: 'token_id',
  },
  545: {
    type: 'parameter_error',
    merchantMessage: "No existe el siguiente token_id: '%s'.",
    param: 'token_id',
  },
  546: {
    type: 'parameter_error',
    merchantMessage:
      "No puedes utilizar un token que ya expiró: %s.' Los tokens expiran tras 5 minutos de creados y solo se pueden usar una vez.",
    param: 'token_id',
  },
  547: {
    type: 'parameter_error',
    merchantMessage:
      "No puedes utilizar un token más de una vez: %s.' Además, recodar también que expiran tras 5 minutos de ser creados.",
    param: 'token_id',
  },
  548: {
    type: 'parameter_error',
    merchantMessage: "No existe el siguiente token_id: '%s' en el entorno de integración.",
    param: 'token_id',
  },
  549: {
    type: 'parameter_error',
    merchantMessage: "No existe el siguiente token_id: '%s' en el entorno de producción.",
    param: 'token_id',
  },
  550: {
    type: 'parameter_error',
    merchantMessage: "El campo 'customer_id' es inválido o está vacío.",
    param: 'customer_id',
  },
  551: {
    type: 'parameter_error',
    merchantMessage: "El campo 'customer_id' tiene un valor inexistente en Culqi.",
    param: 'customer_id',
  },
  552: {
    type: 'parameter_error',
    merchantMessage: "No existe el siguiente customer_id: '%s' en el entorno de integración.",
    param: 'customer_id',
  },
  553: {
    type: 'parameter_error',
    merchantMessage: "No existe el siguiente customer_id: '%s' en el entorno de producción.",
    param: 'customer_id',
  },
  554: {
    type: 'parameter_error',
    merchantMessage:
      "No existe la tarjeta que intentas consultar: '%s' o tiene un formato inválido.",
  },
  555: {
    type: 'parameter_error',
    merchantMessage: "No existe la siguiente tarjeta: '%s' en el entorno de integración.",
  },
  556: {
    type: 'parameter_error',
    merchantMessage: "No existe la siguiente tarjeta: '%s' en el entorno de producción",
  },
  557: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'creation_date' tiene un valor inválido o está vacío. El valor debe estar en formato formato UNIX Time Stamp. Ejem: 1486150271.",
    param: 'creation_date',
  },
  558: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'creation_date_from' tiene un valor inválido o está vacío. El valor debe estar en formato formato UNIX Time Stamp. Ejem: 1486150271.",
    param: 'creation_date_from',
  },
  559: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'creation_date_to' tiene un valor inválido o está vacío. El valor debe estar en formato formato UNIX Time Stamp. Ejem: 1486150271.",
    param: 'creation_date_to',
  },
  560: {
    type: 'parameter_error',
    merchantMessage:
      "Colocaste un valor inválido para el filtro 'limit', debe ser entero y positivo.",
    param: 'limit',
  },
  561: {
    type: 'parameter_error',
    merchantMessage: "El filtro 'before' tiene una tarjeta inválida o está vacía.",
    param: 'before',
  },
  562: {
    type: 'parameter_error',
    merchantMessage: "El filtro 'after' tiene una tarjeta inválida o está vacía.",
    param: 'after',
  },
  563: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'creation_date_from' tiene un valor inválido o está vacío. El valor debe estar en formato formato UNIX Time Stamp. Ejem: 1486150271.",
    param: 'date_from',
  },
  564: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'creation_date_to' tiene un valor inválido o está vacío. El valor debe estar en formato formato UNIX Time Stamp. Ejem: 1486150271.",
    param: 'date_to',
  },
  565: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'card_brand' tiene un valor inválido o está vacio. Estos son los únicos valores permitidos: 'visa', 'mastercard', 'american_express' o 'diners_club'.",
    param: 'card brand',
  },
  566: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'card_type'  tiene un valor inválido o está vacio. Estos son los únicos valores permitidos: 'credito', 'debito' o prepagada.",
    param: 'card type',
  },
  567: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'device_type' tiene un valor inválido o está vacío. Estos son los únicos valores permitidos: 'desktop', 'mobile', 'tablet' o es vacío.",
    param: 'device type',
  },
  568: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'iin' tiene un valor inválido o está vacío.  Un BIN es de 6 dígitos.",
    param: 'iin',
  },
  569: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'country_code' tiene un valor inválido o está vacío. Los valores pertimidos son los códigos ISO-3166-1 (Alfa 2) como : PE, US.",
    param: 'country code',
  },
  570: { type: 'parameter_error', merchantMessage: "No existe la siguiente tarjeta: '%s'" },
  571: {
    type: 'parameter_error',
    merchantMessage:
      "No existe la tarjeta que intentas eliminar: '%s' o tiene un formato inválido.",
  },
  572: {
    type: 'parameter_error',
    merchantMessage: "No existe la siguiente tarjeta: '%s' en el entorno de integración.",
  },
  573: {
    type: 'parameter_error',
    merchantMessage: "No existe la siguiente tarjeta: '%s' en el entorno de producción.",
  },
  574: {
    type: 'parameter_error',
    merchantMessage:
      "No existe la tarjeta que intentas eliminar: '%s' o tiene un formato inválido.",
  },
  575: {
    type: 'parameter_error',
    merchantMessage: "No existe el cargo que intentas devolver: '%s' o tiene un formato inválido.",
    param: 'charge_id',
  },
  576: {
    type: 'parameter_error',
    merchantMessage: "El cargo '%s' ya ha sido devuelto totalmente.",
    param: 'charge_id',
  },
  577: {
    type: 'parameter_error',
    merchantMessage: "No existe el siguiente cargo: '%s' en el entorno de integración.",
    param: 'charge_id',
  },
  578: {
    type: 'parameter_error',
    merchantMessage: "No existe el siguiente cargo: '%s' en el entorno de producción.",
    param: 'charge_id',
  },
  579: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'amount' es inválido o está vacío.  El valor debe ser entero y sin punto decimal. Ejemplo: 19900 (199.00 soles)",
    param: 'amount',
  },
  580: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'amount' es inválido. No puedes devolver una cantidad mayor al monto restante.",
    param: 'amount',
  },
  581: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'amount' es inválido. Lo sentimos, no puedes devolver ese monto ya que está por debajo del permitido. (%s soles)",
    param: 'amount',
  },
  582: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'reason' es inválido o está vacío. Estos son los valores únicos permitidos: 'duplicado','fraudulento' o 'solicitud_comprador'",
    param: 'reason',
  },
  583: {
    type: 'parameter_error',
    merchantMessage:
      "No existe la devolución que intentas consultar: '%s' o tiene un formato inválido.",
  },
  584: {
    type: 'parameter_error',
    merchantMessage: "No existe la siguiente devolución: '%s' en el entorno de integración.",
  },
  585: {
    type: 'parameter_error',
    merchantMessage: "No existe la siguiente devolución: '%s' en el entorno de producción.",
  },
  586: {
    type: 'parameter_error',
    merchantMessage:
      "Colocaste un valor inválido para el filtro 'limit', debe ser entero y positivo.",
    param: 'limit',
  },
  587: {
    type: 'parameter_error',
    merchantMessage: "El filtro 'before' tiene una devolución inválida o está vacía.",
    param: 'before',
  },
  588: {
    type: 'parameter_error',
    merchantMessage: "El filtro 'after' tiene una devolución inválida o está vacía.",
    param: 'after',
  },
  589: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'creation_date_from' tiene un valor inválido o está vacío. El valor debe estar en formato formato UNIX Time Stamp. Ejem: 1486150271.",
    param: 'date_from',
  },
  590: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'creation_date_to' tiene un valor inválido o está vacío. El valor debe estar en formato formato UNIX Time Stamp. Ejem: 1486150271.",
    param: 'date_to',
  },
  591: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'reason' tiene un valor inválido o está vacio. Estos son los únicos valores permitidos: 'duplicado', 'fraudulento' o 'solicitud_comprador'.",
    param: 'reason',
  },
  592: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'currency_code' es inválido o está vacío, el código de la moneda en tres letras (Formato ISO 4217). Culqi actualmente soporta las siguientes monedas: 'USD', PEN'.",
    param: 'currency_code',
  },
  593: {
    type: 'card_error',
    merchantMessage:
      'No tienes activada la siguiente moneda: USD. Contáctate con culqi.com/soporte  para solicitar la activación.',
    param: 'currency_code',
  },
  594: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'amount' es inválido o está vacío.  El valor debe ser entero y sin punto decimal. Ejemplo: 19900 (199.00 soles).",
    param: 'amount',
  },
  595: {
    type: 'parameter_error',
    merchantMessage: "El campo 'amount' es inválido.  El valor debe ser mayor o igual que '300'. ",
    param: 'amount',
  },
  596: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'amount' es inválido.  El valor de 'amount' debe ser de no más de 9999900 (S/ 99,999.00).",
    param: 'amount',
  },
  597: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'interval' es inválido o está vacío. Estos son los valores únicos permitidos: 'dias','semanas', 'meses' o 'años'. Revisar la referencia del API en https://www.culqi.com/api.",
    param: 'reason',
  },
  598: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'limit' es inválido o está vacío, solo permite valores en entero positivo mínimo 2 y máximo 99. Revisa la referencia del API en https://www.culqi.com/api.",
    param: 'limit',
  },
  599: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'name' es inválido o está vacío, el nombre del plan debe ser de menos de 50 caracteres. Revisar la referencia del API en https://www.culqi.com/api.",
    param: 'name',
  },
  600: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'trial_days' es inválido o está vacío, los días de prueba solo pueden ir en el valor un entero positivo mínimo 1 y máximo 99. Revisa la referencia del API en https://www.culqi.com/api.",
    param: 'trial_days',
  },
  601: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'trial_days' es inválido o está vacío.  Revisar la referencia del API en https://www.culqi.com/api.",
  },
  602: {
    type: 'parameter_error',
    merchantMessage: "No existe el plan que intentas consultar: '%s' o tiene un formato inválido.",
  },
  603: {
    type: 'parameter_error',
    merchantMessage: "No existe el siguiente plan: '%s' en el entorno de integración.",
  },
  604: {
    type: 'parameter_error',
    merchantMessage: "No existe el plan que intentas actualizar: '%s' o tiene un formato inválido.",
  },
  605: {
    type: 'parameter_error',
    merchantMessage: "No existe el plan que intentas eliminar: '%s' o tiene un formato inválido.",
  },
  606: {
    type: 'parameter_error',
    merchantMessage: "No existe el siguiente plan: '%s' en el entorno de producción.",
  },
  607: {
    type: 'parameter_error',
    merchantMessage: "No existe el plan que intentas eliminar: '%s' o tiene un formato inválido.",
  },
  608: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'amount' tiene un valor inválido o está vacio. Los valores válidos no deben contener comas decimales, es decir, si queremos filtrar por la cantidad de 25 soles el 'amount' sería 2500.",
    param: 'amount',
  },
  609: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'amount' tiene un valor inválido. El valor de 'amount' debe ser mayor o igual que 100 (1 sol, 1 dólar).",
    param: 'amount',
  },
  610: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'amount' tiene un valor inválido. El valor de 'amount' debe ser de no más de S/ 99,999.00',",
    param: 'amount',
  },
  611: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'creation_date_from' tiene un valor inválido o está vacío. El valor debe estar en formato formato UNIX Time Stamp. Ejem: 1486150271.",
    param: 'date_from',
  },
  612: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'creation_date_to' tiene un valor inválido o está vacío. El valor debe estar en formato formato UNIX Time Stamp. Ejem: 1486150271.",
    param: 'date_to',
  },
  613: {
    type: 'parameter_error',
    merchantMessage: "El filtro 'before' tiene un plan inválido o está vacío.",
    param: 'before',
  },
  614: {
    type: 'parameter_error',
    merchantMessage: "El filtro 'after' tiene un plan inválido o está vacío.",
    param: 'after',
  },
  615: {
    type: 'parameter_error',
    merchantMessage:
      "Colocaste un valor inválido para el filtro 'limit', debe ser entero y positivo.",
    param: 'limit',
  },
  616: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'interval' tiene un valor inválido o está vacio. Estos son los únicos valores permitidos: 'days', 'weeks', 'months' o 'years'.",
    param: 'card type',
  },
  617: {
    type: 'parameter_error',
    merchantMessage: "El campo 'card_id' es inválido o está vacío.",
    param: 'card_id',
  },
  618: {
    type: 'parameter_error',
    merchantMessage: "No existe el siguiente card_id: '%s'.",
    param: 'card_id',
  },
  619: {
    type: 'parameter_error',
    merchantMessage: "No existe el siguiente card_id: '%s' en el entorno de integración.",
    param: 'card_id',
  },
  620: {
    type: 'parameter_error',
    merchantMessage: "No existe el siguiente card_id: '%s' en el entorno de producción.",
    param: 'card_id',
  },
  621: {
    type: 'parameter_error',
    merchantMessage: "El campo 'plan_id' es inválido o está vacío.",
    param: 'plan_id',
  },
  622: {
    type: 'parameter_error',
    merchantMessage: "No existe el siguiente plan_id: '%s'.",
    param: 'plan_id',
  },
  623: {
    type: 'parameter_error',
    merchantMessage: "No existe el siguiente plan_id: '%s' en el entorno de integración.",
    param: 'plan_id',
  },
  624: {
    type: 'parameter_error',
    merchantMessage: "No existe el siguiente plan_id: '%s' en el entorno de producción.",
    param: 'plan_id',
  },
  625: {
    type: 'parameter_error',
    merchantMessage: "El parámetro 'subscription_id' es inválido o está vacío.",
    param: 'subscription_id',
  },
  626: { type: 'parameter_error', merchantMessage: "No existe la suscripción consultada: '%s'." },
  627: {
    type: 'parameter_error',
    merchantMessage: "No existe la siguiente suscripción: '%s' en el entorno de integración.",
  },
  628: {
    type: 'parameter_error',
    merchantMessage: "No existe la siguiente suscripción: '%s' en el entorno de producción.",
  },
  629: {
    type: 'parameter_error',
    merchantMessage:
      "No existe la suscripción que intentas actualizar: '%s' o tiene un formato inválido.",
  },
  630: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'card_Id' es inválido o está vacío. Revisar la referencia del API en https://www.culqi.com/api.",
    param: 'card_id',
  },
  631: {
    type: 'parameter_error',
    merchantMessage: "No existe el siguiente card_id: '%s' en el entorno de integración.",
    param: 'card_id',
  },
  632: {
    type: 'parameter_error',
    merchantMessage: "No existe el siguiente card_id: '%s' en el entorno de producción.",
    param: 'card_id',
  },
  633: {
    type: 'parameter_error',
    merchantMessage:
      "No existe la suscripción que intentas cancelar: '%s' o tiene un formato inválido.",
  },
  634: {
    type: 'parameter_error',
    merchantMessage: "No existe la siguiente suscripción: '%s' en el entorno de integración.",
  },
  635: {
    type: 'parameter_error',
    merchantMessage: "No existe la siguiente suscripción: '%s' en el entorno de iproducción.",
  },
  636: {
    type: 'parameter_error',
    merchantMessage:
      "No existe la suscripción que intentas cancelar: '%s' o tiene un formato inválido.",
  },
  637: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'amount' tiene un valor inválido o está vacio. Los valores válidos no deben contener comas decimales, es decir, si queremos filtrar por la cantidad de 25 soles el 'amount' sería 2500.",
    param: 'amount',
  },
  638: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'amount' tiene un valor inválido. El valor de 'amount' debe ser mayor o igual que 100 (1 sol, 1 dólar).",
    param: 'amount',
  },
  639: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'amount' tiene un valor inválido. El valor de 'amount' debe ser de no más de S/ 99,999.00',",
    param: 'amount',
  },
  640: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'creation_date_from' tiene un valor inválido o está vacío. El valor debe estar en formato formato UNIX Time Stamp. Ejem: 1486150271.",
    param: 'date_from',
  },
  641: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'creation_date_to' tiene un valor inválido o está vacío. El valor debe estar en formato formato UNIX Time Stamp. Ejem: 1486150271.",
    param: 'date_to',
  },
  642: {
    type: 'parameter_error',
    merchantMessage: "El filtro 'before' tiene una suscripción inexistente en la petición. ",
    param: 'before',
  },
  643: {
    type: 'parameter_error',
    merchantMessage: "El filtro 'after' tiene una suscripción inexistente en la petición. ",
    param: 'after',
  },
  644: {
    type: 'parameter_error',
    merchantMessage:
      "Colocaste un valor inválido para el filtro 'limit', debe ser entero y positivo.",
    param: 'limit',
  },
  645: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'interval' es inválido o está vacío. Estos son los valores únicos permitidos: 'dias','semanas', 'meses' o 'años'. Revisar la referencia del API en https://www.culqi.com/api.",
    param: 'interval',
  },
  646: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'status' tiene un valor inválido o está vacio. Estos son los únicos valores permitidos: 'activa' o 'cancelada'. Revisar la referencia del API en https://www.culqi.com/api.",
    param: 'status',
  },
  647: {
    type: 'parameter_error',
    merchantMessage:
      "No existe el evento que intentas consultar: '%s' o tiene un formato inválido.",
  },
  648: {
    type: 'parameter_error',
    merchantMessage: "No existe el siguiente evento: '%s' en el entorno de integración.",
  },
  649: {
    type: 'parameter_error',
    merchantMessage: "No existe el siguiente evento: '%s' en el entorno de producción.",
  },
  650: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'creation_date_from' tiene un valor inválido o está vacío. El valor debe estar en formato formato UNIX Time Stamp. Ejem: 1486150271.",
    param: 'date_from',
  },
  651: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'creation_date_to' tiene un valor inválido o está vacío. El valor debe estar en formato formato UNIX Time Stamp. Ejem: 1486150271.",
    param: 'date_to',
  },
  652: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'type' tiene un valor inválido o está vacio. La lista de valores permitidos se encuentra en https://www.culqi.com/api .",
    param: 'type',
  },
  653: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'creation_date' tiene un valor inválido o está vacío. El valor debe estar en formato formato UNIX Time Stamp. Ejem: 1486150271.",
    param: 'date',
  },
  654: {
    type: 'parameter_error',
    merchantMessage: 'Enviaste el campo metadata con un formato incorrecto. ',
    param: 'metadata',
  },
  655: {
    type: 'card_error',
    code: 'card_declined',
    declineCode: 'expired_card',
    merchantMessage:
      'Tarjeta vencida. La tarjeta está vencida o la fecha de vencimiento ingresada es incorrecta.',
    userMessage:
      'Su tarjeta está vencida. Verifica la fecha de vencimiento de tu tarjeta e ingrésalos correctamente. Si es denegada nuevamente, contáctate con el banco emisor de tu tarjeta.',
  },
  656: {
    type: 'card_error',
    code: 'card_declined',
    declineCode: 'stolen_card',
    merchantMessage:
      'Tarjeta robada. La tarjeta fue bloqueada y reportada al banco emisor como una tarjeta robada.',
    userMessage:
      'La operación ha sido denegada por la entidad emisora de tu tarjeta. Contáctate con el banco para conocer el motivo de la denegación.',
  },
  657: {
    type: 'card_error',
    code: 'card_declined',
    declineCode: 'lost_card',
    merchantMessage:
      'Tarjeta perdida. La tarjeta fue bloqueada y reportada al banco emisor como una tarjeta perdida.',
    userMessage:
      'La operación ha sido denegada por la entidad emisora de tu tarjeta. Contáctate con el banco para conocer el motivo de la denegación.',
  },
  658: {
    type: 'card_error',
    code: 'card_declined',
    declineCode: 'insufficient_funds',
    merchantMessage:
      'Fondos insuficientes. La tarjeta no tiene fondos suficientes para realizar la compra.',
    userMessage:
      'Su tarjeta no tiene fondos suficientes. Para realizar la compra, verifica tus fondos disponibles con el banco emisor o inténta nuevamente con otra tarjeta.',
  },
  659: {
    type: 'card_error',
    code: 'card_declined',
    declineCode: 'contact_issuer',
    merchantMessage:
      'Contactar emisor. La operación fue denegada por el banco emisor de la tarjeta y el cliente necesita contactarse con la entidad para conocer el motivo.',
    userMessage:
      'La operación ha sido denegada por la entidad emisora de tu tarjeta. Contáctate con el banco para conocer el motivo de la denegación o intenta nuevamente con otra tarjeta.',
  },
  660: {
    type: 'card_error',
    code: 'card_declined',
    declineCode: 'invalid_cvv',
    merchantMessage:
      'CVV inválido. El código de seguridad (CVV2, CVC2, CID) de la tarjeta es inválido.',
    userMessage:
      'El código de seguridad (CVV) es inválido. Verifica tu tarjeta e ingresa los dígitos correctamente. Si es denegada nuevamente, contáctate con el banco emisor de tu tarjeta.',
  },
  661: {
    type: 'card_error',
    code: 'card_declined',
    declineCode: 'incorrect_cvv',
    merchantMessage:
      'CVV incorrecto. El código de seguridad (CVV2, CVC2, CID) de la tarjeta es incorrecto.',
    userMessage:
      'El código de seguridad (CVV) es incorrecto. Verifica tu tarjeta e ingresa los dígitos correctamente. Si es denegada nuevamente, contáctate con el banco emisor de tu tarjeta. ',
  },
  662: {
    type: 'card_error',
    code: 'card_declined',
    declineCode: 'too_many_attempts_cvv',
    merchantMessage:
      'Exceso CVV. El cliente ha intentado demasiadas veces ingresar el código de seguridad (CVV2, CVC2, CID) de la tarjeta.',
    userMessage:
      'La operación ha sido denegada por la entidad emisora de tu tarjeta. Contáctate con el banco para conocer el motivo de la denegación o intenta nuevamente con otra tarjeta.',
  },
  663: {
    type: 'card_error',
    code: 'card_declined',
    declineCode: 'issuer_not_available',
    merchantMessage:
      'Emisor no disponible. El banco que emitió la tarjeta no responde. El cliente debe realizar el pago nuevamente.',
    userMessage:
      'El banco emisor de tu tarjeta no responde y la operación no pudo ser completada. Vuelve a realizar el pago en unos minutos o intenta nuevamente con otra tarjeta.',
  },
  664: {
    type: 'card_error',
    code: 'card_declined',
    declineCode: 'issuer_decline_operation',
    merchantMessage:
      'Operación denegada. La operación fue denegada por el banco emisor de la tarjeta por una razón desconocida. ',
    userMessage:
      'La operación ha sido denegada por la entidad emisora de tu tarjeta. Contáctate con el banco para conocer el motivo de la denegación o intenta nuevamente con otra tarjeta.',
  },
  665: {
    type: 'card_error',
    code: 'card_declined',
    declineCode: 'invalid_card',
    merchantMessage:
      'Tarjeta inválida. La tarjeta utilizada tiene restricciones para este tipo de compras. El cliente necesita contactarse con el banco emisor para conocer el motivo de la denegación.',
    userMessage:
      'La operación ha sido denegada por la entidad emisora de tu tarjeta. Contáctate con el banco para conocer el motivo de la denegación o intenta nuevamente con otra tarjeta.',
  },
  666: {
    type: 'card_error',
    code: 'card_declined',
    declineCode: 'processing_error',
    merchantMessage:
      'Error de procesamiento. Ocurrió un error mientras procesabamos la compra. Contáctate con culqi.com/soporte  para que te demos una solución.',
    userMessage:
      'Ocurrió un error mientras procesabamos tu compra. Contáctate con nosotros para que te demos una solución.',
  },
  667: {
    type: 'card_error',
    code: 'card_declined',
    declineCode: 'fraudulent',
    merchantMessage:
      'Operación fraudulenta. El banco emisor de la tarjeta sospecha que se trata de una compra fraudulenta.',
    userMessage:
      'La operación ha sido denegada por la entidad emisora de tu tarjeta. Contáctate con el banco para conocer el motivo de la denegación o intenta nuevamente con otra tarjeta.',
  },
  668: {
    type: 'card_error',
    code: 'card_declined',
    declineCode: 'culqi_card',
    merchantMessage:
      'Tarjeta Culqi. Estás utilizando una tarjeta de pruebas de Culqi para realizar una compra real.',
    userMessage: 'Estás utilizando una tarjeta de pruebas de Culqi para realizar una compra real.',
  },
  669: {
    type: 'api_error',
    merchantMessage:
      'Ups! Algo salió mal en Culqi. Contáctate con culqi.com/soporte  para obtener mas información.',
    userMessage: 'Contactáte con soporte',
  },
  670: {
    type: 'authentication_error',
    merchantMessage:
      'Has utilizado la llave equivocada para realizar la operación. Dirígete al Panel de %s para usar las llaves correctas.',
  },
  671: {
    type: 'authentication_error',
    merchantMessage:
      'Tu código de comercio no está autorizado para realizar este tipo de peticiones. Contáctate con culqi.com/soporte  para obtener mas información.',
  },
  672: {
    type: 'parameter_error',
    merchantMessage:
      'El cargo no está disponible para la acción que deseas realizar, verifica el estado de tu cargo. Entérate como en https://www.culqi.com/api.',
  },
  673: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'description' no tiene un formato válido, debe tener entre 5 y 80 caracteres.",
    param: 'description',
  },
  674: {
    type: 'parameter_error',
    merchantMessage:
      'Se está procesando un cargo actualmente con la misma información, verifica que no estés enviando cargos duplicados.',
  },
  675: {
    type: 'parameter_error',
    merchantMessage:
      'No estás enviando el header de autorización, entérate como realizarlo en https://www.culqi.com/api.',
  },
  676: {
    type: 'parameter_error',
    merchantMessage:
      'Tu código de comercio no está habilitado para realizar operaciones en esta versión, actualiza tu código de comercio desde el panel.',
  },
  677: {
    type: 'authentication_error',
    merchantMessage:
      'Tu llave pública es inválida o está inactiva. Contáctate con culqi.com/soporte  para obtener más información',
  },
  678: {
    type: 'authentication_error',
    merchantMessage:
      'Tu llave privada es inválida o está inactiva. Contáctate con culqi.com/soporte  para obtener más información',
  },
  679: {
    type: 'parameter_error',
    merchantMessage:
      'No se encuentra el Bin de la tarjeta, contactarse con Culqi para mayor información a culqi.com/soporte .',
    userMessage: 'El número de tarjeta de crédito o débito brindado no es válido.',
    param: 'card_number',
  },
  680: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'interval_count' es inválido o está vacío, solo puede ser un entero positivo mínimo 1 y máximo 999. Revisa la referencia del API en https://www.culqi.com/api.",
    param: 'interval_count',
  },
  681: {
    type: 'authentication_error',
    merchantMessage:
      'Las peticiones al ambiente de producción deben ser realizadas con certificado SSL (https)',
  },
  682: { merchantMessage: 'Se eliminó %s con ID %s exitosamente.' },
  683: {
    type: 'parameter_error',
    merchantMessage:
      'Es necesario algun valor para actualizar el cliente. Revisar la referencia del API en https://www.culqi.com/api.',
  },
  684: {
    type: 'parameter_error',
    merchantMessage:
      'Es necesario algun valor para actualizar la tarjeta. Revisar la referencia del API en https://culqi.com/api.',
  },
  685: {
    type: 'parameter_error',
    merchantMessage:
      'El token ingresado ya esta afiliado a una tarjeta. Revisar la referencia del API en https://culqi.com/api.',
  },
  686: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'validate' es invalido o vacio. Los valores permitidos son 'true' o 'false'",
    param: 'validate',
  },
  687: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'fraud_validate' es invalido o vacio. Los valores permitidos son 'true' o 'false'",
    param: 'fraud_validate',
  },
  688: {
    type: 'parameter_error',
    merchantMessage: 'Operación rechazada debido a las reglas antifraude establecidas.',
    userMessage:
      'Lo sentimos, su usuario no puede realizar compras por el momento. Contáctanos para indicarte el motivo de rechazo.',
  },
  689: {
    type: 'parameter_error',
    merchantMessage: 'Transacción por arriba del monto de pedido máximo aceptado por el comercio.',
    userMessage: 'Transacción por arriba del monto de pedido máximo aceptado por el comercio.',
  },
  690: {
    type: 'parameter_error',
    merchantMessage: 'Trasacción por debajo del monto de pedido mínimo aceptado por el comercio.',
    userMessage: 'Trasacción por debajo del monto de pedido mínimo aceptado por el comercio.',
  },
  691: {
    type: 'parameter_error',
    merchantMessage: 'La tarjeta del cliente ha excedido el número máximo de compras al día.',
    userMessage: 'Esta tarjeta ha excedido el número máximo de compras.',
  },
  692: {
    type: 'parameter_error',
    merchantMessage: 'La tarjeta del cliente ha excedido el número máximo de compras en la semana.',
    userMessage: 'Esta tarjeta ha excedido el monto máximo de compras.',
  },
  693: {
    type: 'parameter_error',
    merchantMessage: 'La tarjeta del cliente ha excedido el número máximo de compras en el mes.',
    userMessage: 'Esta tarjeta ha excedido el monto máximo de compras.',
  },
  694: {
    type: 'parameter_error',
    merchantMessage: 'La tarjeta del cliente ha excedido el monto máximo de compra diaria.',
    userMessage: 'Este correo ha excedido el monto máximo de compras.',
  },
  695: {
    type: 'parameter_error',
    merchantMessage: 'La tarjeta del cliente ha excedido el monto máximo de compra semanal.',
    userMessage: 'Este correo ha excedido el monto máximo de compras.',
  },
  696: {
    type: 'parameter_error',
    merchantMessage: 'La tarjeta del cliente ha excedido el monto máximo de compra mensual.',
    userMessage: 'Este correo ha excedido el monto máximo de compras.',
  },
  697: {
    type: 'parameter_error',
    merchantMessage: 'El correo del cliente ha excedido el monto máximo de compra diaria.',
    userMessage: 'Esta tarjeta ha excedido el monto máximo de compras.',
  },
  698: {
    type: 'parameter_error',
    merchantMessage: 'El correo del cliente ha excedido el monto máximo de compra semanal.',
    userMessage: 'Esta tarjeta ha excedido el monto máximo de compras.',
  },
  699: {
    type: 'parameter_error',
    merchantMessage: 'El correo del cliente ha excedido el monto máximo de compra mensual.',
    userMessage: 'Esta tarjeta ha excedido el monto máximo de compras.',
  },
  700: {
    type: 'parameter_error',
    merchantMessage: 'El correo del cliente ha excedido el número máximo de compras al día',
    userMessage: 'Este correo ha excedido el número máximo de compras.',
  },
  701: {
    type: 'parameter_error',
    merchantMessage: 'El teléfono del cliente ha excedido el número máximo de compras al día',
    userMessage: 'Este teléfono ha excedido el número máximo de compras.',
  },
  702: {
    type: 'parameter_error',
    merchantMessage:
      'El device fingerprint del cliente ha excedido el número máximo de compras al día',
    userMessage: 'Este dispositivo ha excedido el número máximo de compras.',
  },
  703: {
    type: 'denegada_fraude',
    merchantMessage:
      'El comercio no acepta pagos con Tarjeta Internacional, por favor intenta con otra tarjeta.',
    userMessage:
      'Tu tarjeta no puede ser procesada por ser internacional,  Contáctanos para permitir tu compra.',
  },
  704: {
    type: 'denegada_fraude',
    merchantMessage:
      'Operación rechazada debido a que el usuario ha realizado operaciones fraudulentas previamente.',
    userMessage:
      'Lo sentimos, su usuario no puede realizar compras por el momento. Contáctanos para indicarte el motivo de rechazo.',
  },
  705: {
    type: 'denegada_fraude',
    merchantMessage:
      'Operación rechazada debido a que se han realizado operaciones fraudulentas con la misma tarjeta previamente.',
    userMessage:
      'Lo sentimos, su tarjeta no puede realizar la transacción. Contáctanos para más detalle.',
  },
  706: {
    type: 'denegada_fraude',
    merchantMessage:
      'Operación rechazada debido a que tiene una alta probabilidad de ser fraudulenta.',
    userMessage: 'Lo sentimos, su operación fue rechazada. Contáctanos para más detalle.',
  },
  707: {
    type: 'denegada_fraude',
    merchantMessage: 'Transacción por arriba del monto de pedido máximo aceptado por el comercio.',
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
  },
  708: {
    type: 'denegada_fraude',
    merchantMessage: 'Trasacción por debajo del monto de pedido mínimo aceptado por el comercio.',
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
  },
  709: {
    type: 'denegada_fraude',
    merchantMessage: 'La tarjeta del cliente ha excedido el número máximo de compras al día.',
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
  },
  710: {
    type: 'denegada_fraude',
    merchantMessage: 'La tarjeta del cliente ha excedido el número máximo de compras en la semana.',
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
  },
  711: {
    type: 'denegada_fraude',
    merchantMessage: 'La tarjeta del cliente ha excedido el número máximo de compras en el mes.',
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
  },
  712: {
    type: 'denegada_fraude',
    merchantMessage: 'La tarjeta del cliente ha excedido el monto máximo de compra diaria.',
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
  },
  713: {
    type: 'denegada_fraude',
    merchantMessage: 'La tarjeta del cliente ha excedido el monto máximo de compra semanal.',
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
  },
  714: {
    type: 'denegada_fraude',
    merchantMessage: 'La tarjeta del cliente ha excedido el monto máximo de compra mensual.',
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
  },
  715: {
    type: 'denegada_fraude',
    merchantMessage: 'El correo del cliente ha excedido el monto máximo de compra diaria.',
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
  },
  716: {
    type: 'denegada_fraude',
    merchantMessage: 'El correo del cliente ha excedido el monto máximo de compra semanal.',
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
  },
  717: {
    type: 'denegada_fraude',
    merchantMessage: 'El correo del cliente ha excedido el monto máximo de compra mensual.',
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
  },
  718: {
    type: 'denegada_fraude',
    merchantMessage: 'El correo del cliente ha excedido el número máximo de compras al día',
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
  },
  719: {
    type: 'denegada_fraude',
    merchantMessage: 'El teléfono del cliente ha excedido el número máximo de compras al día',
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
  },
  720: {
    type: 'denegada_fraude',
    merchantMessage:
      'El device fingerprint del cliente ha excedido el número máximo de compras al día',
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
  },
  721: {
    type: 'denegada_fraude',
    merchantMessage:
      'Operación rechazada debido a que el usuario ha realizado operaciones fraudulentas previamente.',
    userMessage:
      'Lo sentimos, su usuario no puede realizar compras por el momento. Contáctanos para indicarte el motivo de rechazo.',
  },
  722: {
    type: 'denegada_fraude',
    merchantMessage:
      'Operación rechazada debido a que se han realizado operaciones fraudulentas con la misma tarjeta previamente.',
    userMessage:
      'Lo sentimos, su tarjeta no puede realizar la transacción. Contáctanos para más detalle.',
  },
  723: {
    type: 'invalid_request_error',
    merchantMessage:
      'No puedes realizar la devolución por que aún no tienes los fondos suficientes en el balance de tu cuenta',
  },
  724: {
    type: 'parameter_error',
    merchantMessage: 'La tarjeta del cliente ha excedido la cantidad de intentos máximos por día',
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
  },
  726: {
    type: 'comercio_inactivo',
    code: 'ADA006',
    merchantMessage: 'El comercio no se encuentra activo.',
    userMessage: 'Contactar al comercio.',
  },
  727: {
    type: 'comercio_invalido',
    code: 'ADA005',
    merchantMessage: 'Existen problemas con la configuración del comercio o el comercio no existe.',
    userMessage: 'Contactar al comercio.',
  },
  728: {
    type: 'parameter_error',
    merchantMessage: 'El campo ‘order_number’ debe ser unico. ',
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
    param: 'order_number',
  },
  729: {
    type: 'parameter_error',
    merchantMessage: ' El campo ‘expiration_date’ debe ser una fecha en el futuro.',
    userMessage: '¡Revisa la información ingresada! La fecha de expiración es incorrecta.',
    param: 'expiration_date',
  },
  730: {
    type: 'parameter_error',
    merchantMessage: "No existe el siguiente order_id: '%s' en el entorno de integración.",
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
  },
  731: {
    type: 'parameter_error',
    merchantMessage: "No existe el siguiente order_id: '%s' en el entorno de producción",
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
  },
  732: {
    type: 'parameter_error',
    merchantMessage: "No existe el siguiente order_id: '%s'.",
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
  },
  733: {
    type: 'parameter_error',
    merchantMessage:
      'Esta orden no puede ser confirmada porque se encuentra en un estado diferente de creación.  ',
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
  },
  734: {
    type: 'parameter_error',
    merchantMessage: "El filtro 'before' tiene una orden inválido o está vacío.",
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
    param: 'before',
  },
  735: {
    type: 'parameter_error',
    merchantMessage: "El filtro 'after' tiene una orden inválido o está vacío.",
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
    param: 'after',
  },
  737: {
    type: 'parameter_error',
    merchantMessage: "No existe la orden que intentas actualizar: '%s' o tiene un formato inválido",
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
    param: 'order_number',
  },
  738: {
    type: 'parameter_error',
    merchantMessage: "No existe la orden que intentas eliminar: '%s' o tiene un formato inválido.",
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
    param: 'expiration_date',
  },
  739: {
    type: 'card_error',
    code: 'card_declined',
    declineCode: 'issuer_decline_operation',
    merchantMessage: 'Operación Denegada. Intenta nuevamente o utiliza otra tarjeta.',
    userMessage: 'Operación Denegada. Intenta nuevamente o utiliza otra tarjeta.',
  },
  740: {
    type: 'card_error',
    merchantMessage:
      'La tarjeta ingresada no puede ser utilizada para la creación del objeto card porque no permite cargos futuros.',
    userMessage: 'La tarjeta no puede ser registrada. Intente con otra tarjeta.',
    param: 'card type',
  },
  741: {
    type: 'parameter_error',
    merchantMessage: 'El campo processing_channel solo puede tener el valor REC.',
    userMessage: 'El campo processing_channel solo puede tener el valor REC.',
    param: 'error_validacion',
  },
  742: {
    type: 'parameter_error',
    merchantMessage: "El campo 'token_id' es inválido. La longitud debe ser de '25' caracteres.",
    param: 'token_id',
  },
  743: {
    type: 'parameter_error',
    merchantMessage: "El campo 'customer_id' es inválido. La longitud debe ser de '25' caracteres.",
    param: 'customer_id',
  },
  744: {
    type: 'parameter_error',
    merchantMessage: "El parámetro 'id' es inválido. La longitud debe ser de '25' caracteres.",
    param: 'id',
  },
  745: {
    type: 'parameter_error',
    merchantMessage: "El parámetro 'id' es inválido. La longitud debe ser de '25' caracteres.",
    param: 'id',
  },
  746: {
    type: 'parameter_error',
    merchantMessage: "El parámetro 'id' es inválido. La longitud debe ser de '25' caracteres.",
    param: 'id',
  },
  747: {
    type: 'parameter_error',
    merchantMessage: "El parámetro 'id' es inválido. La longitud debe ser de '25' caracteres.",
    param: 'id',
  },
  748: {
    type: 'parameter_error',
    merchantMessage: "El parámetro 'id' es inválido. La longitud debe ser de '25' caracteres.",
    param: 'id',
  },
  749: {
    type: 'parameter_error',
    merchantMessage: "El parámetro 'id' es inválido. La longitud debe ser de '25' caracteres.",
    param: 'id',
  },
  750: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'amount' es inválido o está vacío.  El valor debe ser entero y sin punto decimal. Ejemplo: 19900 (199.00 %s).",
    param: 'amount',
  },
  751: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'amount' es inválido.  El valor debe ser mayor o igual que '300' (3 %s).",
    param: 'amount',
  },
  752: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'amount' es inválido.  El valor de 'amount' debe ser de no más de 9999900 (99,999.00 %s).",
    param: 'amount',
  },
  753: {
    type: 'parameter_error',
    merchantMessage: 'La tarjeta enviada no se encuentra activa.',
    param: 'card_id',
  },
  754: {
    type: 'parameter_error',
    merchantMessage: 'Debe ingresar al menos un parámetro de búsqueda.',
  },
  755: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'order_number' no tiene un formato válido, debe tener entre 1 y 36 caracteres.",
    param: 'order_number',
  },
  756: {
    type: 'card_error',
    code: 'card_declined',
    declineCode: 'culqi_card',
    merchantMessage:
      'Tarjeta Culqi. Estás utilizando una tarjeta de pruebas de Culqi para realizar una compra real.',
    userMessage: 'Estás utilizando una tarjeta de pruebas de Culqi para realizar una compra real.',
  },
  800: {
    type: 'parameter_error',
    merchantMessage:
      'Transaccion por arriba del monto de pedido máximo aceptado por el comercio en dolares.',
    userMessage: 'El monto de la compra es inválido',
  },
  801: {
    type: 'parameter_error',
    merchantMessage:
      'Trasaccion por debajo del monto de pedido mínimo aceptado por el comercio en dolares.',
    userMessage: 'El monto de la compra es inválido',
  },
  802: {
    type: 'parameter_error',
    merchantMessage: 'La tarjeta del cliente ha excedido el monto máximo de compras por hora.',
    userMessage: 'Esta tarjeta ha excedido el monto máximo de compras.',
  },
  803: {
    type: 'parameter_error',
    merchantMessage:
      'La tarjeta del cliente ha excedido el monto máximo de compras por hora en dolares.',
    userMessage: 'Este correo ha excedido el número máximo de compras.',
  },
  804: {
    type: 'parameter_error',
    merchantMessage:
      'La tarjeta del cliente ha excedido el monto máximo de compras diaria en dolares.',
    userMessage: 'Este telefono ha excedido el número máximo de compras.',
  },
  805: {
    type: 'parameter_error',
    merchantMessage:
      'La tarjeta del cliente ha excedido el monto máximo de compras semanal en dolares.',
    userMessage: 'Este dispositivo ha excedido el número máximo de compras.',
  },
  806: {
    type: 'parameter_error',
    merchantMessage:
      'La tarjeta del cliente ha excedido el monto máximo de compras mensual en dolares.',
    userMessage:
      'Transacción por arriba del monto de pedido máximo aceptado por el comercio en dólares.',
  },
  807: {
    type: 'parameter_error',
    merchantMessage: 'La tarjeta del cliente ha excedido el número máximo de compras por segundo.',
    userMessage:
      'Transacción por debajo del monto de pedido mínimo aceptado por el comercio en dólares.',
  },
  808: {
    type: 'parameter_error',
    merchantMessage: 'La tarjeta del cliente ha excedido el número máximo de compras por minuto.',
    userMessage: 'Esta tarjeta ha excedido el monto máximo de compras.',
  },
  809: {
    type: 'parameter_error',
    merchantMessage: 'La tarjeta del cliente ha excedido el número máximo de compras por hora.',
    userMessage: 'Esta tarjeta ha excedido el número máximo de compras por hora.',
  },
  810: {
    type: 'parameter_error',
    merchantMessage: 'La tarjeta del cliente ha excedido el número máximo de compras al dia.',
    userMessage: 'Esta tarjeta ha excedido el número máximo de compras al dia.',
  },
  811: {
    type: 'parameter_error',
    merchantMessage: 'La tarjeta del cliente ha excedido el número ma¡ximo de compras semanal.',
    userMessage: 'Esta tarjeta ha excedido el número máximo de compras semanal.',
  },
  812: {
    type: 'parameter_error',
    merchantMessage: 'La tarjeta del cliente ha excedido el número máximo de compras mensual.',
    userMessage: 'Esta tarjeta ha excedido el número máximo de compras mensual.',
  },
  813: {
    type: 'parameter_error',
    merchantMessage: 'El correo del cliente ha excedido el monto máximo de compras por hora.',
    userMessage: 'Este correo ha excedido el monto máximo de compras.',
  },
  814: {
    type: 'parameter_error',
    merchantMessage:
      'El correo del cliente ha excedido el monto máximo de compras por hora en dolares.',
    userMessage: 'Este correo ha excedido el monto máximo de compras.',
  },
  815: {
    type: 'parameter_error',
    merchantMessage:
      'El correo del cliente ha excedido el monto máximo de compras al dia en dolares.',
    userMessage: 'Este correo ha excedido el monto máximo de compras.',
  },
  816: {
    type: 'parameter_error',
    merchantMessage:
      'El correo del cliente ha excedido el monto máximo de compras semanal en dolares',
    userMessage: 'Este correo ha excedido el monto máximo de compras.',
  },
  817: {
    type: 'parameter_error',
    merchantMessage:
      'El correo del cliente ha excedido el monto máximo de compras mensual en dolares.',
    userMessage: 'Este correo ha excedido el monto máximo de compras.',
  },
  818: {
    type: 'parameter_error',
    merchantMessage: 'El correo del cliente ha excedido el número máximo de compras por segundo',
    userMessage: 'Este correo ha excedido el monto máximo de compras.',
  },
  819: {
    type: 'parameter_error',
    merchantMessage: 'El correo del cliente ha excedido el número máximo de compras por minuto',
    userMessage: 'Este correo ha excedido el monto máximo de compras.',
  },
  820: {
    type: 'parameter_error',
    merchantMessage: 'El correo del cliente ha excedido el número máximo de compras por hora',
    userMessage: 'Este correo ha excedido el monto máximo de compras.',
  },
  821: {
    type: 'parameter_error',
    merchantMessage: 'El correo del cliente ha excedido el número máximo de compras semanal',
    userMessage: 'Este correo ha excedido el número máximo de compras.',
  },
  822: {
    type: 'parameter_error',
    merchantMessage: 'El correo del cliente ha excedido el número máximo de compras mensual',
    userMessage: 'Este correo ha excedido el número máximo de compras.',
  },
  823: {
    type: 'parameter_error',
    merchantMessage: 'El telefono del cliente ha excedido el monto máximo de compras por hora.',
    userMessage: 'Este teléfono ha excedido el monto máximo de compras.',
  },
  824: {
    type: 'parameter_error',
    merchantMessage: 'El telefono del cliente ha excedido el monto máximo de compras al dia.',
    userMessage: 'Este teléfono ha excedido el monto máximo de compras.',
  },
  825: {
    type: 'parameter_error',
    merchantMessage: 'El telefono del cliente ha excedido el monto máximo de compras semanal.',
    userMessage: 'Este teléfono ha excedido el monto máximo de compras.',
  },
  826: {
    type: 'parameter_error',
    merchantMessage: 'El telefono del cliente ha excedido el monto máximo de compras mensual.',
    userMessage: 'Este teléfono ha excedido el monto máximo de compras.',
  },
  827: {
    type: 'parameter_error',
    merchantMessage:
      'El telefono del cliente ha excedido el monto máximo de compras por hora en dolares',
    userMessage: 'Este teléfono ha excedido el monto máximo de compras.',
  },
  828: {
    type: 'parameter_error',
    merchantMessage:
      'El telefono del cliente ha excedido el monto máximo de compras al dia en dolares',
    userMessage: 'Este teléfono ha excedido el monto máximo de compras.',
  },
  829: {
    type: 'parameter_error',
    merchantMessage:
      'El telefono del cliente ha excedido el monto máximo de compras semanal en dolares',
    userMessage: 'Este teléfono ha excedido el monto máximo de compras.',
  },
  830: {
    type: 'parameter_error',
    merchantMessage:
      'El telefono del cliente ha excedido el monto máximo de compras mensual en dolares',
    userMessage: 'Este teléfono ha excedido el monto máximo de compras.',
  },
  831: {
    type: 'parameter_error',
    merchantMessage: 'El telefono del cliente ha excedido el número máximo de compras por segundo',
    userMessage: 'Este teléfono ha excedido el monto máximo de compras.',
  },
  832: {
    type: 'parameter_error',
    merchantMessage: 'El telefono del cliente ha excedido el número máximo de compras por minuto',
    userMessage: 'Este teléfono ha excedido el monto máximo de compras.',
  },
  833: {
    type: 'parameter_error',
    merchantMessage: 'El telefono del cliente ha excedido el número máximo de compras por hora',
    userMessage: 'Este teléfono ha excedido el número máximo de compras.',
  },
  834: {
    type: 'parameter_error',
    merchantMessage: 'El telefono del cliente ha excedido el número máximo de compras semanal',
    userMessage: 'Este teléfono ha excedido el número máximo de compras.',
  },
  835: {
    type: 'parameter_error',
    merchantMessage: 'El telefono del cliente ha excedido el número máximo de compras mensual',
    userMessage: 'Este teléfono ha excedido el número máximo de compras.',
  },
  836: {
    type: 'parameter_error',
    merchantMessage:
      'El device fingerprint del cliente ha excedido el monto máximo de compras por hora',
    userMessage: 'Este dispositivo ha excedido el monto máximo de compras.',
  },
  837: {
    type: 'parameter_error',
    merchantMessage:
      'El device fingerprint del cliente ha excedido el monto máximo de compras al dia',
    userMessage: 'Este dispositivo ha excedido el monto máximo de compras.',
  },
  838: {
    type: 'parameter_error',
    merchantMessage:
      'El device fingerprint del cliente ha excedido el monto máximo de compras semanal',
    userMessage: 'Este dispositivo ha excedido el monto máximo de compras.',
  },
  839: {
    type: 'parameter_error',
    merchantMessage:
      'El device fingerprint del cliente ha excedido el monto máximo de compras mensual',
    userMessage: 'Este dispositivo ha excedido el monto máximo de compras.',
  },
  840: {
    type: 'parameter_error',
    merchantMessage:
      'El device fingerprint del cliente ha excedido el monto máximo de compras por hora en dolares',
    userMessage: 'Este dispositivo ha excedido el monto máximo de compras.',
  },
  841: {
    type: 'parameter_error',
    merchantMessage:
      'El device fingerprint del cliente ha excedido el monto máximo de compras al dia en dolares',
    userMessage: 'Este dispositivo ha excedido el monto máximo de compras.',
  },
  842: {
    type: 'parameter_error',
    merchantMessage:
      'El device fingerprint del cliente ha excedido el monto máximo de compras semanal en dolares',
    userMessage: 'Este dispositivo ha excedido el monto máximo de compras.',
  },
  843: {
    type: 'parameter_error',
    merchantMessage:
      'El device fingerprint del cliente ha excedido el monto máximo de compras mensual en dolares',
    userMessage: 'Este dispositivo ha excedido el monto máximo de compras.',
  },
  844: {
    type: 'parameter_error',
    merchantMessage:
      'El device fingerprint del cliente ha excedido el número máximo de compras por segundo',
    userMessage: 'Este dispositivo ha excedido el monto máximo de compras.',
  },
  845: {
    type: 'parameter_error',
    merchantMessage:
      'El device fingerprint del cliente ha excedido el número máximo de compras por minuto',
    userMessage: 'Este dispositivo ha excedido el número máximo de compras.',
  },
  846: {
    type: 'parameter_error',
    merchantMessage:
      'El device fingerprint del cliente ha excedido el número máximo de compras por hora',
    userMessage: 'Este dispositivo ha excedido el número máximo de compras.',
  },
  847: {
    type: 'parameter_error',
    merchantMessage:
      'El device fingerprint del cliente ha excedido el número máximo de compras semanal',
    userMessage: 'Este dispositivo ha excedido el número máximo de compras.',
  },
  848: {
    type: 'parameter_error',
    merchantMessage:
      'El device fingerprint del cliente ha excedido el número máximo de compras mensual',
    userMessage: 'Este dispositivo ha excedido el número máximo de compras.',
  },
  849: {
    type: 'parameter_error',
    merchantMessage: 'El comercio ha excedido el número de intentos por token en un segundo.',
    userMessage: 'El comercio ha excedido el número de intentos por token.',
  },
  850: {
    type: 'parameter_error',
    merchantMessage: 'El comercio ha excedido el número de intentos por token en un minuto.',
    userMessage: 'El comercio ha excedido el número de intentos por token.',
  },
  851: {
    type: 'parameter_error',
    merchantMessage: 'El comercio ha excedido el número de intentos por token en una hora.',
    userMessage: 'El comercio ha excedido el número de intentos por token.',
  },
  852: {
    type: 'parameter_error',
    merchantMessage: 'El comercio ha excedido el número de intentos por token diario.',
    userMessage: 'El comercio ha excedido el número de intentos por token.',
  },
  853: {
    type: 'parameter_error',
    merchantMessage: 'El comercio ha excedido el número de intentos por token semanal.',
    userMessage: 'El comercio ha excedido el número de intentos por token.',
  },
  854: {
    type: 'parameter_error',
    merchantMessage: 'El comercio ha excedido el número de intentos por token mensual.',
    userMessage: 'El comercio ha excedido el número de intentos por token.',
  },
  855: {
    type: 'parameter_error',
    merchantMessage: 'El correo del cliente ha excedido el número máximo de tokens por segundo',
    userMessage: 'Este cliente ha excedido el número máximo de tokens.',
  },
  856: {
    type: 'parameter_error',
    merchantMessage: 'El correo del cliente ha excedido el número máximo de tokens por minuto',
    userMessage: 'Este cliente ha excedido el número máximo de tokens.',
  },
  857: {
    type: 'parameter_error',
    merchantMessage: 'El correo del cliente ha excedido el número máximo de tokens por hora',
    userMessage: 'Este cliente ha excedido el número máximo de tokens.',
  },
  858: {
    type: 'parameter_error',
    merchantMessage: 'El correo del cliente ha excedido el número máximo de tokens diario',
    userMessage: 'Este cliente ha excedido el número máximo de tokens.',
  },
  859: {
    type: 'parameter_error',
    merchantMessage: 'El correo del cliente ha excedido el número máximo de tokens semanal',
    userMessage: 'Este cliente ha excedido el número máximo de tokens.',
  },
  860: {
    type: 'parameter_error',
    merchantMessage: 'El correo del cliente ha excedido el número máximo de tokens mensual',
    userMessage: 'Este cliente ha excedido el número máximo de tokens.',
  },
  861: {
    type: 'parameter_error',
    merchantMessage: 'La tarjeta del cliente ha excedido el número máximo de tokens por segundo',
    userMessage: 'Esta tarjeta ha excedido el número máximo de tokens.',
  },
  862: {
    type: 'parameter_error',
    merchantMessage: 'La tarjeta del cliente ha excedido el número máximo de tokens por minuto',
    userMessage: 'Esta tarjeta ha excedido el número máximo de tokens.',
  },
  863: {
    type: 'parameter_error',
    merchantMessage: 'La tarjeta del cliente ha excedido el número máximo de tokens por hora',
    userMessage: 'Esta tarjeta ha excedido el número máximo de tokens.',
  },
  864: {
    type: 'parameter_error',
    merchantMessage: 'La tarjeta del cliente ha excedido el número máximo de tokens diario',
    userMessage: 'Esta tarjeta ha excedido el número máximo de tokens.',
  },
  865: {
    type: 'parameter_error',
    merchantMessage: 'La tarjeta del cliente ha excedido el número máximo de tokens semanal',
    userMessage: 'Esta tarjeta ha excedido el número máximo de tokens.',
  },
  866: {
    type: 'parameter_error',
    merchantMessage: 'La tarjeta del cliente ha excedido el número máximo de tokens mensual',
    userMessage: 'Esta tarjeta ha excedido el número máximo de tokens.',
  },
  867: {
    type: 'parameter_error',
    merchantMessage: 'El correo del cliente ha excedido el número máximo de intentos por segundo.',
    userMessage: 'Este cliente ha excedido el número máximo de intentos de compra.',
  },
  868: {
    type: 'parameter_error',
    merchantMessage: 'El correo del cliente ha excedido el número máximo de intentos por minuto.',
    userMessage: 'Este cliente ha excedido el número máximo de intentos de compra',
  },
  869: {
    type: 'parameter_error',
    merchantMessage: 'El correo del cliente ha excedido el número máximo de intentos por hora.',
    userMessage: 'Este cliente ha excedido el número máximo de intentos de compra',
  },
  870: {
    type: 'parameter_error',
    merchantMessage: 'El correo del cliente ha excedido el número máximo de intentos diario.',
    userMessage: 'Este cliente ha excedido el número máximo de intentos de compra',
  },
  871: {
    type: 'parameter_error',
    merchantMessage: 'El correo del cliente ha excedido el número máximo de intentos semanal.',
    userMessage: 'Este cliente ha excedido el número máximo de intentos de compra',
  },
  872: {
    type: 'parameter_error',
    merchantMessage: 'El correo del cliente ha excedido el número máximo de intentos mensual.',
    userMessage: 'Este cliente ha excedido el número máximo de intentos de compra',
  },
  873: {
    type: 'parameter_error',
    merchantMessage: 'La tarjeta del cliente ha excedido el número máximo de intentos por segundo.',
    userMessage: 'Esta tarjeta ha excedido el número máximo de intentos de compra .',
  },
  874: {
    type: 'parameter_error',
    merchantMessage: 'La tarjeta del cliente ha excedido el número máximo de intentos por minuto.',
    userMessage: 'Esta tarjeta ha excedido el número máximo de intentos de compra ',
  },
  875: {
    type: 'parameter_error',
    merchantMessage: 'La tarjeta del cliente ha excedido el número máximo de intentos por hora.',
    userMessage: 'Esta tarjeta ha excedido el número máximo de intentos de compra',
  },
  876: {
    type: 'parameter_error',
    merchantMessage: 'La tarjeta del cliente ha excedido el número máximo de intentos diario.',
    userMessage: 'Esta tarjeta ha excedido el número máximo de intentos de compra ',
  },
  877: {
    type: 'parameter_error',
    merchantMessage: 'La tarjeta del cliente ha excedido el número máximo de intentos semanal.',
    userMessage: 'Esta tarjeta ha excedido el número máximo de intentos de compra .',
  },
  878: {
    type: 'parameter_error',
    merchantMessage: 'La tarjeta del cliente ha excedido el número máximo de intentos mensual.',
    userMessage: 'Esta tarjeta ha excedido el número máximo de intentos de compra .',
  },
  879: {
    type: 'parameter_error',
    merchantMessage:
      'El teléfono del cliente ha excedido el número máximo de intentos por segundo.',
    userMessage: 'Este teléfono ha excedido el número máximo de intentos de compra.',
  },
  880: {
    type: 'parameter_error',
    merchantMessage: 'El teléfono del cliente ha excedido el número máximo de intentos por minuto.',
    userMessage: 'Este teléfono ha excedido el número máximo de intentos de compra.',
  },
  881: {
    type: 'parameter_error',
    merchantMessage: 'El teléfono del cliente ha excedido el número máximo de intentos por hora.',
    userMessage: 'Este teléfono ha excedido el número máximo de intentos de compra.',
  },
  882: {
    type: 'parameter_error',
    merchantMessage: 'El teléfono del cliente ha excedido el número máximo de intentos diario.',
    userMessage: 'Este teléfono ha excedido el número máximo de intentos de compra.',
  },
  883: {
    type: 'parameter_error',
    merchantMessage: 'El teléfono del cliente ha excedido el número máximo de intentos semanal.',
    userMessage: 'Este teléfono ha excedido el número máximo de intentos de compra.',
  },
  884: {
    type: 'parameter_error',
    merchantMessage: 'El teléfono del cliente ha excedido el número máximo de intentos mensual.',
    userMessage: 'Este teléfono ha excedido el número máximo de intentos de compra.',
  },
  885: {
    type: 'parameter_error',
    merchantMessage:
      'El dispositivo del cliente ha excedido el número máximo de intentos por segundo.',
    userMessage: 'Este dispositivo ha excedido el número máximo de intentos de compra.',
  },
  886: {
    type: 'parameter_error',
    merchantMessage:
      'El dispositivo del cliente ha excedido el número máximo de intentos por minuto.',
    userMessage: 'Este dispositivo ha excedido el número máximo de intentos de compra.',
  },
  887: {
    type: 'parameter_error',
    merchantMessage:
      'El dispositivo del cliente ha excedido el número máximo de intentos por hora.',
    userMessage: 'Este dispositivo ha excedido el número máximo de intentos de compra.',
  },
  888: {
    type: 'parameter_error',
    merchantMessage: 'El dispositivo del cliente ha excedido el número máximo de intentos diario.',
    userMessage: 'Este dispositivo ha excedido el número máximo de intentos de compra.',
  },
  889: {
    type: 'parameter_error',
    merchantMessage: 'El dispositivo del cliente ha excedido el número máximo de intentos semanal.',
    userMessage: 'Este dispositivo ha excedido el número máximo de intentos de compra.',
  },
  890: {
    type: 'parameter_error',
    merchantMessage: 'El dispositivo del cliente ha excedido el número máximo de intentos mensual.',
    userMessage: 'Este dispositivo ha excedido el número máximo de intentos de compra.',
  },
  891: {
    type: 'parameter_error',
    merchantMessage:
      'El comercio ha excedido el monto máximo acumulado de compras diario permitido.',
    userMessage: 'El comercio ha excedido el monto máximo acumulado de compras permitido.',
  },
  892: {
    type: 'parameter_error',
    merchantMessage:
      'El comercio ha excedido el monto máximo acumulado de compras semanal permitido.',
    userMessage: 'El comercio ha excedido el monto máximo acumulado de compras permitido.',
  },
  893: {
    type: 'parameter_error',
    merchantMessage:
      'El comercio ha excedido el monto máximo acumulado de compras mensual permitido.',
    userMessage: 'El comercio ha excedido el monto máximo acumulado de compras permitido.',
  },
  894: {
    type: 'parameter_error',
    merchantMessage:
      'El comercio ha excedido el monto máximo acumulado de compras diario en dólares permitido.',
    userMessage: 'El comercio ha excedido el monto máximo acumulado de compras en permitido.',
  },
  895: {
    type: 'parameter_error',
    merchantMessage:
      'El comercio ha excedido el monto máximo acumulado de compras semanal en dólares permitido.',
    userMessage: 'El comercio ha excedido el monto máximo acumulado de compras en permitido.',
  },
  896: {
    type: 'parameter_error',
    merchantMessage:
      'El comercio ha excedido el monto máximo acumulado de compras mensual en dólares permitido.',
    userMessage: 'El comercio ha excedido el monto máximo acumulado de compras en permitido.',
  },
  897: {
    type: 'parameter_error',
    code: 'invalid_cvv',
    merchantMessage: 'Código de seguridad (CVV) no ingresado',
    userMessage:
      'El código de seguridad (CVV) no ha sido ingresado. Intente nuevamente ó utilice otra tarjeta.',
  },
  898: {
    type: 'invalid_request_error',
    merchantMessage: 'No puedes devolver una transacción con antigüedad mayor a 6 meses.',
  },
  899: {
    type: 'invalid_request_error',
    merchantMessage:
      'No puedes realizar la devolución por que el monto a devolver es menor al monto mínimo.',
  },
  900: {
    type: 'invalid_request_error',
    merchantMessage:
      'No puedes realizar la devolución por que el monto final de la transacción sería inferior al monto mínimo.',
  },
  901: {
    type: 'api_error',
    merchantMessage:
      'Ocurrió un error al realizar la operación. Favor intenta nuevamente o contáctanos para ayudarte.',
  },
  902: {
    type: 'operacion_denegada',
    code: 'DNGE0680',
    declineCode: 'soft_block',
    merchantMessage:
      'Tarjeta con bloqueo temporal por reintentos. El cliente debe intentar nuevamente con otra tarjeta. (680).',
    userMessage: 'Operación denegada. Intente nuevamente con otra tarjeta.',
  },
  903: {
    type: 'api_error',
    code: 'CAP001',
    merchantMessage:
      'El proceso de captura no ha podido ser ejecutado. Por favor, intente nuevamente.',
    userMessage: 'El proceso de captura no ha podido ser ejecutado. Por favor, intente nuevamente',
  },
  904: {
    type: 'tarjeta_perdida',
    code: 'DNGE0030',
    merchantMessage:
      'Tarjeta perdida. La tarjeta fue bloqueada y reportada al banco emisor como una tarjeta perdida.',
    userMessage:
      'Lo sentimos, no podemos procesar la tarjeta ingresada. Por favor, intente con otra tarjeta.',
  },
  905: {
    type: 'tarjeta_robada',
    code: 'DNGE0031',
    merchantMessage:
      'Tarjeta robada. La tarjeta fue bloqueada y reportada al banco emisor como una tarjeta robada.',
    userMessage:
      'Lo sentimos, no podemos procesar la tarjeta ingresada. Por favor, intente con otra tarjeta.',
  },
  906: {
    type: 'tarjeta_vencida',
    code: 'DNGE0053',
    merchantMessage: 'Tarjeta vencida. La tarjeta almacenada utilizada se encuentra vencida.',
    userMessage:
      'Lo sentimos, no podemos procesar la tarjeta ingresada. Por favor, intente con otra tarjeta.',
  },
  907: {
    type: 'denegada_fraude',
    merchantMessage:
      'Tarjeta bloqueada. La tarjeta fue bloqueada y reportada al banco emisor como una tarjeta bloqueada.',
    userMessage:
      'Lo sentimos, no podemos procesar la tarjeta ingresada. Por favor, intente con otra tarjeta.',
  },
  908: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'amount' es inválido.  El valor de 'amount' debe ser de no más de '%s' ('%s' '%s').",
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
    param: 'amount',
  },
  909: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'amount' es inválido.  El valor debe ser mayor o igual que '600' (S/. 6.00 ó $ 6.00).",
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
    param: 'amount',
  },
  999: {
    type: 'api_error',
    code: 'DNGE0999',
    merchantMessage:
      'Operación denegada. La operación fue denegada por el banco emisor de la tarjeta por una razón desconocida',
    userMessage:
      'La operación ha sido denegada por la entidad emisora de tu tarjeta. Contáctate con el banco para conocer el motivo de la denegación o intenta nuevamente con otra tarjeta.',
  },
  1000: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'application_fee' es inválido.  El valor debe ser entero y sin punto decimal. Ejemplo: 19900 (199.00 soles)",
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
    param: 'application_fee',
  },
  1001: {
    type: 'parameter_error',
    merchantMessage: 'Tarjeta Inválida.',
    userMessage: 'Su tarjeta es inválida. Contáctese con la entidad emisora de su tarjeta. ',
  },
  1002: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'document_number' es inválido o está vacío, debe ser mayor de 8 y menor de 14 caracteres. Revisar la referencia del API en https://www.culqi.com/api.",
    userMessage: 'Recuerda que debes ingresar un documento válido de 8 a 12 números.',
    param: 'document_number',
  },
  1003: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'document_type' es inválido o está vacío, debe ser uno de los valores permitidos (dni, ce, pas, ruc, other). Revisar la referencia del API en https://www.culqi.com/api.",
    userMessage: 'Por favor ingresa tu tipo de documento correctamente.',
    param: 'document_type',
  },
  1004: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'document_number' es inválido o está vacío, debe ser de %s caracteres para document_type = %s. Revisar la referencia del API en https://www.culqi.com/api.",
    userMessage: 'Por favor ingresa tu número de documento correctamente.',
    param: 'document_number',
  },
  1005: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'modification_date_from' tiene un valor inválido o está vacío. El valor debe estar en formato formato UNIX Time Stamp. Ejem: 1486150271.",
    param: 'modification_date_from',
  },
  1006: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'modification_date_to' tiene un valor inválido o está vacío. El valor debe estar en formato formato UNIX Time Stamp. Ejem: 1486150271.",
    param: 'modification_date_to',
  },
  1007: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'status' tiene un valor inválido o está vacio. Estos son los únicos valores permitidos: 'completa', 'pendiente' o 'rechazada'.",
    param: 'status',
  },
  1008: {
    type: 'api_error',
    merchantMessage: 'No pudimos procesar tu operación. Por favor, intenta nuevamente.',
    userMessage: 'No pudimos procesar tu operación. Por favor, intenta nuevamente.',
  },
  1009: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'order_types' tiene un valor inválido o está vacio. Estos son los únicos valores permitidos: 'cip' o 'cuotealo'.",
    userMessage:
      '¡Lo sentimos! No podemos procesar tu operación. Contáctate con soporte al cliente del negocio.',
    param: 'order_types',
  },
  1010: {
    type: 'api_error',
    merchantMessage: "El comercio no puede soportar el tipo '%s'.",
    userMessage:
      '¡Lo sentimos! No podemos procesar tu operación. Contáctate con soporte al cliente del negocio.',
    param: 'order_types',
  },
  1011: {
    type: 'authentication_error',
    merchantMessage:
      "Enviaste un formato de autenticación erróneo, colocaste el header 'X-AUTHORIZATION-EXPRESS' pero sin el formato correcto con Basic. Usando la autenticación Basic, el header de 'X-AUTHORIZATION-EXPRESS' sería algo así: 'X-AUTHORIZATION-EXPRESS: Basic CREDENCIALES_BASE64'",
  },
  1012: {
    type: 'authentication_error',
    merchantMessage:
      "Las credenciales que enviaste en el header 'X-AUTHORIZATION-EXPRESS' son inválidas.",
  },
  1013: {
    type: 'validacion_3ds',
    merchantMessage:
      'Tu transacción necesita una validación 3DS para asegurar la identidad del comprador.',
    userMessage:
      'Tu transacción necesita una validación 3DS para asegurar la identidad del comprador.',
  },
  1014: {
    type: 'parameter_error',
    merchantMessage:
      'No se pudo realizar la transacción debido a que la configuración de la tarjeta acepta un único pago.',
    param: 'source_id',
  },
  1015: {
    type: 'parameter_error',
    merchantMessage:
      'No se pudo realizar la transacción debido a que la configuración de la tarjeta.',
    param: 'source_id',
  },
  1016: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'date_from' tiene un valor inválido. El valor debe estar en formato formato UNIX Time Stamp. Ejem: 1486150271.",
    param: 'date_from',
  },
  1017: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'date_to' tiene un valor inválido. El valor debe estar en formato formato UNIX Time Stamp. Ejem: 1486150271.",
    param: 'date_to',
  },
  1018: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'operation_type' tiene un valor inválido. Los valores permitidos son: Authorized, Canceled, Return, Fraud",
    param: 'operation_type',
  },
  1019: {
    type: 'parameter_error',
    merchantMessage: "El filtro 'page' tiene un valor inválido.",
    param: 'page',
  },
  1020: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'sort' tiene un valor inválido. Los valores permitidos son: ASC, DESC",
    param: 'sort',
  },
  1021: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'limit' tiene un valor inválido. Tiene que ser numérico del 1 al 200",
    param: 'limit',
  },
  1022: {
    type: 'parameter_error',
    merchantMessage:
      "El filtro 'currency' tiene un valor inválido o está vacío, se debe colocar código de la moneda en tres letras (Formato ISO 4217). Culqi actualmente soporta las siguientes monedas: USD, PEN.",
    param: 'currency',
  },
  1027: {
    type: 'parameter_error',
    merchantMessage: "El campo 'charge_id' es inválido o está vacío.",
    param: 'charge_id',
  },
  1028: {
    type: 'card_error',
    code: 'card_declined',
    merchantMessage: 'Tarjeta bloqueada para compras por Internet',
    userMessage: ' Tarjeta Bloqueada para compras por Internet',
  },
  1029: {
    type: 'card_error',
    code: 'card_declined',
    merchantMessage:
      'Tarjeta inactiva para compras por internet o denegada por prevención de fraude del banco.',
    userMessage:
      ' Tarjeta inactiva para compras por internet o denegada por prevención de fraude del banco.',
  },
  1030: {
    type: 'parameter_error',
    merchantMessage:
      'Lo sentimos, no pudimos procesar tu operación. Los pagos con Cuotéalo deben tener un monto mínimo de $30 y máximo de $2000.',
    userMessage:
      'Los pagos en Cuotéalo son de mínimo $30 y máximo de $2000. Por favor, revisa el monto a pagar.',
    param: 'amount',
  },
  1031: {
    type: 'parameter_error',
    merchantMessage:
      'Lo sentimos, no pudimos procesar tu operación. Los pagos con Cuotéalo deben tener un monto mínimo de S/100 y máximo de S/7000.',
    userMessage:
      'Los pagos en Cuotéalo son de mínimo S/100 y máximo de S/7000. Por favor, revisa el monto a pagar.',
    param: 'amount',
  },
  1032: {
    type: 'parameter_error',
    merchantMessage:
      "El campo 'currency_code' es inválido o está vacío, el código de la moneda en tres letras (Formato ISO 4217). Culqi con Yape actualmente soporta las siguientes monedas: PEN'.",
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
    param: 'currency_code',
  },
  1035: {
    type: 'validacion_3ds',
    merchantMessage: 'Los parámetros de autenticación 3DS son incorrectos',
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
    param: 'authentication_3DS',
  },
  1036: {
    type: 'validacion_3ds',
    merchantMessage: "No se encontraron parámetros 3DS asociados con el token: '%s'",
    userMessage: 'Hubo algunos problemas al intentar validar tu compra. Contáctanos para ayudarte.',
    param: 'authentication_3DS',
  },
};

export default errors;
