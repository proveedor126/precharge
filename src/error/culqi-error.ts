export enum ExceptionType {
  Validation,
  Authorization,
  Internal,
}

export abstract class CulqiError extends Error {
  constructor(
    public responseCode: number,
    public exceptionType: ExceptionType,
    public chargeId?: string,
  ) {
    super(`The following Culqi Error has been thrown: ${responseCode}`);
  }
}
