import { CulqiError, ExceptionType } from './culqi-error';

export default class ValidationCulqiError extends CulqiError {
  constructor(responseNumber: number, chargeId?: string) {
    super(responseNumber, ExceptionType.Validation, chargeId);
  }
}
