import { CulqiError, ExceptionType } from './culqi-error';

export default class UnexpectedError extends CulqiError {
  constructor(chargeId?: string) {
    super(669, ExceptionType.Internal, chargeId);
  }
}
