import middy from '@middy/core';
import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';
import { CulqiError, ExceptionType } from '../../error/culqi-error';
import container from '../../infrastructure/di/di-container';
import ConsoleLogger from '../../infrastructure/logger/console.logger';
import errors from '../../error/error-list';
const errorFilterMiddleware = (): middy.MiddlewareObj<
  APIGatewayProxyEvent,
  APIGatewayProxyResult
> => {
  const onError: middy.MiddlewareFn<APIGatewayProxyEvent, APIGatewayProxyResult, unknown> = async (
    request,
  ): Promise<void> => {
    try {
      const logger = container.resolve<ConsoleLogger>('ConsoleLogger');

      const generalError = request?.error as Error;

      logger.error(
        'Error caught when processing a request',
        generalError.message,
        generalError.stack,
      );

      const culqiError = request?.error as CulqiError;

      const error = errors[culqiError.responseCode];

      if (!error) {
        throw new Error('No valid error has been caught');
      }

      let statusCode = 500;

      switch (culqiError.exceptionType) {
        case ExceptionType.Authorization:
          statusCode = 401;
        case ExceptionType.Validation:
          statusCode = 400;
      }

      request.response = {
        statusCode,
        body: JSON.stringify({ code: culqiError.responseCode, ...error }),
      };
    } catch (err) {
      request.response = {
        statusCode: 500,
        body: JSON.stringify({ code: 669, ...errors[669] }),
      };
    }
  };

  return {
    onError,
  };
};

export default errorFilterMiddleware;
