import middy from '@middy/core';
import { APIGatewayProxyResult } from 'aws-lambda';
import { ZodError } from 'zod';
import CulqiLambdaEvent from '../event/culqi-lambda.event';
import container from '../../infrastructure/di/di-container';
import ConsoleLogger from '../../infrastructure/logger/console.logger';
import ValidationCulqiError from '../../error/validation.culqi-error';

const validateAndParseBodyMiddleware = (): middy.MiddlewareObj<
  CulqiLambdaEvent<unknown>,
  APIGatewayProxyResult
> => {
  const before: middy.MiddlewareFn<CulqiLambdaEvent<unknown>, APIGatewayProxyResult> = async (
    request,
  ): Promise<void> => {
    if (request?.event?.body && request?.event?.controllerData?.validateSchema) {
      try {
        const bodyRequest = request.event.controllerData.validateSchema.parse(
          JSON.parse(request?.event?.body),
        );
        request.event.request = bodyRequest;
      } catch (err) {
        const validationError = err as ZodError;
        const logger = container.resolve<ConsoleLogger>('ConsoleLogger');
        logger.error('Request Body is not valid', { validationErrors: validationError.issues });
        const errorCode = Number.parseInt(validationError.issues[0].message);
        throw new ValidationCulqiError(errorCode);
      }
    }
  };

  return {
    before,
  };
};

export default validateAndParseBodyMiddleware;
