import middy from '@middy/core';
import { APIGatewayProxyResult } from 'aws-lambda';
import CulqiLambdaEvent from '../event/culqi-lambda.event';
import ControllerManager from '../controller/controller-manager';
import { CulqiError } from '../../error/culqi-error';
import UnexpectedError from '../../error/unexpected-error';

const routeRequestMiddleware = (): middy.MiddlewareObj<
  CulqiLambdaEvent<unknown>,
  APIGatewayProxyResult
> => {
  const before: middy.MiddlewareFn<CulqiLambdaEvent<unknown>, APIGatewayProxyResult> = async (
    request,
  ): Promise<void> => {
    const controllerData = await ControllerManager.getControllerData(
      request.event.path,
      request.event.httpMethod,
    );

    if (!controllerData) {
      throw new UnexpectedError();
    }

    request.event.controllerData = controllerData;
  };

  return {
    before,
  };
};

export default routeRequestMiddleware;
