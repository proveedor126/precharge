import middy from '@middy/core';
import { APIGatewayProxyResult } from 'aws-lambda';
import CulqiLambdaEvent from '../event/culqi-lambda.event';
import container from '../../infrastructure/di/di-container';
import TextDecompressor from '../../infrastructure/adapter/text-decompressor.adapter';
import MerchantDataDto, { merchantDataSchema } from '../../application/dto/merchant-data.dto';

const parseMerchantHeaderMiddleware = (): middy.MiddlewareObj<
  CulqiLambdaEvent<unknown>,
  APIGatewayProxyResult
> => {
  const before: middy.MiddlewareFn<CulqiLambdaEvent<unknown>, APIGatewayProxyResult> = async (
    request,
  ): Promise<void> => {
    const textDecompressor = container.resolve<TextDecompressor>('TextDecompressor');

    const merchantCacheData = request.event.headers['Merchant-Cache-Data'];

    if (merchantCacheData) {
      const decompressedText = await textDecompressor.decompress(merchantCacheData);

      const parseResult = await merchantDataSchema.safeParse(JSON.parse(decompressedText));

      if (parseResult.success) {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const requestObject = request.event.request as any;
        requestObject['merchantData'] = parseResult.data;
      }
    }
  };

  return {
    before,
  };
};

export default parseMerchantHeaderMiddleware;
