import middy from '@middy/core';
import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';
import container from '../../infrastructure/di/di-container';
import ConsoleLogger from '../../infrastructure/logger/console.logger';

const inOutLoggerMiddleware = (): middy.MiddlewareObj<
  APIGatewayProxyEvent,
  APIGatewayProxyResult
> => {
  const before: middy.MiddlewareFn<APIGatewayProxyEvent, APIGatewayProxyResult> = async (
    request,
  ): Promise<void> => {
    const logger = container.resolve<ConsoleLogger>('ConsoleLogger');
    const { queryStringParameters, pathParameters, body, path, headers, httpMethod } =
      request.event;
    logger.info('Incoming Request', {
      queryStringParameters,
      pathParameters,
      headers,
      httpMethod,
      path,
      body,
    });
  };

  const after: middy.MiddlewareFn<APIGatewayProxyEvent, APIGatewayProxyResult> = async (
    request,
  ): Promise<void> => {
    const logger = container.resolve<ConsoleLogger>('ConsoleLogger');
    const { queryStringParameters, pathParameters, body, path, headers, httpMethod } =
      request.event;
    logger.info('Request Processed', {
      queryStringParameters,
      pathParameters,
      headers,
      httpMethod,
      path,
      body,
      statusCode: request.response?.statusCode,
      responseBody: request.response?.body,
      responseHeaders: request.response?.headers,
    });
  };

  return {
    after,
    before,
  };
};

export default inOutLoggerMiddleware;
