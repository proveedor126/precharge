import middy from '@middy/core';
import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';
import CulqiLambdaEvent from '../event/culqi-lambda.event';

export enum EventParameterType {
  QueryString,
  Path,
}

const queryStringParameterExtractor = (
  event: CulqiLambdaEvent<unknown>,
): { [key: string]: string | undefined } | null => event.queryStringParameters;

const pathParameterExtractor = (
  event: CulqiLambdaEvent<unknown>,
): { [key: string]: string | undefined } | null => {
  const { actionPath, controllerPath } = event.controllerData;

  const endpoint = actionPath ? `/${actionPath}` : '';

  const fullPath = `${controllerPath}${endpoint}`;

  const splittedRequestPath = event.path.split('/');
  const splittedRoute = fullPath.split('/');

  if (splittedRequestPath.length !== splittedRoute.length) {
    return {};
  }

  const parameters: { [key: string]: string } = {};

  for (let i = 0; i < splittedRequestPath.length; i++) {
    const currentRoutePath = splittedRoute[i];
    const currentRequestPathPart = splittedRequestPath[i];

    if (!currentRoutePath || !currentRoutePath.startsWith(':')) {
      continue;
    }

    const key = currentRoutePath.replace(':', '');

    parameters[key] = currentRequestPathPart;
  }

  return parameters;
};

const extractors = {
  [EventParameterType.QueryString]: queryStringParameterExtractor,
  [EventParameterType.Path]: pathParameterExtractor,
};

const parseParametersMiddleware = <T>(
  constructor: new () => T,
  parameterType: EventParameterType,
): middy.MiddlewareObj<CulqiLambdaEvent<T>, APIGatewayProxyResult> => {
  const before: middy.MiddlewareFn<CulqiLambdaEvent<T>, APIGatewayProxyResult> = async (
    request,
  ): Promise<void> => {
     const parameters = extractors[parameterType](request.event);

    if (parameters) {
      if (!request.event.request) {
        request.event.request = new constructor();
      }

      for (const key in parameters) {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const requestObject = request.event.request as any;

        if (!requestObject.hasOwnProperty(key)) {
          requestObject[key] = parameters[key];
        }
      }
    }
  };

  return {
    before,
  };
};

export default parseParametersMiddleware;
