import middy from '@middy/core';
import handler from './handler';
import ValidateChargeRequestDto from '../../application/dto/validate-charge-request.dto';
import parseParametersMiddleware, {
  EventParameterType,
} from '../middleware/parse-parameters.middleware';
import errorFilterMiddleware from '../middleware/error-filter.middleware';
import inOutLoggerMiddleware from '../middleware/in-out-logger.middleware';
import routeRequestMiddleware from '../middleware/route-request.middleware';
import validateAndParseBodyMiddleware from '../middleware/validate-and-parse-body.middleware';
import ControllerManager from '../controller/controller-manager';
import controllers from '../controller/controllers';
import parseMerchantHeaderMiddleware from '../middleware/parse-merchant-header.middleware';

ControllerManager.registerControllers(controllers);

const culqiFunction = middy(handler)
  .use(inOutLoggerMiddleware())
  .use(routeRequestMiddleware())
  .use(validateAndParseBodyMiddleware())
  .use(
    parseParametersMiddleware<ValidateChargeRequestDto>(
      ValidateChargeRequestDto,
      EventParameterType.QueryString,
    ),
  )
  .use(
    parseParametersMiddleware<ValidateChargeRequestDto>(
      ValidateChargeRequestDto,
      EventParameterType.Path,
    ),
  )
  .use(parseMerchantHeaderMiddleware())
  .use(errorFilterMiddleware());

export default culqiFunction;
