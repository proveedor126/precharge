import { APIGatewayProxyResult, Context } from 'aws-lambda';
import CulqiLambdaEvent from '../event/culqi-lambda.event';

const handler = async (event: CulqiLambdaEvent<unknown>, context: Context): Promise<APIGatewayProxyResult> => {
  context.callbackWaitsForEmptyEventLoop = false;

  const result = await event.controllerData.method(event.request);

  const response: APIGatewayProxyResult = {
    statusCode: event.controllerData.httpSuccessfulCode,
    body: JSON.stringify({
      request: event.request,
      result,
    }),
  };

  return response;
};

export default handler;
