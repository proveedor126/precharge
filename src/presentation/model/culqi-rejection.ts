interface RejectionObject {
  object: string;
  type: string;
  merchant_message: string;
  code?: string;
  param?: string;
  decline_code?: string;
  charge_id?: string;
  user_message?: string;
}

type CulqiRejection = RejectionObject & { [key: string]: string };

export default CulqiRejection;
