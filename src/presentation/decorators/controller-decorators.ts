import { ParseParams } from 'zod';

interface ValidateSchema<T> {
  parse(data: unknown, params?: Partial<ParseParams>): T;
}

export function controller(name: string, path: string) {
  // eslint-disable-next-line @typescript-eslint/ban-types
  return function (constructor: Function) {
    Reflect.defineMetadata('culqi_controller_watermark', true, constructor);
    Reflect.defineMetadata('culqi_controller', name, constructor);
    Reflect.defineMetadata('culqi_path', path, constructor);
  };
}

function registerAction(path = '', method: string) {
  return function (_target: unknown, _key: string | symbol, descriptor: PropertyDescriptor) {
    Reflect.defineMetadata('culqi_action_watermark', true, descriptor.value);
    Reflect.defineMetadata('culqi_method', method, descriptor.value);
    Reflect.defineMetadata('culqi_path', path, descriptor.value);
    return descriptor;
  };
}

export const httpGet = (path = '') => registerAction(path, 'GET');
export const httpPost = (path = '') => registerAction(path, 'POST');
export const httpPut = (path = '') => registerAction(path, 'PUT');
export const httpDelete = (path = '') => registerAction(path, 'DELETE');

export function zodValidate(schema: ValidateSchema<unknown>) {
  return function (_target: unknown, _key: string | symbol, descriptor: PropertyDescriptor) {
    Reflect.defineMetadata('culqi_validate_schema', schema, descriptor.value);
    return descriptor;
  };
}

export function httpSuccessfulCode(code: number) {
  return function (_target: unknown, _key: string | symbol, descriptor: PropertyDescriptor) {
    Reflect.defineMetadata('culqi_http_successful_code', code, descriptor.value);
    return descriptor;
  };
}
