import { ParseParams } from 'zod';
import container from '../../infrastructure/di/di-container';

interface Type<T = unknown> extends Function {
  new (...args: unknown[]): T;
}

interface ValidateSchema<T> {
  parse(data: unknown, params?: Partial<ParseParams>): T;
}

export type controllerData = {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  instance: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  method: any;
  controllerPath: string;
  actionMethod: string;
  actionPath: string;
  validateSchema: ValidateSchema<unknown> | undefined;
  httpSuccessfulCode: number;
};

export default class ControllerManager {
  private static controllerMap: Map<string, Type<unknown>> = new Map<string, Type<unknown>>();
  private static controllerPaths: { controllerPath: string; controllerName: string }[] = [];

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  static registerControllers(controllers: any) {
    controllers.forEach((controllerType: Type<unknown>) => {
      const controller = Object.create(controllerType);

      if (Reflect.getMetadata('culqi_controller_watermark', controller)) {
        const controllerName = Reflect.getMetadata('culqi_controller', controller);
        const controllerPath = Reflect.getMetadata('culqi_path', controller);

        this.controllerMap.set(controllerName, controller);

        this.controllerPaths.push({ controllerPath, controllerName });
      }
    });
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private static getMethods(obj: any): string[] {
    const properties = new Set<string>();

    const currentObj = Object.getPrototypeOf(obj);
    Object.getOwnPropertyNames(currentObj).map((item) => properties.add(item));

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    return [...properties.keys()].filter(
      (item: any) => typeof obj[item] === 'function' && item !== 'constructor',
    );
  }

  private static validatePath(requestPath: string, route: string): boolean {
    const splittedRequestPath = requestPath.split('/');
    const splittedRoute = route.split('/');

    if (splittedRequestPath.length !== splittedRoute.length) {
      return false;
    }

    for (let i = 0; i < splittedRequestPath.length; i++) {
      const currentRoutePath = splittedRoute[i];
      const currentRequestPathPart = splittedRequestPath[i];

      if (currentRoutePath && currentRoutePath.startsWith(':')) {
        const requestPartNotEmpty = !!currentRequestPathPart;

        if (!requestPartNotEmpty) {
          return false;
        }

        continue;
      }

      if (currentRequestPathPart.toLowerCase() !== currentRoutePath.toLowerCase()) {
        return false;
      }
    }

    return true;
  }

  static getControllerData(pathToFind: string, methodToFind: string): controllerData | undefined {
    for (let i = 0; i < this.controllerPaths.length; i++) {
      const currentControllerPath = this.controllerPaths[i].controllerPath.toLowerCase();

      if (pathToFind.toLowerCase().startsWith(currentControllerPath)) {
        const controllerPath = this.controllerPaths[i].controllerPath;
        const controllerName = this.controllerPaths[i].controllerName;

        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const instance: any = container.resolve(controllerName);

        const methods = this.getMethods(instance);

        for (let i = 0; i < methods.length; i++) {
          const currentMethod = methods[i];

          if (Reflect.getMetadata('culqi_action_watermark', instance[currentMethod])) {
            const actionMethod = Reflect.getMetadata('culqi_method', instance[currentMethod]);
            const actionPath = Reflect.getMetadata('culqi_path', instance[currentMethod]);

            const endpoint = actionPath ? `/${actionPath}` : '';

            const fullPath = `${controllerPath}${endpoint}`;

            if (this.validatePath(pathToFind, fullPath) && actionMethod === methodToFind) {
              const method = instance[currentMethod].bind(instance);

              const validateSchema = Reflect.getMetadata(
                'culqi_validate_schema',
                instance[currentMethod],
              );

              const httpSuccessfulCode = Reflect.getMetadata(
                'culqi_http_successful_code',
                instance[currentMethod],
              );

              return {
                instance,
                method,
                controllerPath,
                actionMethod,
                actionPath,
                validateSchema: validateSchema as ValidateSchema<unknown>,
                httpSuccessfulCode,
              };
            }
          }
        }
      }
    }

    return undefined;
  }
}
