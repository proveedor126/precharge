import { inject, injectable } from 'tsyringe';
import { controller, httpPost, zodValidate } from '../decorators/controller-decorators';
import ValidateChargeUseCase from '../../application/use-case/validate-charge.use-case';
import ValidateChargeResponseDto from '../../application/dto/validate-charge-response.dto';
import ValidateChargeRequestDto, {
  validateChargeRequestSchema,
} from '../../application/dto/validate-charge-request.dto';

@injectable()
@controller('ValidateChargeController', '/validate-charge')
export class ValidateChargeController {
  constructor(
    @inject('ValidateChargeUseCase') private _validateChargeUseCase: ValidateChargeUseCase,
  ) {}
  // eslint-disable-next-line @typescript-eslint/no-empty-function

  @zodValidate(validateChargeRequestSchema)
  @httpPost('')
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  async validateCharge(request: ValidateChargeRequestDto): Promise<ValidateChargeResponseDto> {
    const result = await this._validateChargeUseCase.execute(request);
    return result;
  }
}
