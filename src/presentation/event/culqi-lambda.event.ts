import { APIGatewayProxyEvent } from 'aws-lambda';
import { controllerData } from '../controller/controller-manager';

interface CulqiLambdaEventDetails<T> {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  request: T;
  controllerData: controllerData;
}

type CulqiLambdaEvent<T> = CulqiLambdaEventDetails<T> & APIGatewayProxyEvent;

export default CulqiLambdaEvent;
