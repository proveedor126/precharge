# CulqiFunction

El presente repositorio contiene un boilerplate con el propósito de poder implementar funciones lambda a utilizarse en Culqi.

## Probando la solución

### Requisitos

* Asegurarse de tener instalado AWS en su PC local. De no tenerlo instalado, por favor revisar [esta guía](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html).
* Asegurarse de tener instalado SAM CLI en su pc LOCAL. De no tenerlo instalado, por favor revisar [esta guía](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html).
* Asegurarse de tener instalado Node JS en la máquina local. De no tenerlo instalado visitar su página oficial [NodeJS](https://nodejs.org/en/).
* Asegurarse de tener instalado Docker. De no tenerlo instalado, visitar su página oficial [Docker](https://www.docker.com/).

### Ejecución local

Para compilar la solución, ejecutar el siguiente comando en la raiz de la fuente clonada:

```sh
$ npm run build
```

Seguido de ello, ejecutar el siguiente comando:

```sh
$ npm run start:dev
```
