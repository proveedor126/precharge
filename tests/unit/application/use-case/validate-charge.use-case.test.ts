import 'reflect-metadata';
import {
  getIsTestToken,
  getSource,
} from '../../../../src/application/use-case/validate-charge.use-case';
import { Response } from '../../../../src/constants/constants';

import ValidationCulqiError from '../../../../src/error/validation.culqi-error';

describe('ValidateChargeUseCase', () => {
  describe('getIsTestToken', () => {
    describe('Should get flag isTestToken true', () => {
      it('when token is tkn_test_visa', () => {
        const tokenMock = 'tkn_test_visa';
        expect(getIsTestToken(tokenMock)).toBe(true);
      });

      it('when token is tkn_test_mastercard', () => {
        const tokenMock = 'tkn_test_mastercard';
        expect(getIsTestToken(tokenMock)).toBe(true);
      });

      it('when token is tkn_test_amex', () => {
        const tokenMock = 'tkn_test_amex';
        expect(getIsTestToken(tokenMock)).toBe(true);
      });

      it('when token is tkn_test_diners', () => {
        const tokenMock = 'tkn_test_diners';
        expect(getIsTestToken(tokenMock)).toBe(true);
      });
    });

    it('Should get flag isTestToken false when token is not a test token', () => {
      const tokenMock = 'some_random_tokeb';
      expect(getIsTestToken(tokenMock)).toBe(false);
    });
  });

  describe('getSource', () => {
    it('Should throw error when source_id is not 25 length and token is not a test token', () => {
      const isTestTokenMock: boolean = false;
      const sourceId: string = 'foo';

      expect(() => {
        getSource(isTestTokenMock, sourceId);
      }).toThrow(new ValidationCulqiError(Response.INVALID_SOURCE_ID_INEXISTENT_VALUE));
    });

    describe('Should get source', () => {
      it('when source_id prefix is tkn', () => {
        const isTestToken = false;
        const sourceId = 'tkn__11111111111111111111';
        expect(getSource(isTestToken, sourceId)).toBe(1);
      });

      it('when source_id prefix is crd', () => {
        const isTestToken = false;
        const sourceId = 'crd__11111111111111111111';
        expect(getSource(isTestToken, sourceId)).toBe(2);
      });

      it('when source_id prefix is ype', () => {
        const isTestToken = false;
        const sourceId = 'ype__11111111111111111111';
        expect(getSource(isTestToken, sourceId)).toBe(3);
      });
    });

    it('Should throw error when prefix does not belong to any source', () => {
      const isTestTokenMock: boolean = false;
      const sourceId: string = 'xyz__11111111111111111111';

      expect(() => {
        getSource(isTestTokenMock, sourceId);
      }).toThrow(new ValidationCulqiError(Response.INVALID_SOURCE_ID_INEXISTENT_VALUE));
    });
  });
});
