import 'reflect-metadata';
import {
  getLightToken,
  getProductId,
  getToken,
} from '../../../../../src/application/use-case/charge-validator/card.charge-validator';
import { Response, products } from '../../../../../src/constants/constants';
import { sources } from '../../../../../src/application/use-case/charge-validator/charge-validator';
import LightToken from '../../../../../src/domain/entity/light-token.entity';
import LightTokenRepository from '../../../../../src/domain/repository/light-token.repository';
import Token from '../../../../../src/domain/entity/token.entity';
import TokenRepository from '../../../../../src/domain/repository/token.repository';
import ValidationCulqiError from '../../../../../src/error/validation.culqi-error';

describe('CardChargeValidator', () => {
  describe('getProductId', () => {
    it('Should return ChargeRequest product_id when product_id is not undefined', () => {
      const productIdMock = 99;
      expect(getProductId(productIdMock)).toBe(productIdMock);
    });

    it('Should return TOKEN when product_id is undefined', () => {
      const productIdMock = undefined;
      expect(getProductId(productIdMock)).toBe(products.TOKEN);
    });
  });

  describe('getLightToken', () => {
    it('Should get token and delete it form repository when token is found by refId', async () => {
      const idMock = 'foo';
      const refIdMock = 'foo';
      const lightTokenMock = new LightToken(idMock, refIdMock, [1, 2], undefined, 1);
      const getByRefIdMock = jest.fn().mockResolvedValue(lightTokenMock);
      const getByRefIdAndMerchantIdMock = jest.fn();
      const deleteMock = jest.fn();

      const lightTokenRepositoryMock = jest.mocked<LightTokenRepository>({
        getByRefId: getByRefIdMock,
        getByRefIdAndMerchantId: getByRefIdAndMerchantIdMock,
        delete: deleteMock,
      });

      const result = await getLightToken(refIdMock, lightTokenRepositoryMock);

      expect(getByRefIdMock).toHaveBeenCalledWith(refIdMock);
      expect(getByRefIdAndMerchantIdMock).not.toHaveBeenCalled();
      expect(deleteMock).toHaveBeenCalledWith(idMock);
      expect(result).toMatchObject(lightTokenMock);
    });
  });

  describe('getToken', () => {
    it('Should get token when source is card it is found by refId', async () => {
      const refIdMock = 'foo';
      const tokenMock = new Token(refIdMock, [1, 2], true, 1, new Date(2050, 1, 1), undefined, 1);
      const getByRefId = jest.fn().mockResolvedValue(tokenMock);
      const getByRefIdAndMerchantIdMock = jest.fn();
      const updateMock = jest.fn();

      const tokenRepositoryMock = jest.mocked<TokenRepository>({
        getByRefId: getByRefId,
        getByRefIdAndMerchantId: getByRefIdAndMerchantIdMock,
        update: updateMock,
      });

      const result = await getToken(refIdMock, tokenRepositoryMock);

      expect(getByRefId).toHaveBeenCalledWith(refIdMock);
      expect(updateMock).not.toHaveBeenCalled();
      expect(tokenMock).toMatchObject(result);
    });

    it('Should throw error when token is not found', async () => {
      const sourceMock = sources.tkn;
      const refIdMock = 'foo';
      const merchantIdMock = 1;
      const getByRefId = jest.fn();
      const getByRefIdAndMerchantIdMock = jest.fn();
      const updateMock = jest.fn();

      const tokenRepositoryMock = jest.mocked<TokenRepository>({
        getByRefId: getByRefId,
        getByRefIdAndMerchantId: getByRefIdAndMerchantIdMock,
        update: updateMock,
      });

      await expect(() => getToken(refIdMock, tokenRepositoryMock)).rejects.toThrowError(
        new ValidationCulqiError(Response.TOKEN_ID_NOT_FOUND),
      );
    });
  });
});
