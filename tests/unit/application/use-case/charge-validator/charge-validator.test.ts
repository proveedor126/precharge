import 'reflect-metadata';
import {
  environments,
  getAmount,
  getOperation,
  operations,
  validateAmount,
  validateSourceEnvironment,
} from '../../../../../src/application/use-case/charge-validator/charge-validator';
import ValidationCulqiError from '../../../../../src/error/validation.culqi-error';
import { Response, products } from '../../../../../src/constants/constants';
import LightToken from '../../../../../src/domain/entity/light-token.entity';
import Token from '../../../../../src/domain/entity/token.entity';
import UnexpectedError from '../../../../../src/error/unexpected-error';
import MerchantDataDto from '../../../../../src/application/dto/merchant-data.dto';

describe('ChargeValidator', () => {
  describe('getAmount', () => {
    it('should parse amount to number', () => {
      const amountMock = '50000';

      expect(getAmount(amountMock)).toBe(50000);
    });
  });

  describe('getOperation', () => {
    it('Should get CARD_VALIDATE_CHARGE when payment type is 1', () => {
      const paymentTypeMock = 1;
      expect(getOperation(paymentTypeMock)).toBe(operations.CARD_VALIDATE_CHARGE);
    });

    it('Should get CHARGE_CREATION when payment type is different to 1', () => {
      const paymentTypeMock = 2;
      expect(getOperation(paymentTypeMock)).toBe(operations.CHARGE_CREATION);
    });
  });

  describe('validateAmount', () => {
    it('Should return error when tokens has no brand', () => {
      const operationMock = operations.CARD_VALIDATE_CHARGE;
      const amountMock = '99000';
      const currencyMock = 'PEN';
      const lightTokenMock = undefined;
      const tokenMock = undefined;
      const merchanDataMock = undefined;

      expect(() =>
        validateAmount(
          operationMock,
          amountMock,
          currencyMock,
          lightTokenMock,
          tokenMock,
          merchanDataMock,
        ),
      ).toThrowError(new UnexpectedError());
    });

    it('Should return error when currency and brand does not match with any merchant terminal from light token', () => {
      const operationMock = operations.CARD_VALIDATE_CHARGE;
      const amountMock = '99000';
      const currencyMock = 'PEN';
      const lightTokenMock = new LightToken('FOO', 'foo', undefined, 1);
      const tokenMock = undefined;
      const merchanDataMock: MerchantDataDto = {
        merchant: {
          id: 1,
        },
        merchantProcessorTerminal: [
          {
            currencyIsoCode: 'USD',
            currencyExponent: 2,
            brandId: 1,
          },
        ],
      };

      expect(() =>
        validateAmount(
          operationMock,
          amountMock,
          currencyMock,
          lightTokenMock,
          tokenMock,
          merchanDataMock,
        ),
      ).toThrowError(
        new ValidationCulqiError(Response.INVALID_CURRENCY_CODE_NOT_AVAILABLE_FOR_MERCHANT_RF),
      );
    });

    it('Should return error when currency and brand does not match with any merchant terminal from token', () => {
      const operationMock = operations.CARD_VALIDATE_CHARGE;
      const amountMock = '99000';
      const currencyMock = 'PEN';
      const lightTokenMock = undefined;
      const tokenMock = new Token('foo', true, 1, new Date(2024, 1, 1), undefined, 1);
      const merchanDataMock: MerchantDataDto = {
        merchant: {
          id: 1,
        },
        merchantProcessorTerminal: [
          {
            currencyIsoCode: 'USD',
            currencyExponent: 2,
            brandId: 1,
          },
        ],
      };

      expect(() =>
        validateAmount(
          operationMock,
          amountMock,
          currencyMock,
          lightTokenMock,
          tokenMock,
          merchanDataMock,
        ),
      ).toThrowError(
        new ValidationCulqiError(Response.INVALID_CURRENCY_CODE_NOT_AVAILABLE_FOR_MERCHANT_RF),
      );
    });

    it('Should return error when operation is not CARD_VALIDATE_CHARGE and amount is out of range', () => {
      const operationMock = operations.CHARGE_CREATION;
      const amountMock = '9999999000';
      const currencyMock = 'PEN';
      const lightTokenMock = undefined;
      const tokenMock = new Token('foo', true, 1, new Date(2024, 1, 1), undefined, 1);
      const merchanDataMock: MerchantDataDto = {
        merchant: {
          id: 1,
        },
        merchantProcessorTerminal: [
          {
            currencyIsoCode: 'PEN',
            currencyExponent: 2,
            brandId: 1,
          },
        ],
      };

      expect(() =>
        validateAmount(
          operationMock,
          amountMock,
          currencyMock,
          lightTokenMock,
          tokenMock,
          merchanDataMock,
        ),
      ).toThrowError(new ValidationCulqiError(Response.INVALID_AMOUNT));
    });

    it('Should pass validation when operation is CARD_VALIDATE_CHARGE and amount is under range', () => {
      const operationMock = operations.CHARGE_CREATION;
      const amountMock = '99000';
      const currencyMock = 'PEN';
      const lightTokenMock = undefined;
      const tokenMock = new Token('foo', true, 1, new Date(2024, 1, 1), undefined, 1);
      const merchanDataMock: MerchantDataDto = {
        merchant: {
          id: 1,
        },
        merchantProcessorTerminal: [
          {
            currencyIsoCode: 'PEN',
            currencyExponent: 2,
            brandId: 1,
          },
        ],
      };

      expect(() =>
        validateAmount(
          operationMock,
          amountMock,
          currencyMock,
          lightTokenMock,
          tokenMock,
          merchanDataMock,
        ),
      ).not.toThrowError();
    });
  });

  it('Should pass validation when operation is CARD_VALIDATE_CHARGE and amount is under range', () => {
    const operationMock = operations.CARD_VALIDATE_CHARGE;
    const amountMock = '99000';
    const currencyMock = 'PEN';
    const lightTokenMock = undefined;
    const tokenMock = new Token('foo', true, 1, new Date(2024, 1, 1), undefined, 1);
    const merchanDataMock: MerchantDataDto = {
      merchant: {
        id: 1,
      },
      merchantProcessorTerminal: [
        {
          currencyIsoCode: 'PEN',
          currencyExponent: 2,
          brandId: 1,
        },
      ],
    };

    expect(() =>
      validateAmount(
        operationMock,
        amountMock,
        currencyMock,
        lightTokenMock,
        tokenMock,
        merchanDataMock,
      ),
    ).not.toThrowError();
  });
});

describe('validateSourceEnvironment', () => {
  describe('Should throw error when environment is TEST and source_id belongs to LIVE', () => {
    it('and productId is token', () => {
      const environmentMock = environments.TEST;
      process.env.CULQI_ENVIRONMENT = environmentMock;

      const sourceIdMock = environments.LIVE;

      const productIdMock = products.TOKEN;

      expect(() => validateSourceEnvironment(sourceIdMock, productIdMock)).toThrow(
        new ValidationCulqiError(Response.TOKEN_NOT_IN_INTEGRATION),
      );
    });

    it('and productId is not token', () => {
      const environmentMock = environments.TEST;
      process.env.CULQI_ENVIRONMENT = environmentMock;

      const sourceIdMock = environments.LIVE;

      const productIdMock = 99;

      expect(() => validateSourceEnvironment(sourceIdMock, productIdMock)).toThrow(
        new ValidationCulqiError(Response.CARD_NOT_EXISTS_IN_INTEGRATION_RF),
      );
    });
  });

  describe('Should throw error when environment is LIVE and source_id belongs to TEST', () => {
    it('and productId is token', () => {
      const environmentMock = environments.LIVE;
      process.env.CULQI_ENVIRONMENT = environmentMock;

      const sourceIdMock = environments.TEST;

      const productIdMock = products.TOKEN;

      expect(() => validateSourceEnvironment(sourceIdMock, productIdMock)).toThrow(
        new ValidationCulqiError(Response.TOKEN_NOT_IN_PRODUCTION),
      );
    });

    it('and productId is not token', () => {
      const environmentMock = environments.LIVE;
      process.env.CULQI_ENVIRONMENT = environmentMock;

      const sourceIdMock = environments.TEST;

      const productIdMock = 99;

      expect(() => validateSourceEnvironment(sourceIdMock, productIdMock)).toThrow(
        new ValidationCulqiError(Response.CARD_NOT_EXISTS_IN_PRODUCTION_RF),
      );
    });
  });

  describe('Should not throw any error', () => {
    it('when enviroment is TEST and source_id belongs to TEST', () => {
      const environmentMock = environments.TEST;
      process.env.CULQI_ENVIRONMENT = environmentMock;

      const sourceIdMock = environments.TEST;

      const productIdMock = products.TOKEN;

      expect(() => validateSourceEnvironment(sourceIdMock, productIdMock)).not.toThrow();
    });

    it('when enviroment is LIVE and source_id belongs to LIVE', () => {
      const environmentMock = environments.LIVE;
      process.env.CULQI_ENVIRONMENT = environmentMock;

      const sourceIdMock = environments.LIVE;

      const productIdMock = products.TOKEN;

      expect(() => validateSourceEnvironment(sourceIdMock, productIdMock)).not.toThrow();
    });
  });
});
