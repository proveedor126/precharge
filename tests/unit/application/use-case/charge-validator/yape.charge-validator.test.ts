import 'reflect-metadata';
import {
  validateYapeAmount,
  validateYapeCharge,
} from '../../../../../src/application/use-case/charge-validator/yape.charge-validator';
import ValidationCulqiError from '../../../../../src/error/validation.culqi-error';
import { Response } from '../../../../../src/constants/constants';
import LightToken from '../../../../../src/domain/entity/light-token.entity';
import Token from '../../../../../src/domain/entity/token.entity';
import UnexpectedError from '../../../../../src/error/unexpected-error';

describe('YapeChargeValidator', () => {
  describe('validateYapeCharge', () => {
    it('Should throw error when amount is above Yape max', () => {
      const amountMock = 60000;
      const currencyIsoMock = 'PEN';

      expect(() => validateYapeCharge(amountMock, currencyIsoMock)).toThrowError(
        new ValidationCulqiError(Response.INVALID_AMOUNT),
      );
    });

    it('Should throw error when curreny is not PEN', () => {
      const amountMock = 20000;
      const currencyIsoMock = 'USD';

      expect(() => validateYapeCharge(amountMock, currencyIsoMock)).toThrowError(
        new ValidationCulqiError(Response.INVALID_CURRENCY_CODE_YAPE),
      );
    });

    it('Should throw no error if amount and currency are correct', () => {
      const amountMock = 30000;
      const currencyIsoMock = 'PEN';

      expect(() => validateYapeCharge(amountMock, currencyIsoMock)).not.toThrowError();
    });
  });

  describe('validateYapeAmount', () => {
    it('Should throw error when yape amount from Light Token is different from charge amount', () => {
      const amount = 50000;
      const lightToken = new LightToken('foo', 'foo', [2], 40000, 1);

      expect(() => validateYapeAmount(amount, lightToken, undefined)).toThrowError(
        new ValidationCulqiError(Response.INVALID_AMOUNT),
      );
    });

    it('Should throw error when yape amount from Token is different from charge amount', () => {
      const amount = 50000;
      const token = new Token('foo', [2], true, 1, new Date(2040, 1, 1), 40000, 1);

      expect(() => validateYapeAmount(amount, undefined, token)).toThrowError(
        new ValidationCulqiError(Response.INVALID_AMOUNT),
      );
    });

    it('Should throw error when amount is empty', () => {
      const amount = 50000;
      const token = new Token('foo', true, 1, new Date(2040, 1, 1), undefined, 1);

      expect(() => validateYapeAmount(amount, undefined, token)).toThrowError(
        new UnexpectedError(),
      );
    });

    it('Should throw no error when amount is the as yape amount', () => {
      const amount = 50000;
      const token = new Token('foo', true, 1, new Date(2040, 1, 1), 50000, 1);

      expect(() => validateYapeAmount(amount, undefined, token)).not.toThrowError();
    });
  });
});
