import 'reflect-metadata';
import {
  getLightToken,
  getProductId,
  getToken,
} from '../../../../../src/application/use-case/charge-validator/base-token.charge-validator';
import { Response, products } from '../../../../../src/constants/constants';
import { sources } from '../../../../../src/application/use-case/charge-validator/charge-validator';
import LightToken from '../../../../../src/domain/entity/light-token.entity';
import LightTokenRepository from '../../../../../src/domain/repository/light-token.repository';
import Token from '../../../../../src/domain/entity/token.entity';
import TokenRepository from '../../../../../src/domain/repository/token.repository';
import ValidationCulqiError from '../../../../../src/error/validation.culqi-error';
import UnexpectedError from '../../../../../src/error/unexpected-error';

describe('CardChargeValidator', () => {
  describe('getProductId', () => {
    it('Should return TOKEN', () => {
      expect(getProductId()).toBe(products.TOKEN);
    });
  });

  describe('getLightToken', () => {
    it('Should get token and delete it form repository', async () => {
      const idMock = 'foo';
      const refIdMock = 'foo';
      const merchantIdMock = 1;
      const lightTokenMock = new LightToken(idMock, refIdMock, undefined, 1);
      const getByRefIdMock = jest.fn();
      const getByRefIdAndMerchantIdMock = jest.fn().mockResolvedValue(lightTokenMock);
      const deleteMock = jest.fn();

      const lightTokenRepositoryMock = jest.mocked<LightTokenRepository>({
        getByRefId: getByRefIdMock,
        getByRefIdAndMerchantId: getByRefIdAndMerchantIdMock,
        delete: deleteMock,
      });

      const result = await getLightToken(refIdMock, merchantIdMock, lightTokenRepositoryMock);

      expect(getByRefIdAndMerchantIdMock).toHaveBeenCalledWith(refIdMock, merchantIdMock);
      expect(getByRefIdMock).not.toHaveBeenCalled();
      expect(deleteMock).toHaveBeenCalledWith(idMock);
      expect(result).toMatchObject(lightTokenMock);
    });

    it('Should throw error when token is not found', async () => {
      const idMock = 'foo';
      const refIdMock = 'foo';
      const merchantIdMock = 1;
      const getByRefIdMock = jest.fn();
      const getByRefIdAndMerchantIdMock = jest.fn().mockResolvedValue(undefined);
      const deleteMock = jest.fn();

      const lightTokenRepositoryMock = jest.mocked<LightTokenRepository>({
        getByRefId: getByRefIdMock,
        getByRefIdAndMerchantId: getByRefIdAndMerchantIdMock,
        delete: deleteMock,
      });

      await expect(
        async () => await getLightToken(refIdMock, merchantIdMock, lightTokenRepositoryMock),
      ).rejects.toThrowError(new ValidationCulqiError(Response.TOKEN_EXPIRED_RF));
    });

    it('Should throw error when merchantId is empty', async () => {
      const idMock = 'foo';
      const refIdMock = 'foo';
      const merchantIdMock = undefined;
      const getByRefIdMock = jest.fn();
      const getByRefIdAndMerchantIdMock = jest.fn().mockResolvedValue(undefined);
      const deleteMock = jest.fn();

      const lightTokenRepositoryMock = jest.mocked<LightTokenRepository>({
        getByRefId: getByRefIdMock,
        getByRefIdAndMerchantId: getByRefIdAndMerchantIdMock,
        delete: deleteMock,
      });

      await expect(
        async () => await getLightToken(refIdMock, merchantIdMock, lightTokenRepositoryMock),
      ).rejects.toThrowError(new UnexpectedError());
    });
  });

  describe('getToken', () => {
    it('Should get token when it is found by refId and merchant', async () => {
      const refIdMock = 'foo';
      const merchantIdMock = 1;
      const tokenMock = new Token(refIdMock, true, 1, new Date(2050, 1, 1), undefined, 1);
      const getByRefId = jest.fn();
      const getByRefIdAndMerchantIdMock = jest.fn().mockResolvedValue(tokenMock);
      const updateMock = jest.fn();

      const tokenRepositoryMock = jest.mocked<TokenRepository>({
        getByRefId: getByRefId,
        getByRefIdAndMerchantId: getByRefIdAndMerchantIdMock,
        update: updateMock,
      });

      const result = await getToken(refIdMock, merchantIdMock, tokenRepositoryMock);

      expect(getByRefIdAndMerchantIdMock).toHaveBeenCalledWith(refIdMock, merchantIdMock);
      expect(result).toMatchObject(tokenMock);
    });

    it('Should throw error when token is not found', async () => {
      const sourceMock = sources.tkn;
      const refIdMock = 'foo';
      const merchantIdMock = 1;
      const getByRefId = jest.fn();
      const getByRefIdAndMerchantIdMock = jest.fn();
      const updateMock = jest.fn();

      const tokenRepositoryMock = jest.mocked<TokenRepository>({
        getByRefId: getByRefId,
        getByRefIdAndMerchantId: getByRefIdAndMerchantIdMock,
        update: updateMock,
      });

      await expect(() =>
        getToken(refIdMock, merchantIdMock, tokenRepositoryMock),
      ).rejects.toThrowError(new ValidationCulqiError(Response.TOKEN_ID_NOT_FOUND));
    });

    it('Should throw error when token is has expired', async () => {
      const refIdMock = 'foo';
      const merchantIdMock = 1;
      const tokenMock = new Token(refIdMock, [1, 2], true, 1, new Date(2010, 1, 1), undefined, 1);
      const getByRefId = jest.fn();
      const getByRefIdAndMerchantIdMock = jest.fn().mockResolvedValue(tokenMock);
      const updateMock = jest.fn();

      const tokenRepositoryMock = jest.mocked<TokenRepository>({
        getByRefId: getByRefId,
        getByRefIdAndMerchantId: getByRefIdAndMerchantIdMock,
        update: updateMock,
      });

      await expect(() =>
        getToken(refIdMock, merchantIdMock, tokenRepositoryMock),
      ).rejects.toThrowError(new ValidationCulqiError(Response.TOKEN_EXPIRED_RF));
    });

    it('Should throw error when merchantId is empty', async () => {
      const refIdMock = 'foo';
      const merchantIdMock = undefined;
      const getByRefId = jest.fn();
      const getByRefIdAndMerchantIdMock = jest.fn().mockResolvedValue(undefined);
      const updateMock = jest.fn();

      const tokenRepositoryMock = jest.mocked<TokenRepository>({
        getByRefId,
        getByRefIdAndMerchantId: getByRefIdAndMerchantIdMock,
        update: updateMock,
      });

      await expect(
        async () => await getToken(refIdMock, merchantIdMock, tokenRepositoryMock),
      ).rejects.toThrowError(new UnexpectedError());
    });
  });
});
